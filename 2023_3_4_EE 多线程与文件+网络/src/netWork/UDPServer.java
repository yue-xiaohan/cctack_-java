package netWork;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class UDPServer {
    // 站在服务器的角度:
// 1. 源 IP: 服务器程序本机的 IP
// 2. 源端口: 服务器绑定的端口 (此处手动指定了 9090)
// 3. 目的 IP: 包含在收到的数据报中. (客户端的IP)
// 4. 目的端口: 包含在收到的数据报中. (客户端的端口)
// 5. 协议类型: UDP
    private DatagramSocket socket = null;

    public UDPServer(int port) throws SocketException {
        this.socket = new DatagramSocket(port);
    }

    // 启动服务器.接受到请求并返回响应
    public void start() throws IOException {
        System.out.println("启动服务器");
        /*创建一个来接受*/
        DatagramPacket reseive=new DatagramPacket(new byte[4096],4096);
        socket.receive(reseive);
        /*解析为字符串*/
        String str=new String(reseive.getData(),0,reseive.getLength());
        /*接受响应*/
        String respond=respond(str);
        DatagramPacket newrespond =new DatagramPacket(respond.getBytes(),
                0,respond.length(),reseive.getAddress(),reseive.getPort());
        socket.send(newrespond);
        System.out.printf("[%s:%d] req: %s, resp: %s\n",
                reseive.getAddress().toString(), reseive.getPort(), reseive, respond);
    }
    public String respond(String str){
        return str;
    }
    public static void main(String[] args) throws IOException {
        UdpEchoServer server = new UdpEchoServer(9096);
        server.start();
    }
}
