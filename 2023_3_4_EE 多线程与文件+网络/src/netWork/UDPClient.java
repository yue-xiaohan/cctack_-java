package netWork;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class UDPClient {
    private DatagramSocket socket = null;
    private String serverIP;
    private int serverPort;

    public UDPClient(String serverIP, int serverPort) throws SocketException {
        socket = new DatagramSocket();
        this.serverIP = serverIP;
        this.serverPort = serverPort;
    }
    public void start() throws IOException {

        Scanner scanner = new Scanner(System.in);
        String str=new String();
        str= scanner.nextLine();
        DatagramPacket request=new DatagramPacket(str.getBytes(),str.getBytes().length,
                InetAddress.getByName(serverIP),serverPort);
        socket.send(request);
        DatagramPacket responsePacket = new DatagramPacket(new byte[1024], 1024);
        socket.receive(responsePacket);
        String newrespond=new String(responsePacket.getData(),0,responsePacket.getLength(),
                "UTF-8");
        System.out.println(newrespond);
    }

    public static void main(String[] args) throws IOException {
        UDPClient udpClient=new UDPClient("127.0.0.1",9096);
        udpClient.start();

    }
}
