package netWork;

import java.io.IOException;
import java.net.SocketException;
import java.util.HashMap;

public class UdpDictServer extends UdpEchoServer {
    private HashMap<String, String> dict = new HashMap<>();

    public UdpDictServer(int port) throws SocketException {
        super(port);

        // 简单构造几个词

        dict.put("blue", "蓝色");
        dict.put("red", "红色");
        dict.put("write", "白色");
        dict.put("black", "黑色");
    }

    @Override
    public String process(String request) {
        return dict.getOrDefault(request, "该词无法被翻译!");
    }

    public static void main(String[] args) throws IOException {
        UdpDictServer server = new UdpDictServer(9090);
        server.start();
    }
}