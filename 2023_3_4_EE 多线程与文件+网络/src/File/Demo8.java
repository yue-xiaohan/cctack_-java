package File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Demo8 {
    public static void main(String[] args) {
        // 构造方法中需要指定打开文件的路径.
        // 此处的路径可以是绝对路径, 也可以是相对路径, 还可以是 File 对象
//        InputStream inputStream = null;
//        try {
//            // 1. 创建对象, 同时也是在打开文件.
//            inputStream = new FileInputStream("d:/test.txt");
//            // 2. 尝试一个一个字节的读, 把整个文件都读完.
//            while (true) {
//                int b = inputStream.read();
//                if (b == -1) {
//                    // 读到了文件末尾
//                    break;
//                }
//                System.out.println(b);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            // 3. 读完之后要记得关闭文件, 释放资源~
//            try {
//                inputStream.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }

//        try (InputStream inputStream = new FileInputStream("d:/test.txt")) {
//            while (true) {
//                int b = inputStream.read();
//                if (b == -1) {
//                    break;
//                }
//                System.out.println(b);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        try (InputStream inputStream = new FileInputStream("d:/test.txt")) {
            // 一次读取若干个字节.
            while (true) {
                byte[] buffer = new byte[1024];
                int len = inputStream.read(buffer);
                if (len == -1) {
                    // 如果返回 -1 说明读取完毕了
                    break;
                }
                for (int i = 0; i < len; i++) {
                    System.out.println(buffer[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
