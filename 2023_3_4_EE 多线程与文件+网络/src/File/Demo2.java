package File;

import java.io.File;
import java.util.Hashtable;

public class Demo2 {
    public static void main(String[] args) {
        // File f = new File("d:/test.txt");
        File f = new File("./test.txt");//这些构造方法并不会创建一个文件
        System.out.println(f.exists());//检测是否存在
        System.out.println(f.isDirectory());//检测是否为目录
        System.out.println(f.isFile());//检测是否为普通文件
    }

}
