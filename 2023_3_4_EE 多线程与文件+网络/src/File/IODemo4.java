package File;

import java.io.File;
import java.util.Arrays;

public class IODemo4 {
    public static void main(String[] args) {
        File file = new File("test-dir");
        String[] results = file.list();
        System.out.println(Arrays.toString(results));
        File[] results2 = file.listFiles();
        System.out.println(Arrays.toString(results2));
    }
}
