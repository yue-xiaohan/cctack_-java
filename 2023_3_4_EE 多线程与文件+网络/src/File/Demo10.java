package File;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;


public class Demo10 {
    public static void main(String[] args) {
        // 按照字符来读写
        try (Reader reader = new FileReader("d:/test.txt")) {
            // 按照字符来读.
            while (true) {
                char[] buffer = new char[1024];
                int len = reader.read(buffer);
                if (len == -1) {
                    break;
                }
//                for (int i = 0; i < len; i++) {
//                    System.out.println(buffer[i]);
//                }
                // 如果这里传入的 数组 是 byte 数组, 还可以手动的指定一下 utf8 字符集避免乱码.
                String s = new String(buffer, 0, len);
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
