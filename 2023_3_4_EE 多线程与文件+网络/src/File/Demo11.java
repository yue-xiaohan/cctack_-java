package File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


public class Demo11 {
    public static void main(String[] args) {
        // 按照字符来写
        try (Writer writer = new FileWriter("d:/test.txt")) {
            writer.write("xyz");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
