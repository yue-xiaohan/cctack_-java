package File;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class IODemo6 {
    public static void main(String[] args) throws IOException {
        // 这个过程, 相当于 C 中的 fopen , 文件的打开操作~~
//        InputStream inputStream = null;
//        try {
//            inputStream = new FileInputStream("d:/test.txt");
//        } finally {
//            inputStream.close();
//        }

        try (InputStream inputStream = new FileInputStream("d:/test.txt")) {
            // 读文件
            // read 一次返回的是一个字节. 但是此处的返回值类型是 int !!!
            while (true) {
                int b = inputStream.read();
                if (b == -1) {
                    // 读到末尾了, 结束循环即可
                    break;
                }
                System.out.printf("%x\n", b);
            }
        }
    }
}
