package File;

import java.io.File;
import java.io.IOException;

// File 的构造
public class Demo1 {
    public static void main(String[] args) throws IOException {
        /*常见方法*/
        File f = new File("d:\\test.txt");
        System.out.println(f.getParent()); // 获取到文件的父目录
        System.out.println(f.getName());    // 获取到文件名
        System.out.println(f.getPath());    // 获取到文件路径(构造 File 的时候指定的路径)
        System.out.println(f.getAbsolutePath()); // 获取到绝对路径
        System.out.println(f.getCanonicalPath()); // 获取到绝对路径

        System.out.println("=================");
        /*相对路径的写法-"./" 通常可以省略*/
        File f2 = new File("./test.txt");
        System.out.println(f2.getParent()); // 获取到文件的父目录
        System.out.println(f2.getName());    // 获取到文件名
        System.out.println(f2.getPath());    // 获取到文件路径(构造 File 的时候指定的路径)
        System.out.println(f2.getAbsolutePath()); // 获取到绝对路径
        System.out.println(f2.getCanonicalPath()); // 获取到绝对路径
    }
}
