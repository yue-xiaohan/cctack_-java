package File;

import java.io.File;
import java.io.IOException;

public class IODemo2 {
    public static void main(String[] args) throws IOException {
        // 在相对路径中, ./ 通常可以省略
        File file = new File("./hello_world.txt");
        System.out.println(file.exists());
        System.out.println(file.isDirectory());
        System.out.println(file.isFile());

        // 创建文件
        file.createNewFile();

        System.out.println(file.exists());
        System.out.println(file.isDirectory());
        System.out.println(file.isFile());

        file.delete();
        System.out.println("删除文件之后");
        System.out.println(file.exists());
    }
}
