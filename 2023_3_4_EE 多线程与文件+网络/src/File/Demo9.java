package File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


public class Demo9 {
    public static void main(String[] args) {
        // 使用字节流, 写文件的案例.
        try (OutputStream outputStream = new FileOutputStream("d:/test.txt")) {
//            outputStream.write(97);
//            outputStream.write(98);
//            outputStream.write(99);

            byte[] buffer = new byte[]{97, 98, 99};
            outputStream.write(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
