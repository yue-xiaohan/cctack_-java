package Multithreading;

public class Demo5 {
    public static void main(String[] args) {
        /*创建多线程的方式,使用匿名内部类.实现Runnable接口*/
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello thread");
            }
        });
        t.start();
    }
}