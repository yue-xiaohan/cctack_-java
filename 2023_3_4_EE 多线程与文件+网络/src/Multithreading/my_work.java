package Multithreading;

import java.util.Random;

/*class  my_Thread extends  Thread{
    //第一种多线程写法
    @Override
    public void run() {
        System.out.println("hello");
    }
}*/
/*class my_Thread implements Runnable{

    @Override
    public void run() {
        System.out.println("第二种");
    }
}*/
public class my_work {
    static  long even =0;
    static  long odd=0;
    public static void main(String[] args) throws InterruptedException {
        /*给定一个很长的数组 (长度 1000w), 通过随机数的方式生成 1-100 之间的整数.
        实现代码, 能够创建两个线程, 对这个数组的所有元素求和.
        其中线程1 计算偶数下标元素的和, 线程2 计算奇数下标元素的和.
        最终再汇总两个和, 进行相加
        记录程序的执行时间. */
        int[] arr=new int[1000_0000];
        Random random=new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i]= random.nextInt(99)+1;
        }
        long beg = System.currentTimeMillis();
        Thread t1=new Thread(()->{
            for (int i = 0; i < arr.length; i=i+2) {
                even +=arr[i];
            }
        });
        t1.start();
        Thread t2=new Thread(()->{
            for (int i = 1; i < arr.length; i=i+2) {
                odd +=arr[i];
            }
        });
        t2.start();
        t1.join();
        t2.join();
        long end = System.currentTimeMillis();
        long sum=even+odd;
        System.out.println("偶数的值为"+even);
        System.out.println("奇数的值为"+odd);
        System.out.println("奇数+偶数的总和为"+sum);
        System.out.println("消耗时间: " + (end - beg) + "ms");

    }
    public static void main1(String[] args) {
        //实现线程的5种写法

        Thread thread=new Thread(()->{
            System.out.println("第5种写法,使用Lambert表达式");
        });
        thread.start();

    }
}
