package Multithreading;

public class ThreadDemo12 {
    public static void main(String[] args) throws InterruptedException {
        Thread t = new Thread(() -> {
            while (true) {
                // 为了防止 hello 把 线程状态冲没了, 先注释掉.
                // System.out.println("hello");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        // 在启动之前, 获取线程状态. NEW
        System.out.println(t.getState());

        t.start();


        Thread.sleep(2000);
        System.out.println(t.getState());
    }
}
