package Multithreading;

public class Work2 {
    public static void main(String[] args) {
        Thread a = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.print("A");
            }
        });

        Thread b = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.print("B");
            }
        });

        Thread c = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.print("C");
            }
        });
        c.run();
        b.run();
        a.run();
    }

    public static void main1(String[] args) throws InterruptedException {
        for(int i=0;i<10;i++) {
            Thread t1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.print("A");
                }
            });
            t1.run();
            Thread t2 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.print("B");
                }
            });
            t2.run();
            Thread t3 = new Thread(new Runnable() {
                @Override
                public void run() {
                    System.out.println("C");
                }
            });
            t3.run();
        }
    }

}
