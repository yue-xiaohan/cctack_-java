package Multithreading;

import java.util.concurrent.Semaphore;

public class Demo30 {
    public static void main(String[] args) throws InterruptedException {
        // 初始化的值表示可用资源有 4 个.
        Semaphore semaphore = new Semaphore(4);
        // 申请资源, P 操作
        semaphore.acquire();
        System.out.println("申请成功");
        semaphore.acquire();
        System.out.println("申请成功");
        semaphore.acquire();
        System.out.println("申请成功");
        semaphore.acquire();
        System.out.println("申请成功");
        semaphore.acquire();
        System.out.println("申请成功");
        // 释放资源, V 操作
        // semaphore.release();
    }
}