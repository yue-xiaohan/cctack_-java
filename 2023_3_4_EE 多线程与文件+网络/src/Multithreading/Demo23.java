package Multithreading;

import java.util.Timer;
import java.util.TimerTask;

public class Demo23 {
    /*标准库中,定时器的使用*/
    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                System.out.println("hello timer");
            }
        }, 3000);

        System.out.println("main");
    }
}