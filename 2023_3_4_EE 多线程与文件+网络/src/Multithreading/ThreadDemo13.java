package Multithreading;

// 线程不安全
class Counter22 {
    private int count = 0;

    public void add() {
        count++;
    }

    public int get() {
        return count;
    }
}

public class ThreadDemo13 {
    public static void main(String[] args) throws InterruptedException {
        Counter22 counter = new Counter22();

        // 搞两个线程, 两个线程分别对这个 counter 自增 5w 次.
        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                counter.add();
            }
        });
        Thread t2 = new Thread(() -> {
            for (int i = 0; i < 50000; i++) {
                counter.add();
            }
        });
        t1.start();
        t2.start();

        // 等待两个线程执行结束, 然后看结果.
        t1.join();
        t2.join();

        System.out.println(counter.get());
    }
}
