package Multithreading;

// 实现单例模式 - 懒汉模式
class Singleton2 {
    // 1. 就不是立即就初始化实例.
    private static volatile Singleton2 instance = null;
    // 2. 把构造方法设为 private
    private Singleton2() {}
    // 3. 提供一个方法来获取到上述单例的实例
    //    只有当真正需要用到这个 实例 的时候, 才会真正去创建这个实例.
    public static Singleton2 getInstance() {
        // 如果这个条件成立, 说明当前的单例未初始化过的, 存在线程安全风险, 就需要加锁~~
        if (instance == null) {
            synchronized (Singleton2.class) {
                if (instance == null) {
                    instance = new Singleton2();
                }
            }
        }
        return instance;
    }
}

public class Demo20 {
    public static void main(String[] args) {
        Singleton2 instance = Singleton2.getInstance();
    }
}