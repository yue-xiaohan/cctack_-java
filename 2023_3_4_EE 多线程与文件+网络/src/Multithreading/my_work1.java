package Multithreading;

public class my_work1 {
    static  int count=0;
    synchronized static void  increase(){
        count++;
    }

    public static void main(String[] args) throws InterruptedException {
        Thread t1=new Thread(() ->{
            for (int i = 0; i < 5_0000; i++) {
                increase();
            }
        });
        t1.start();
        Thread t2=new Thread(() ->{
            for (int i = 0; i < 5_0000; i++) {
                increase();
            }
        });
        t2.start();
        t1.join();

        t2.join();
        System.out.println(count);

    }
}
