package Multithreading;

import java.util.concurrent.locks.ReentrantLock;

public class Demo29 {
    public static void main(String[] args) {
        ReentrantLock locker = new ReentrantLock();
        // 加锁
        locker.lock();
        // 抛出异常了. 就容易导致 unlock 执行不到~~
        // 解锁
        locker.unlock();
    }
}