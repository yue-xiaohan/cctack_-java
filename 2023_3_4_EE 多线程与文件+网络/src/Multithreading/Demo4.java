package Multithreading;

public class Demo4 {
    public static void main(String[] args) {
        /*创建多线程的方式,使用匿名内部类,实现继承Thread的效果*/
        Thread t = new Thread() {
            @Override
            public void run() {
                System.out.println("hello thread");
            }
        };
        t.start();
        Thread t2=new Thread(){
            @Override
            public void run() {
                System.out.println("hello");
            }
        };
    }
}