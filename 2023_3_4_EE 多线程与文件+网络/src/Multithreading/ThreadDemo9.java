package Multithreading;

public class ThreadDemo9 {
    public static boolean isQuit = false;

    public static void main(String[] args) {
        // boolean isQuit = false;

        Thread t = new Thread(() -> {
            while (!isQuit) {
                System.out.println("hello t");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("t 线程终止");
        });

        t.start();

        // 在主线程中, 修改 isQuit
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        isQuit = true;
    }
}
