package Multithreading;

public class Demo8 {
    /*讲了创建别名的方式,和多线程的使用情况*/
    public static void main(String[] args) {
        Thread t1 = new Thread(() -> {
            while (true) {
                System.out.println("hello thread1");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "thread t1");
        t1.start();

        Thread t2 = new Thread(() -> {
            while (true) {
                System.out.println("hello thread2");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "thread t2");
        t2.start();
    }
}