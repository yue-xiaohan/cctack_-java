package Multithreading;

public class Work {

    public static void main(String[] args) throws InterruptedException {
        /*Thread[] threads = new Thread[20];
        for (int i = 0; i < 20; i++) {
            final int flag=i;
            threads[i]=new Thread(()->{
                System.out.println("第 "+flag+"线程");
            });
        }
        for (int i = 0; i < threads.length; i++) {
            threads[i].start();
            threads[i].join();
        }
        System.out.println("main:ok");*/
        Thread thread=new Thread(()->{
            System.out.println("Thread线程");
        },"TS");
        thread.start();
        System.out.println("获取线程对象"+Thread.currentThread());
        System.out.println("线程的别名"+thread.getName());
        System.out.println("线程优先级"+thread.getPriority());


    }
}
