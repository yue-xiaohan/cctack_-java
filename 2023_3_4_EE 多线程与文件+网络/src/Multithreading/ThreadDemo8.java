package Multithreading;

public class ThreadDemo8 {
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            while (true) {

            }
        });

        // 默认是前台线程, 也就是设为 false
        // 此时这个线程会阻止进程结束

        // 改成 true 变成后台线程. 不影响进程的结束.
        t.setDaemon(true);
        t.start();

    }
}
