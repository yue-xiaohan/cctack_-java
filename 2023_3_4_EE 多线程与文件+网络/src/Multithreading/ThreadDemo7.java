package Multithreading;

public class ThreadDemo7 {
    public static void main(String[] args) {
        Thread t = new Thread(() -> {
            System.out.println("hello t");
        });

        t.start();

        try {
            // 上述 t 线程没有进行任何循环和 sleep, 意味着里面的代码会迅速执行完毕.
            // main 线程如果 sleep 结束, 此时 t 基本上就是已经执行完了的状态. 此时 t 对象还在
            // 但是在 系统中 对应的线程已经结束了.
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(t.isAlive());
    }
}
