package Multithreading;

public class Demo7 {
    /*讲了,多线程与单线程实现过程的区别,效率的提升*/
    private static final long count = 100_0000_0000L;

    public static void serial() {
        // 记录程序执行时间.
        long beg = System.currentTimeMillis();
        long a = 0;
        for (long i = 0; i < count; i++) {
            a++;
        }
        long b = 0;
        for (long i = 0; i < count; i++) {
            b++;
        }
        long end = System.currentTimeMillis();
        System.out.println("消耗时间: " + (end - beg) + "ms");
    }

    public static void concurrency() throws InterruptedException {
        long beg = System.currentTimeMillis();
        Thread t1 = new Thread(() -> {
            long a = 0;
            for (long i = 0; i < count; i++) {
                a++;
            }
        });
        t1.start();
        Thread t2 = new Thread(() -> {
            long b = 0;
            for (long i = 0; i < count; i++) {
                b++;
            }
        });
        t2.start();

        // 此处不能直接这么记录结束时间. 别忘了, 现在这个求时间戳的代码是在 main 线程中.
        // main 和 t1 t2 之间是并发执行的关系, 此处 t1 和 t2 还没执行完呢, 这里就开始记录结束时间了. 这显然是不准确的.
        // 正确做法应该是让 main 线程等待 t1 和 t2 跑完了, 再来记录结束时间.
        // join 效果就是等待线程结束. t1.join 就是让 main 线程等待 t1 结束. t2.join 让 main 线程等待 t2 结束.
        t1.join();
        t2.join();
        long end = System.currentTimeMillis();
        System.out.println("消耗时间: " + (end - beg) + "ms");
    }

    public static void main(String[] args) throws InterruptedException {
         serial();
        concurrency();
    }
}