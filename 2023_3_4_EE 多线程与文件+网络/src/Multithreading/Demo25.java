package Multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Demo25 {
    public static void main(String[] args) {
        // 创建一个固定线程数目的线程池. 参数指定了线程个数
        //ExecutorService pool = Executors.newFixedThreadPool(10);
        // 创建一个自动扩容的线程池. 会根据任务量来自动进行扩容
        // ExecutorService pool= Executors.newCachedThreadPool();
        // 创建一个只有一个线程的线程池.
        // ExecutorService pool= Executors.newSingleThreadExecutor();
        // 创建一个带有定时器功能的线程池. 类似于 Timer
        ExecutorService pool=Executors.newScheduledThreadPool(10);

        for (int i = 0; i < 10; i++) {
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("第4种");
                }
            });
        }
    }
}