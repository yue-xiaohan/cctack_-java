package Multithreading;

// Runnable 就是在描述一个 "任务"
class MyRunnable implements Runnable {
    @Override
    public void run() {
        System.out.println("hello");
    }
}

public class Demo3 {
    /*创建多线程的方式,实现Runnable接口*/
    public static void main(String[] args) {
        Thread t = new Thread(new MyRunnable());
        t.start();
    }
}