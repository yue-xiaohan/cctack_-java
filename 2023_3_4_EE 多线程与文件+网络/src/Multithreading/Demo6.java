package Multithreading;

public class Demo6 {
    public static void main(String[] args) {
        /*创建多线程的方式,使用Lambert表达式*/
        Thread t = new Thread(() -> {
            System.out.println("hello thread");
        });
        t.start();
    }
}