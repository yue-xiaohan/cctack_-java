package Multithreading;

import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;

// 创建一个类, 表示一个任务.
class MyTask implements Comparable<MyTask> {
    //ctrl+o快捷生成构造方法
    // 任务具体要干啥
    private Runnable runnable;
    // 任务具体啥时候干. 保存任务要执行的毫秒级时间戳
    private long time;

    // delay 是一个时间间隔. 不是绝对的时间戳的值
    public MyTask(Runnable runnable, long delay) {
        this.runnable = runnable;
        this.time = System.currentTimeMillis() + delay;
    }

    public void run() {
        runnable.run();
    }

    public long getTime() {
        return time;
    }

    @Override
    public int compareTo(MyTask o) {
        // 到底是谁见谁, 才是一个时间小的在前? 需要咱们背下来.//
        return (int) (this.time - o.time);
    }
}

class MyTimer {
    // 定时器内部要能够存放多个任务
    private PriorityBlockingQueue<MyTask> queue = new PriorityBlockingQueue<>();

    public void schedule(Runnable runnable, long delay) {
        MyTask task = new MyTask(runnable, delay);
        queue.put(task);
        // 每次任务插入成功之后, 都唤醒一下扫描线程, 让线程重新检查一下队首的任务看是否时间到要执行~~
        synchronized (locker) {
            locker.notify();
        }
    }

    private Object locker = new Object();

    public MyTimer() {
        Thread t = new Thread(() -> {
            while (true) {
                try {
                    synchronized (locker) {
                        // 先取出队首元素
                        MyTask task = queue.take();
                        // 再比较一下看看当前这个任务时间到了没?
                        long curTime = System.currentTimeMillis();
                        if (curTime < task.getTime()) {
                            // 时间没到, 把任务再塞回到队列中.
                            queue.put(task);
                            // 指定一个等待时间
                           // synchronized (locker) {//这个锁加到这儿肯定是有问题的,但到底是什么问题呢?
                                /*关键在于多线程的调度是随机的无序的
                                 *可能当前线程还未走到锁这儿,就被另一个线程插入任务,唤醒就失败了
                                 * 此时有可能会导致新的任务无法顺利执行*/
                                locker.wait(task.getTime() - curTime);
                          //  }
                        } else {
                            // 时间到了, 执行这个任务
                            task.run();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }
}

public class Demo24 {
    public static void main(String[] args) {
        MyTimer myTimer = new MyTimer();
        myTimer.schedule(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello timer!");
            }
        }, 3000);
        System.out.println("main");
    }
}