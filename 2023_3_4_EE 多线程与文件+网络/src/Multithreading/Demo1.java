package Multithreading;

class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("hello thread");
    }
}

// 最基本的创建线程的办法.
public class Demo1 {
    public static void main(String[] args) {
        Thread t = new MyThread();
        t.start();
    }
}