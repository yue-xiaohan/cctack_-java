package Multithreading;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

class MyThreadPool2 {
    // 阻塞队列用来存放任务.
    private BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();

    public void submit(Runnable runnable) throws InterruptedException {
        queue.put(runnable);
    }

    // 此处实现一个固定线程数的线程池.
    public MyThreadPool2(int n) {
        for (int i = 0; i < n; i++) {
            Thread t = new Thread(() -> {
                try {
                    while (true) {
                        // 此处需要让线程内部有个 while 循环, 不停的取任务.
                        Runnable runnable = queue.take();
                        runnable.run();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            // 不要忘记, 启动线程.
            t.start();
        }
    }
}

public class ThreadDemo25 {
    public static void main(String[] args) throws InterruptedException {
        MyThreadPool2 pool = new MyThreadPool2(10);
        for (int i = 0; i < 1000; i++) {
            int number = i;
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("hello " + number);
                }
            });
        }

        Thread.sleep(3000);
    }
}
