package Multithreading;

public class Demo17 {
    /*讲述关键字wait的用法*/
    public static void main(String[] args) throws InterruptedException {
        Object object = new Object();
        synchronized (object) {
            System.out.println("wait 前");
            object.wait();
            System.out.println("wait 后");
        }
    }
}