package Multithreading;

// 把这个类设定成单例的~~
class Singleton1 {
    // 唯一实例的本体
    private static Singleton1 instance = new Singleton1();

    // 获取到实例的方法
    public static Singleton1 getInstance() {
        return instance;
    }

    // 禁止外部 new 实例.
    private Singleton1() { }
}

public class ThreadDemo17 {
    public static void main(String[] args) {
        // 此时 s1 和 s2 是同一个对象!!
        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        // Singleton s3 = new Singleton();
    }
}
