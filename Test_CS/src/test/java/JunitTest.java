import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.stream.Stream;

public class JunitTest {
    public static Stream<Arguments> Generator() {
        return Stream.of(Arguments.arguments(1, "张三"),
                Arguments.arguments(2, "李四"),
                Arguments.arguments(3, "王五")
        );
    }

    @Test
    void Test01() {
        System.out.println("这是JunitTest里面的Test01");
    }

    @Test
    void Test02() {
        System.out.println("这是JunitTest里面的Test02");
    }

    @Disabled
    void Test03() {
        WebDriver webDriver = new ChromeDriver();
        webDriver.get("https://www.baidu.com");
        webDriver.findElement(By.cssSelector("#s-top-left > a:nth-child(6)"));
    }

//    @BeforeAll
//    static void SetUp() {
//        System.out.println("这是我们BeforeAll里面的语句");
//    }
//
//    @AfterAll
//    static void TearDown() {
//        System.out.println("这是AfterAll的语句");
//    }

    @BeforeEach
    void BeforeEachTest() {
        System.out.println("这是BeforeEach里面的语句");
    }

    @AfterEach
    void AfterEachTest() {
        System.out.println("这是AfterEach里面的语句");
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3})
    void Test04(int num) {
        System.out.println(num);
    }

    @ParameterizedTest
    @ValueSource(strings = {"1", "2", "3"})
    void Test05(String number) {
        System.out.println(number);
    }

    @ParameterizedTest
    @CsvFileSource(resources = "test01.csv")
    void Test06(String name) {
        System.out.println(name);
    }

    @ParameterizedTest
    @MethodSource("Generator")
    void Test07(int num, String name) {
        System.out.println(num + ":" + name);
    }
}
