/**
 * @Author 12629
 * @Description：
 */
public class Test {
    public static void main(String[] args){
        Object o=new Object(){
            public boolean equals(Object obj){
                return true;
            }
        };
        System.out.println(o.equals("Fred"));
    }
    public String name="abc";
    public static void main3(String[] args){
        Test test=new Test();
        Test testB=new Test();
        System.out.println(test.equals(testB)+","+test.name.equals(testB.name));
    }
    public static void main2(String[] args) {
        int i=0;
        Integer j = new Integer(0);
        System.out.println(i==j);
        System.out.println(j.equals(i));
    }
    public static void main1(String[] args) {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.insert(12);
        binarySearchTree.insert(21);
        binarySearchTree.insert(5);
        binarySearchTree.insert(18);
        binarySearchTree.insert(9);
        binarySearchTree.inorder(binarySearchTree.root);
        System.out.println();
        BinarySearchTree.TreeNode ret = binarySearchTree.find(12);
        System.out.println(ret.val);
        System.out.println("============");

        binarySearchTree.remove(13);
        binarySearchTree.inorder(binarySearchTree.root);
        System.out.println();
    }
}