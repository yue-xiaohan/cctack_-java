package component.controller;

import component.Art.Articles;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class ArtInfo {
    @Bean("article") // 将当前方法返回的对象存储到 IoC 容器
    public Articles getArt(){
        // 伪代码
        Articles articleInfo = new Articles();
        articleInfo.setAid(1);
        articleInfo.setTitle("标题？");
        articleInfo.setContent("今天周一！！！");
        articleInfo.setCreatetime(LocalDateTime.now());
        return articleInfo;
    }

    public void sayHi(){
        System.out.println("Hi, Articles~");
    }
}
