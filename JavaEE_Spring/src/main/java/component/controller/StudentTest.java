package component.controller;


import component.Service.StuTest111;
import component.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class StudentTest {
    @Autowired
    public StuTest111 studentTest;
    public Student getUser(Integer id , String  name){
        return studentTest.getStudent(id,name);
    }

}
