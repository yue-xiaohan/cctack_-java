
import component.USER.User;
import component.controller.AATEST;
import component.controller.StudentTest;
import component.controller.UserBeans;
import component.controller.aCon;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class Main {

    public static void main11(String[] args) {
        BeanFactory context = new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        // ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        User user = context.getBean("aaa", User.class);

        System.out.println(user.toString());
    }
    public static void main5(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        User user =(User) context.getBean("user");

        user.toString();
    }
    public static void mai4(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        StudentTest studentTest =(StudentTest) context.getBean("studentTest");
        System.out.println(studentTest.getUser(1, "zhangsan"));


    }

    public static void main2(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        //JavaEE 第四节 Spring的读与取
        // 测试 首字母是小写的情况
        aCon a = context.getBean("aCon",aCon.class);
        System.out.println(a.f());

        // 测试 首字母和第二字母都是大写的情况
        AATEST aatest = context.getBean("AATEST",AATEST.class);
        System.out.println(aatest.f());
    }
    public static void main1(String[] args) {
        //1:先得到Spring 对象
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        /*第一种方式 - 如果是null 会报错
        * 第二种方式 - 如果存在多个user会报错
        * 第三种方式 -  日常最多使用的 ,不会有第一种和第二种的错误*/
//        Art art = context.getBean("art",Art.class);
//        System.out.println(art.f());
        //3:使用Bean (可选)
        //System.out.println(user.sayHi());
    }
}
