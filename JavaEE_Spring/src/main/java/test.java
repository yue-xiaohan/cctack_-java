import component.Art.Articles;
import component.controller.ArtInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class test {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Articles articles =context.getBean("articles",Articles.class);
        System.out.println(articles.toString());
    }
}
