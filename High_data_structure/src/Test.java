import avltree.AVLTree;
import rbtree.RBTree;
/**
 * @Author 12629
 * @Description：
 */
public class Test {
    public static void main2(String[] args) {
        int[] array = {4, 2, 6, 1, 3, 5, 15, 7, 16,14};
        RBTree rbTree = new RBTree();
        for (int i = 0; i < array.length; i++) {
            rbTree.insert(array[i]);
        }
        System.out.println(rbTree.isRBTree());
        rbTree.inorder(rbTree.root);
    }
    public static void main1(String[] args) {
        int[] array = {4, 2, 6, 1, 3, 5, 15, 7, 16,14};
        //int[] array = {30,20,90,60,180,40};
        AVLTree avlTree = new AVLTree();
        for (int i = 0; i < array.length; i++) {
            avlTree.insert(array[i]);
        }
        System.out.println(avlTree.isBalanced(avlTree.root));
    }
}