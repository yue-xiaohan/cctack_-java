package rbtree;

/**
 * @Author 12629
 * @Description：
 */
public class RBTree {

    static class RBTreeNode {
        public RBTreeNode left ;
        public RBTreeNode right;
        public RBTreeNode parent;
        public int val;
        public COLOR color;

        public RBTreeNode(int val) {
            this.val = val;
            //我们新创建的节点，颜色默认是红色的.为什么？
            this.color = COLOR.RED;
        }
    }

    public RBTreeNode root;

    public boolean insert(int val) {
        RBTreeNode node = new RBTreeNode(val);
        if (root == null) {
            root = node;
            root.color = COLOR.BLACK;
            return true;
        }

        RBTreeNode parent = null;
        RBTreeNode cur = root;
        while (cur != null) {
            if (cur.val < val) {
                parent = cur;
                cur = cur.right;
            } else if (cur.val == val) {
                return false;
            } else {
                parent = cur;
                cur = cur.left;
            }
        }
        //cur == null
        if (parent.val < val) {
            parent.right = node;
        } else {
            parent.left = node;
        }
        //
        node.parent = parent;
        cur = node;
        //红黑树来说：就需要调整颜色

        while (parent != null && parent.color == COLOR.RED) {
            RBTreeNode grandFather = parent.parent;//这个引用不可能为空
            if(parent == grandFather.left) {
                RBTreeNode uncle = grandFather.right;
                if(uncle != null && uncle.color == COLOR.RED) {
                    parent.color = COLOR.BLACK;
                    uncle.color = COLOR.BLACK;
                    grandFather.color = COLOR.RED;
                    //继续向上修改
                    cur = grandFather;
                    parent = cur.parent;
                }else {

                    //uncle不存在 或者 uncle是黑色的
                    //情况三：
                    if(cur == parent.right) {
                        rotateLeft(parent);
                        RBTreeNode tmp = parent;
                        parent = cur;
                        cur = tmp;
                    }//情况三  变成了情况二

                    //情况二
                    rotateRight(grandFather);
                    grandFather.color = COLOR.RED;
                    parent.color = COLOR.BLACK;
                }
            }else {
                //parent == grandFather.right
                RBTreeNode uncle = grandFather.left;
                if(uncle != null && uncle.color == COLOR.RED) {
                    parent.color = COLOR.BLACK;
                    uncle.color = COLOR.BLACK;
                    grandFather.color = COLOR.RED;
                    //继续向上修改
                    cur = grandFather;
                    parent = cur.parent;
                }else {
                    //uncle不存在 或者 uncle是黑色的
                    //情况三：
                    if(cur == parent.left) {
                        rotateRight(parent);
                        RBTreeNode tmp = parent;
                        parent = cur;
                        cur = tmp;
                    }//情况三  变成了情况二
                    //情况二
                    rotateLeft(grandFather);
                    grandFather.color = COLOR.RED;
                    parent.color = COLOR.BLACK;
                }
            }
        }
        root.color = COLOR.BLACK;
        return true;
    }

    /**
     * 左单旋
     * @param parent
     */
    private void rotateLeft(RBTreeNode parent) {
        RBTreeNode subR = parent.right;
        RBTreeNode subRL = subR.left;

        parent.right = subRL;

        subR.left = parent;
        if(subRL != null) {
            subRL.parent = parent;
        }

        RBTreeNode pParent = parent.parent;
        parent.parent = subR;

        if(root == parent) {
            root = subR;
            root.parent = null;
        }else {
            if(pParent.left == parent) {
                pParent.left = subR;
            }else {
                pParent.right = subR;
            }
            subR.parent = pParent;
        }
    }

    /**
     * 右单旋
     * @param parent
     */
    private void rotateRight(RBTreeNode parent) {

        RBTreeNode subL = parent.left;
        RBTreeNode subLR = subL.right;

        parent.left = subLR;
        subL.right = parent;
        //没有subLR
        if(subLR != null) {
            subLR.parent = parent;
        }
        //必须先记录
        RBTreeNode pParent = parent.parent;
        parent.parent = subL;
        //检查 当前是不是就是根节点
        if(parent == root) {
            root = subL;
            root.parent = null;
        }else {
            //不是根节点，判断这棵子树是左子树还是右子树
            if(pParent.left == parent) {
                pParent.left = subL;
            }else {
                pParent.right = subL;
            }
            subL.parent = pParent;
        }
    }

    /**
     * 判断当前树 是不是红黑树
     *  得满足 红黑树的性质
     * @return
     */
    public boolean isRBTree() {
        if(root == null) {
            //如果一棵树是空树，那么这棵树就是红黑树
            return true;
        }

        if(root.color != COLOR.BLACK) {
            System.out.println("违反了性质：根节点必须是黑色的！");
        }

        //存储当前红黑树当中 最左边路径的黑色的节点个数
        int blackNum = 0;
        RBTreeNode cur = root;
        while (cur != null) {
            if(cur.color == COLOR.BLACK) {
                blackNum++;
            }
            cur = cur.left;
        }
        //检查是否存在两个连续的红色节点  && 每条路径上黑色的节点的个数是一致的
        return checkRedColor(root) && checkBlackNum(root,0,blackNum);
    }

    /**
     *
     * @param root
     * @param pathBlackNum 每次递归的时候，计算黑色节点的个数  0
     * @param blackNum 事先计算好的某条路径上的黑色节点的个数   2
     * @return
     */
    private boolean checkBlackNum(RBTreeNode root,int pathBlackNum,int blackNum) {
        if(root == null) return true;
        if(root.color == COLOR.BLACK) {
            pathBlackNum++;
        }
        if(root.left == null && root.right == null) {
            if(pathBlackNum != blackNum) {
                System.out.println("违反了性质：每条路径上黑色的节点个数是不一样的！");
                return false;
            }
        }
        return checkBlackNum(root.left,pathBlackNum,blackNum) &&
                checkBlackNum(root.right,pathBlackNum,blackNum);
    }

    /**
     * 检查是否存在两个连续的红色节点
     * @param root
     * @return
     */
    private boolean checkRedColor(RBTreeNode root) {
        if(root == null) return true;
        if(root.color == COLOR.RED) {
            RBTreeNode parent = root.parent;
            if(parent.color == COLOR.RED) {
                System.out.println("违反了性质：连续出现了两个红色的节点");
                return false;
            }
        }
        return checkRedColor(root.left) && checkRedColor(root.right);
    }

    public void inorder(RBTreeNode root) {
        if(root == null) {
            return;
        }
        inorder(root.left);
        System.out.print(root.val+" ");
        inorder(root.right);
    }

}