package bitsetdemo;

import java.util.BitSet;
public class Test {

    public static void main3(String[] args) {
        int[] array = {1,3,2,13,10,3,14,18,3};
        MyBitSet myBitSet = new MyBitSet(18);
        for (int i = 0; i < array.length; i++) {
            myBitSet.set(array[i]);
        }

        for (int i = 0; i < myBitSet.elem.length; i++) {
            for (int j = 0; j < 8; j++) {
                if((myBitSet.elem[i] & (1 << j) ) != 0 ) {
                    System.out.println(i*8+j);
                }
            }
        }
    }

    public static void main2(String[] args) {
        int[] array = {1,2,3,10,4,18,130};
        MyBitSet myBitSet = new MyBitSet(18);
        for (int i = 0; i < array.length; i++) {
            myBitSet.set(array[i]);
        }
        System.out.println(myBitSet.getUsedSize());
        System.out.println(myBitSet.get(10));
        myBitSet.reSet(10);
        System.out.println(myBitSet.get(10));
    }

    public static void main1(String[] args) {
        int[] array = {1,2,3,10,4,18,13};
        BitSet bitSet = new BitSet(18);
        for (int i = 0; i < array.length; i++) {
            bitSet.set(array[i]);
        }
        System.out.println(bitSet.get(100));
    }
}