 class Student {
    private String name;//属于对象变量、
    private int age;
    public static String classname="网络2001";

    public static void setClassname(){
       Student.classname ="456";
    }
    public static void Print(){

        System.out.println("asdqwe");

    }
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void doClass(){
       // Print();//可以直接调用静态方法
       // classname="123";
        System.out.println("姓名"+name+"年龄"+age+"班级名"+classname);
    }

}
public class Test01 {
    public static void main(String[] args) {
        Student student=new Student("张三",20);
        student.doClass();
        Student.setClassname();
        student.doClass();
    }
    public static void main2(String[] args) {
        System.out.println(Student.classname);
        //说明静态的成员不属于对象
        Student student=null;//能访问，但会被打的吧？
        System.out.println(student.classname);

    }
    public static void main1(String[] args) {
        Student student=new Student("张三",20);
        Student student1=new Student("李四",20);
        Student student2=new Student("王五",20);
        student.doClass();
        student1.doClass();
        student2.doClass();
        //System.out.println("");
        System.out.println(student.classname);
    }
}
