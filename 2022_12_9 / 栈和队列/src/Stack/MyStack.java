package Stack;

import java.util.Arrays;

public class MyStack {
    public int [] elem;
    public int usedSize;
    public MyStack() {
        this.elem=new int[5];
    }
    public MyStack(int[] elem, int usedSize) {
        this.elem = elem;
        this.usedSize = usedSize;
    }
    public void push(int val){
        if (isFull()){
            this.elem= Arrays.copyOf(this.elem,2*this.elem.length);
        }
        this.elem[this.usedSize]=val;
        usedSize++;
    }
    public boolean isFull(){
        return this.usedSize ==this.elem.length;
    }
    public int pop(){
        if (isEmpty()){
            throw new RuntimeException("栈为空");
        }
        int oldVal=this.elem[usedSize-1];
        usedSize--;
        return oldVal;
    }

    public int peek(){
        if (isEmpty()){
            throw new RuntimeException("栈为空");
        }
        int oldVal=this.elem[usedSize-1];
        return oldVal;
    }
    public boolean isEmpty(){
        return this.usedSize == 0;
    }
}
