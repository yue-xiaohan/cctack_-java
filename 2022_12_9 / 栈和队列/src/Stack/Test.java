package Stack;

import java.util.Stack;

public class Test {
    public static void main(String[] args) {
        MyStack stack=new MyStack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        System.out.println(stack.pop());//弹出栈顶元素，并且删除
        System.out.println(stack.peek());//弹出栈顶元素，但不删除
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());//测试此堆栈是否为空。
    }
}
