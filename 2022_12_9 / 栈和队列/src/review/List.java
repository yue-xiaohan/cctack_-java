package review;

public class List {
    //复习链表，
    //考的链表都是不带头的，非循环的单链表
    class ListNode {
        public int val;
        public ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }
    public ListNode head;

    //头插法
    public void addFirst(int data){
        ListNode cur=new ListNode(data);
        cur.next= head;
        head =cur;
    }

    //尾插法
   public void addLast(int data) {
       ListNode cur=new ListNode(data);
       if (head==null){
          head=cur;
          return;
       }
       ListNode tmp=head;
       while (tmp.next!=null){
           tmp= tmp.next;
       }
       tmp.next=cur;
   }
}
