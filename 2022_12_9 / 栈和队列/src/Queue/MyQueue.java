package Queue;

class Node{
    public int val;
    public Node next;
    public Node(int val){
        this.val=val;
    }
}

public class MyQueue {
    public Node head;
    public Node last;

    public void offer(int val){
        Node node=new Node( val);
        if (head == null){
            head=node;
            last=node;
        }
        else {
            last.next=node;
            last=last.next;//尾插法
        }

    }
    public  int poll(){
        if (isEmpty()){
            throw new RuntimeException("队列为空");
        }
        int oldval= head.val;
        this.head=head.next;
        return oldval;

    }
    public boolean isEmpty(){
        return this.head == null;
    }
    public int peek(){
        if (isEmpty()){
            throw new RuntimeException("队列为空");
        }
        return head.val;
    }
}
