package Queue;

class MyCircularQueue {
    public int[] elem;
    public int front;//队头下标
    public int rear;//队尾下标


    public MyCircularQueue(int k) {
        this.elem=new int[k+1];//因为用的是第三种判断的，用标志或size没有这个额外空间的问题
    }

    public boolean enQueue(int value) {
        //入队
        if (isFull())return false;

        this.elem[rear]=value;
        rear=(rear+1)%elem.length;
        return true;
    }

    public boolean deQueue() {
        //出队
        if (isEmpty())return false;

        front=(front+1)%elem.length;
        return true;

    }

    public int Front() {
        //拿队头元素
        if (isEmpty()){
            return -1;
        }
        return elem[front];
    }

    public int Rear() {
        //拿队尾元素
        if (isEmpty()){
            return -1;
        }
        int index=-1;
        if (rear == 0){
            index=elem.length-1;
        }
        else{
            index=rear-1;
        }
        return elem[index];
    }

    public boolean isEmpty() {
        return front==rear;
    }

    public boolean isFull() {
        if ((this.rear+1)%elem.length == front){
            return true;
        }
        return false;
    }
}
