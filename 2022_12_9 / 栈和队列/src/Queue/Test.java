package Queue;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Test {
    public static void main(String[] args) {
        MyQueue myQueue=new MyQueue();
        myQueue.offer(1);
        myQueue.offer(2);
        myQueue.offer(3);
        System.out.println(myQueue.peek());
        System.out.println(myQueue.poll());
        System.out.println(myQueue.poll());
        System.out.println(myQueue.poll());

    }
    public static void main1(String[] args) {
        Queue<Integer> queue=new LinkedList<>();//普通队列，一端进，一遍出
        Deque<Integer> deque=new LinkedList<>();//双端队列，一端不仅可以进还可以出。
        queue.add(1);//会抛出异常
        queue.offer(2);//都是插入，。但是offer不会抛出异常
        System.out.println(queue.peek());
    }
}
