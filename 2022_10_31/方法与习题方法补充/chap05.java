import java.security.Key;
import java.util.Scanner;

public class chap05 {
    //方法
    //方法的调用是在栈上的，当遇到return和右花括号就说明方法结束了，对应的方法的栈就被回收了。



    //**************************
    public  static  int max1(int a,int b){
        return a>b?a:b;

    }
    public  static  float max1(float a,float b,float c){
        float d=a>b?a:b;
        return  d>c?d:c;

    }
    public static void main12(String[] args) {//12
        Scanner sc=new Scanner(System.in);
        int a=10,b=20;
        float a1=5f;
        float b1=6f;
        float c1=7f;
        System.out.println("俩个整数的方法"+max1(a,b));
        System.out.println("三个小数的方法"+max1(a1,b1,c1));


    }




    public static  int  Add(int a,int b){
        return  a+b;

    }
    public static  float  Add(float a,float b,float c){

        return a+b+c;
    }
    public static void main11(String[] args) {//11
        Scanner sc=new Scanner(System.in);
        int a=10,b=20;
        float a1=5f;
        float b1=6f;
        float c1=7f;
        System.out.println("俩个整数的方法"+Add(a,b));
        System.out.println("三个小数的方法"+Add(a1,b1,c1));


    }

    //***************************************************
    public static int   Fibonacci(int n){
        int a=1,b=1;
        int c=0;
        if(n<=2)
        {
            return 1;
        }
        while(n>2){
            c=a+b;
            a=b;
            b=c;
            n--;
        }
        return c;
    }
    public static void main10(String[] args) {//10
        //求斐波那契数列的第n项。(迭代实现)
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int c= Fibonacci(n);
        System.out.println("第n项的值为："+c);

    }
    //***************************************************
    public static int  Js4(){
        int a=0,b=0,c=0;
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入三个带比较的数字");
        a= sc.nextInt();
        b=sc.nextInt();
        c= sc.nextInt();
        int d=Max(a,b);
        return c>d?c:d;

    }
    public static int Max(int a,int b){
        return a>b?a:b;
    }
    public static void main9(String[] args) {
        //创建方法求两个数的最大值max2，随后再写一个求3个数的最大值的函数max3。
        //要求：在max3这个函数中，调用max2函数，来实现3个数的最大值计算
        int a=Js4();
        System.out.println("三个数中的最大值为"+a);
    }
    //***************************************************
    public static void Js3(int key){
        for(int i=1;i<= key;i++)
        {
            for(int j=1;j<=i ;j++)
            {
                System.out.printf("%d*%d=%d  ",i,j,i*j);
            }
            System.out.println();
        }
    }
    public static void main8(String[] args) {
        //输出n*n的乘法口诀表，n由用户输入。
        Scanner sc=new Scanner(System.in);
        int n= sc.nextInt();
        Js3(n);

    }
    //***************************************************
    public  static void Js2(String key){//实现字符串
        Scanner sc=new Scanner(System.in);
        StringBuffer Key=new StringBuffer();
        int count=3;
       while(count!=0)
       {
           String password=sc.nextLine();
           if(password.equals(key))
           {
               System.out.println("登录成功");
           }
           else
           {
               System.out.println("密码错误，请重新输入");
               count--;
           }
       }
    }
    public static void Js2(int key){//整数类型
        Scanner sc=new Scanner(System.in);
        int i=0;
        while(sc.hasNext()){
            int n= sc.nextInt();
            if(n==key){
                System.out.println("登录成功");
            }else{
                System.out.println("密码错误，请重新输入");
            }

            if (i==2){
                System.out.println("你已经输错三次，程序退出");
                break;
            }
            i++;
        }
    }
    public static void main7(String[] args) {
        //7
        //编写代码模拟三次密码输入的场景。 最多能输入三次密码，写的没用字符串
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入原始密码");
        int Key=sc.nextInt();
        Js2(Key);
    }
    //******************************************************

    public static void Js1(){
        Scanner sc=new Scanner(System.in);
        int n= sc.nextInt();
        int s=0;
        while(n!=0)
        {
            s=n%10;
            System.out.println(s);
            n/=10;
        }
    }
    public static void main6(String[] args) {
        //输出一个整数的每一位，如：123的每一位是3，2，1
        Js1();

    }


//******************************************************
    public static void Js(){
        int a=1;
        double j=1.0;
        double sum=0;
        for(int i=1;i<= 100;i++)
        {
           sum+= (j/i)*a;
           a*=-1;//也可写a=-a;
        }
        System.out.println(sum);
    }
    public static void main5(String[] args) {
        //计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值 。
        Js();
    }

//******************************************************


    //******************************************
    //以下板书，以上作业
    //******************************************

//******************************************************

    public static int sum(int a,int b)
    {
        return  a+b;
    }
    public static float sum(float a,float b){

        return a+b ;
    }
    public static int  sum(int a,int b,int c){

        return a+b+c ;
    }
    public static void main4(String[] args) {
        int a=10;
        int b=20;
        System.out.println(sum(a,b));
    }


//******************************************************

    public  static  void  swap(int a,int b){
        int temp=a;
        a=b;
        b=temp;
    }
    public static void main3(String[] args) {
        int a=10;
        int b=20;
        swap(a,b);//值传递
        System.out.println("swap值传递");
        System.out.println(a);
        System.out.println(b);
        System.out.println("swap引用传递");//需要



    }

//******************************************************

    public static  int facNum(int num){
        int sum=0;
        for(int j=1;j<= num;j++){
            sum+=fac(j);
        }
        return  sum;
    }
    public  static  int fac(int n){
        int ret=1;
        for(int i=1; i<= n;i++) {
            ret*=i;
        }
        return  ret;
    }
    public static void main2(String[] args) {
        int sum=0;
       sum=facNum(3);
        System.out.println(sum);
    }

//******************************************************


    public static int add(int a,int b){

        return  a+b;
    }
    public static void main1(String[] args) {
        //方法作用
        int a=10;
        int b=20;
        int ret1=add(1,2);
        System.out.println(ret1);

    }
}
