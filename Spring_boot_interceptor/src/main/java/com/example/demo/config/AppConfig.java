package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration//随着系统启动而启动
public class AppConfig implements WebMvcConfigurer { //该类的类名无所谓
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())//添加拦截器
                .addPathPatterns("/**") // 拦截所有请求
                .excludePathPatterns("/user/login") // 排除的url地址（不拦截的url地址）
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/**/*.html");

    }

//    扩展给所有请求地址增加api前缀 : 在做路由匹配 或反向代理的时候需要用
//    @Override
//    public void configurePathMatch(PathMatchConfigurer configurer) {
//        configurer.addPathPrefix("/zhangsan", c -> true);
//    }
}
