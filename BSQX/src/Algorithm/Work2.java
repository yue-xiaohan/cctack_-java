package Algorithm;

public class Work2 {
    /*字符串相关的题型*/
    /*替换空格
    解决思路：
    //虽然是替换问题，但是生成的字符串整体变长了.
    //因替换内容比被替换内容长，所以，一定涉及到字符串中字符的移动问题
    //移动方向一定是向后移动，所以现在的问题无非是移动多少的问题
    //因为是 ' ' -> "%20",是1换3，所以可以先统计原字符串中空格的个数(设为n)，然后可以计算出新字符串的长度
    //所以：new_length = old_length + 2*n
    //最后，定义新老索引（或者指针），各自指向新老空间的结尾，然后进行old->new的移动
    //如果是空格，就连续放入“%20”,其他平移即可*/
    public String replaceSpace(StringBuffer str) {
        int count = 0;
        for(int i = 0; i < str.length(); i++){
            if(str.charAt(i) == ' '){
                count++;
            }
        }
        int new_length = str.length() + 2*count;
        int old_end = str.length() - 1; //索引老字符串最后一个有效位置
        int new_end = new_length - 1; //索引新字符串最后一个有效位置
        str.setLength(new_length); //设置字符串新大小，防止越界
        while(old_end >= 0 && new_end >= 0){
            if(str.charAt(old_end) == ' '){
                //当前位置是空格
                str.setCharAt(new_end--, '0');
                str.setCharAt(new_end--, '2');
                str.setCharAt(new_end--, '%');
                --old_end;
            } else{
                //当前位子不是空格，平移即可
                str.setCharAt(new_end--, str.charAt(old_end));
                --old_end;
            }
        } return str.toString();
    }


}
