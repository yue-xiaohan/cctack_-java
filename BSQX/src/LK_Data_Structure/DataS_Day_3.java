package LK_Data_Structure;

import java.util.*;
import java.util.HashMap;

public class DataS_Day_3 {
    /*. 两个数组的交集 II
    * 给你两个整数数组 nums1 和 nums2 ，请你以数组形式返回两数组的交集。返回结果中每个元素出现的次数，
    * 应与元素在两个数组中都出现的次数一致（如果出现次数不一致，则考虑取较小值）。可以不考虑输出结果的顺序。*/
    public int[] intersect(int[] nums1, int[] nums2) {
        if (nums1.length > nums2.length) {
            return intersect(nums2, nums1);
        }
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums1) {
            int count = map.getOrDefault(num, 0) + 1;
            map.put(num, count);
        }
        int[] intersection = new int[nums1.length];
        int index = 0;
        for (int num : nums2) {
            int count = map.getOrDefault(num, 0);
            if (count > 0) {
                intersection[index++] = num;
                count--;
                if (count > 0) {
                    map.put(num, count);
                } else {
                    map.remove(num);
                }
            }
        }
        return Arrays.copyOfRange(intersection, 0, index);
    }
    /* 买卖股票的最佳时机
    * 给定一个数组 prices ，它的第 i 个元素 prices[i] 表示一支给定股票第 i 天的价格。
    你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。
    * 设计一个算法来计算你所能获取的最大利润。*/
    /*官方题解:思我们只要用一个变量记录一个历史最低价格 minprice，我们就可以假设自己的股票是在那天买的。那么我们在第 i 天卖出股票能得到的利润就是 prices[i] - minprice
    在每一天得到的都是局部最优解.:说到底还是动态规划。
    */
    public int maxProfit1(int prices[]) {
        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice) {
                minprice = prices[i];
            } else if (prices[i] - minprice > maxprofit) {
                maxprofit = prices[i] - minprice;
            }
        }
        return maxprofit;
    }
    /*我写的代码:核心逻辑也是利用找到最小和最大,但是为什么.哦
    * 我明白了,应该找到的差额最大,我这是单方面的最小和最大,所以测试案例不能全部通过*/
    public int maxProfit(int[] prices) {
        if (prices == null){
            return 0;
        }
        boolean[] pricesFlag=new boolean[prices.length];
        int min=prices.length-1;
        int max=0;
        int max_index=0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[min] > prices[i]){
                min=i;
            }

        }
        for (int i = 0; i < prices.length; i++) {
            if (i <= min){
                pricesFlag[i]=true;
            }
            if (max <= prices[i] && pricesFlag[i] == false){
                max=prices[i];
                max_index=i;
            }

        }
        if (!pricesFlag[max_index]){
            return prices[max_index]-prices[min];
        }
        return 0;
    }

}
