package LK_Data_Structure;
import java.util.*;
public class DataS__Day_1 {
    /* 存在重复元素
    * 给你一个整数数组 nums 。如果任一值在数组中出现 至少两次 ，
    * 返回 true ；如果数组中每个元素互不相同，返回 false */
    public boolean containsDuplicate(int[] nums) {
        //解法1:排序,排完序后,重复的俩个元素一定是相邻的
        Arrays.sort(nums);
        for (int i = 0; i < nums.length-1; i++) {
            if (nums[i] == nums[i + 1]) {
                return true;
            }
        }
        return false;
    }
    public boolean containsDuplicate2(int[] nums) {
        //解法2:哈希表,利用哈希表来看是否存在过
        HashMap<Integer,Integer> map=new HashMap<>();
        for (int i=0; i < nums.length;i++){
            if (!map.containsKey(nums[i])){
                map.put(nums[i],1);
            }else {
                return true;
            }
        }
        return false;
    }
    /* 最大子数组和
    * 给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。*/
    public int maxSubArray(int[] nums) {
//        解题思路:动态规划
        /*动态规划的解题方法:1定义状态,2编写状态转移方程,3设置初始值
        * f(n) =f(n-1)+num[i]/num[i]*/
        int[] dp=new int[nums.length];
        dp[0]=nums[0];
        int res = dp[0];
        for (int i = 1; i < nums.length; i++) {
            if (dp[i - 1] > 0) {
                dp[i] = dp[i - 1] + nums[i];
            } else {
                dp[i] = nums[i];
            }
            if (res < dp[i]){
                res=dp[i];
            }
        }
        return res;
    }
}
