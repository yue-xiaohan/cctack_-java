package LK_Data_Structure;

public class DataS_Day_8 {
    /*反转链表
        头插法*/
    public ListNode reverseList(ListNode head) {
        //如果头结点为空返回空
        if (head==null)return null;

        ListNode cur=head.next;
        head.next=null;
        while (cur!=null){
            ListNode tmp=cur.next;
            cur.next=head;
            head=cur;
            cur=tmp;
        }
        return head;
    }
    /*删除链表中的所有  重复元素*/
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null) {
            return head;
        }
        ListNode cur = head;
        while (cur.next != null) {
            if (cur.val == cur.next.val) {
                cur.next = cur.next.next;
            } else {
                cur = cur.next;
            }
        }
        return head;
    }
}
