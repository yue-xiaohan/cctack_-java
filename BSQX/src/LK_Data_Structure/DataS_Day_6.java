package LK_Data_Structure;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;

public class DataS_Day_6 {
    /*给定一个字符串 s ，找到 它的第一个不重复的字符，并返回它的索引 。如果不存在，则返回 -1 。*/
    public int firstUniqChar(String s) {
        if(s==null)return -1;
        int[] arr=new int[26];
        for (int i = 0; i < s.length(); i++) {
            arr[s.charAt(i)-97]++;
        }
        for (int i = 0; i < s.length(); i++) {
            if (arr[s.charAt(i)-'a']==1){
                return i;
            }
        }
        return -1;

    }
    /*给你两个字符串：ransomNote 和 magazine ，判断 ransomNote 能不能由 magazine 里面的字符构成。*/

    public boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) {
            return false;
        }
        int[] cnt = new int[26];
        for (char c : magazine.toCharArray()) {
            cnt[c - 'a']++;
        }
        for (char c : ransomNote.toCharArray()) {
            cnt[c - 'a']--;
            if(cnt[c - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }
    /*给定两个字符串 s 和 t ，编写一个函数来判断 t 是否是 s 的字母异位词。
      注意：若 s 和 t 中每个字符出现的次数都相同，则称 s 和 t 互为字母异位词。*/
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()){
            return false;
        }
        int[] arr=new int[26];
        for (int i = 0; i < s.length(); i++) {
            char ch=s.charAt(i);
            arr[ch-'a']++;
        }
        for (int i = 0; i < t.length(); i++) {
            char ch=t.charAt(i);
            arr[ch - 'a']--;
            if ( arr[ch - 'a'] < 0){
                return false;
            }
        }
        for (int i:arr) {
            if (i != 0){
                return false;
            }
        }
        return true;
    }

}
