package LK_Data_Structure;

import java.util.Stack;

public class DataS_Day_9 {
    /* 有效的括号*/
    public boolean isValid(String s) {
        if (s==null)return false;
        Stack<Character> stack=new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i)=='('){
                stack.push(')');
            }else if(s.charAt(i)=='['){
                stack.push(']');
            }else if(s.charAt(i)=='{') {
                stack.push('}');
            }else if(stack.isEmpty()||s.charAt(i)!=stack.pop())return false;
        }
        return stack.isEmpty();
    }
}
/*用栈实现队列*/
class MyQueue {
    /*请你仅使用两个栈实现先入先出队列。队列应当支持一般队列支持的所有操作（push、pop、peek、empty）：*/
    Stack<Integer> stack1;
    Stack<Integer> stack2;
    public MyQueue() {
        stack1=new Stack<>();
        stack2=new Stack<>();
    }
    public void push(int x) {
        stack1.push(x);
    }

    public int pop() {
        if (empty()){
            return -1;
        }
        while (!stack1.isEmpty()){
            stack2.push(stack1.pop());
        }
        int k=stack2.pop();
        while (!stack2.isEmpty()){
            stack1.push(stack2.pop());
        }
        return k;
    }

    public int peek() {
        if (empty()){
            return -1;
        }
        while (!stack1.isEmpty()){
            stack2.push(stack1.pop());
        }
        int k=stack2.peek();
        while (!stack2.isEmpty()){
            stack1.push(stack2.pop());
        }
        return k;
    }

    public boolean empty() {
        return stack1.isEmpty()&& stack2.isEmpty();
    }
}
