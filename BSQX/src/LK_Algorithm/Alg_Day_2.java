package LK_Algorithm;

import java.util.Arrays;

public class Alg_Day_2 {
    /* 轮转数组
    * 给定一个整数数组 nums，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
    * 解题思路:利用移动次数求余,然后复制一个数组用来保存值*/
    public void rotate(int[] nums, int k) {
        int n = nums.length;
        int[] newArr = Arrays.copyOf(nums,nums.length);
        for (int i = 0; i < n; ++i) {
            nums[(i + k) % n] = newArr[i];
        }
    }
    /* 有序数组的平方
    * 给你一个按 非递减顺序 排序的整数数组 nums，返回 每个数字的平方 组成的新数组，要求也按 非递减顺序 排序。
      解题思路:1,先平方后排序,时间复杂度取决于排序的时间复杂度*/
    public int[] sortedSquares(int[] nums) {
        if (nums == null){
            return null;
        }
        for (int i = 0; i < nums.length; i++) {
            nums[i]=nums[i]*nums[i];
        }
        Arrays.sort(nums);
        return nums;
    }
    /*双指针*/
    public int[] sortedSquares1(int[] nums) {
        int n = nums.length;
        int[] ans = new int[n];
        for (int i = 0, j = n - 1, pos = n - 1; i <= j;) {
            if (nums[i] * nums[i] > nums[j] * nums[j]) {
                ans[pos] = nums[i] * nums[i];
                ++i;
            } else {
                ans[pos] = nums[j] * nums[j];
                --j;
            }
            --pos;
        }
        return ans;
    }




}
