package LK_Algorithm;

public class Alg_Day_1 {
    /* 二分查找
    * 给定一个 n 个元素有序的（升序）整型数组 nums 和一个目标值 target  ，
    * 写一个函数搜索 nums 中的 target，如果目标值存在返回下标，否则返回 -1。
     */
    public int search(int[] nums, int target) {
        if (nums == null){
            return -1;
        }
        int i=0;
        int j=nums.length-1;
        while (i <= j){
            int mid = (j - i) / 2 + i;
            if (nums[mid] == target){
                return  mid;
            } else if (nums[mid] > target){
                j=mid-1;
            }else {
                i=mid+1;
            }
        }
        return -1;
    }
    /* 第一个错误的版本*/
    /*public int firstBadVersion(int n) {
        int left=1;
        int right=n;
        while (left < right){
            int mid=(right-left)/2+left;
            if (isBadVersion(mid)) {
                right = mid; // 答案在区间 [left, mid] 中
            } else {
                left = mid + 1; // 答案在区间 [mid+1, right] 中
            }
        }
        return left;
    }*/
    /*搜索插入位置
    * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。
    * 如果目标值不存在于数组中，返回它将会被按顺序插入的位置。*/
    public static int searchInsert(int[] nums, int target) {
        //二分查找
        int left=0;
        int right=nums.length-1;
        int mid=0;
        while (left<=right){
            mid=(right-left)/2+left;
            if (nums[mid]==target){
                return mid;
            }else if (nums[mid]>target){
                right=mid-1;
            }else {
                left=mid+1;
            }
        }
        if (nums[mid]<target){
            return mid+1;
        }else {
            return mid;
        }
    }
}
