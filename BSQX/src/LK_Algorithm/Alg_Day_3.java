package LK_Algorithm;

public class Alg_Day_3 {
    /*给定一个数组 nums，编写一个函数将所有 0 移动到数组的末尾，同时保持非零元素的相对顺序。
    * 解题思路:
    *   左指针左边均为非零数；:左指针每次指向当前最近的0位置(离数组开始)
        右指针左边直到左指针处均为零。:右指针每次指向最近的非0元素
        因此每次交换，都是将左指针的零与右指针的非零数交换，且非零数的相对顺序并未改变。*/

    public void moveZeroes(int[] nums) {
        int n = nums.length, left = 0, right = 0;
        while (right < n) {
            if (nums[right] != 0) {
                swap(nums, left, right);
                left++;
            }
            right++;
        }
    }

    public void swap(int[] nums, int left, int right) {
        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }
    /*给你一个下标从 1 开始的整数数组 numbers ，该数组已按 非递减顺序排列  ，
    请你从数组中找出满足相加之和等于目标数 target 的两个数。如果设这两个数分别是 numbers[index1] 和 numbers[index2] ，
    则 1 <= index1 < index2 <= numbers.length 。
    以长度为 2 的整数数组 [index1, index2] 的形式返回这两个整数的下标 index1 和 index2。
    解题思路:之前做过一个没有排好序的俩数之和,那个是用哈希表来写的
    而这个利用排序的性质,直接用双指针排配二分查找,很快*/
    public static void main(String[] args) {
        int[] arr={2,3,4};
        Alg_Day_3 alg_day_3=new Alg_Day_3();
        alg_day_3.twoSum(arr,6);

    }
    public int[] twoSum(int[] numbers, int target) {
        if (numbers == null){
            return null;
        }
        int[] array=new int[2];
        int left=0;
        int right=numbers.length-1;
        while (left < right){
          if (numbers[left] + numbers[right] > target){
              right--;
          }else if (numbers[left] + numbers[right] < target){
              left++;
          }else {
              array[0]=left+1;
              array[1]=right+1;
              break;
          }
        }
        return array;
    }


}
