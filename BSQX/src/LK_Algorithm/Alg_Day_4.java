package LK_Algorithm;

public class Alg_Day_4 {
    /* 反转字符串
    不要给另外的数组分配额外的空间，你必须原地修改输入数组、使用 O(1) 的额外空间解决这一问题
    * 解题思路:双指针*/
    public void reverseString(char[] s) {
        int i=0;
        int j=s.length-1;

        while (i < j){
            char ch=s[i];
            s[i]=s[j];
            s[j]=ch;
            i++;
            j--;
        }
    }
    /*给定一个字符串 s ，你需要反转字符串中每个单词的字符顺序，同时仍保留空格和单词的初始顺序。
    * */
    public String reverseWords(String s) {
        String[] ret=s.split(" ");
        StringBuilder stringBuilder=new StringBuilder();
        for (int i = 0; i < ret.length; i++) {

            int k=ret[i].length()-1;
            while (k >= 0){
                stringBuilder.append(ret[i].charAt(k));
                k--;
            }
            if (i != ret.length-1){
                stringBuilder.append(" ");
            }

        }
        return stringBuilder.toString();
    }
}
