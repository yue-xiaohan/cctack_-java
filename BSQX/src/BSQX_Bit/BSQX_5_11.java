package BSQX_Bit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class BSQX_5_11 {
    public static void main(String[] args) {
        //子序列问题:
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n = scanner.nextInt();
            int[] ret = new int[n];
            for (int i = 0; i < n; i++) {
                ret[i] = scanner.nextInt();
            }
            ArrayList<Integer> arrayList = new ArrayList<>();
            process2(ret,0,arrayList);
            int k =0,size =arrayList.size() ;
            for (int i = 0; i < size; i++) {
                if (k < arrayList.get(i))k =arrayList.get(i);
            }
            System.out.println(k);

        }

    }

    public static void process2(int[] str, int i,ArrayList<Integer> arrayList){
        if (i == str.length){
            int count =fun(str);
            arrayList.add(count);
            return;
        }
        process2(str,i+1, arrayList);//要当前字符的路
        int tmp =str[i];
        str[i] =0;
        process2(str,i+1,arrayList);//不要当前字符的路
        str[i] = tmp;
    }
    public static int fun(int[] ret){
        //判断是不是单调递增的的
        int count=0;
        for (int i = 1; i < ret.length; i++) {
            if (ret[i] == 0){
                continue;
            }
            if (ret[i] < ret[i-1]){
                return -1;
            }
            count++;
        }
        return count;
    }
    public static void process3(int[] str, int i,ArrayList<int[]> arrayList){
        if (i == str.length){
           arrayList.add(Arrays.copyOf(str,str.length));

            return;
        }
        process3(str,i+1,arrayList);//要当前字符的路
        int tmp =str[i];
        str[i] =0;
        process3(str,i+1,arrayList);//不要当前字符的路
        str[i] = tmp;
    }
    public static void main1(String[] args) {
        /*错排问题:n个位置,n个编号,求,每个编号都放错的情况*/
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n =scanner.nextInt();
            long ret = func2(n);
            System.out.println(ret);
        }
    }
    public static long func2(int n){
        if (n == 1 ) return 0;
        if (n == 2) return 1;
        return (n - 1) * (func2(n - 1) + func2(n - 2));
    }
}
