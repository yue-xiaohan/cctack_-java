package BSQX_Bit;

import java.util.Scanner;
import java.util.Stack;

public class BSQX_3_26 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int num=scanner.nextInt();
        int ret=fib(num);
        System.out.println(ret);
    }
    public static int fib(int num){
        if (num < 2){
            return 0;
        }
        int a=0;
        int b=1;
        int c=0;
        int max=Integer.MIN_VALUE;
        int min=Integer.MAX_VALUE;
        while (c < num){
            min=c;
            c=a+b;
            a=b;
            b=c;
            max=c;
        }
        int k=Math.min(max-num,num-min);
        return k;
    }
    /*括号匹配问题*/
    public boolean chkParenthesis(String A, int n) {
       if (A == null)return false;
       Stack<Character> stack=new Stack<>();
        for (int i = 0; i < n; i++) {
            if (A.charAt(i) == '('){
                stack.push(')');
            }else if (A.charAt(i) == '['){
                stack.push(']');
            }else if (A.charAt(i) == '{'){
                stack.push('}');
            } else if (stack.isEmpty() || stack.pop() != A.charAt(i))return false;

        }
        return stack.isEmpty();
    }
}
