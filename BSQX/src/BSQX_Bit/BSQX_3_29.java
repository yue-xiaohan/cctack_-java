package BSQX_Bit;

import java.util.Scanner;

public class BSQX_3_29 {

    public static void main(String[] args) {
        Scanner in=new Scanner(System.in);
        String str=new String();
        str=in.nextLine();
        int n=str.length();
        int sum=0;

        /*长度得分*/
        if (n >= 8){
            sum=25;
        }else if (n <= 4){
            sum=5;
        }else {
            sum=10;
        }
        /*字母*/
        boolean flg1=false;
        boolean flg2=false;

        /*数字*/
       int sz=0;

       /*符号*/
       int fh=0;

        for (int i = 0; i < n; i++) {
            if (str.charAt(i) >= 'a' &&  str.charAt(i) <= 'z'){flg1=true;}
            else if (str.charAt(i) >= 'A' &&  str.charAt(i) <= 'Z'){flg2=true;}
            else if (str.charAt(i) >= '0' &&  str.charAt(i) <= '9')sz++;
            else if ((str.charAt(i) >= 0x21 &&  str.charAt(i) <= 0x2F)|| (str.charAt(i) >= 0x3A &&  str.charAt(i) <= 0x40)
                    || (str.charAt(i) >= 0x5B &&  str.charAt(i) <= 0x60) || (str.charAt(i) >= 0x7B &&  str.charAt(i) <= 0x7E)){
                fh++;
            }
        }
        if (flg1 && flg2 )sum+=20;
        else if (flg1 || flg2) sum+=10;

        if (sz == 1)sum+=10;
        else if (sz > 1)sum+=20;

        if (fh == 1)sum+=10;
        else if (fh > 1)sum+=25;


        if (flg1 && flg2 && sz > 0 && fh >0)sum+=5;
        else if ((flg1 || flg2) && sz > 0 && fh >0)sum+=3;
        else if ((flg1 || flg2) && sz > 0)sum+=2;


       if (sum >= 90) System.out.println("VERY_SECURE");
       else if (sum >= 80)System.out.println("SECURE");
       else if (sum >= 70)System.out.println("VERY_STRONG");
       else if (sum >= 60)System.out.println("STRONG");
       else if (sum >= 50)System.out.println("AVERAGE");
       else if (sum >= 25)System.out.println("WEAK");
       else if (sum >= 0)System.out.println("VERY_WEAK");


    }



    public boolean checkWon(int[][] board) {
        // write code here
        int n=board.length;
        if (board == null){
            return  false;
        }
        int count=0;
        for (int k = 0; k < n; k++) {
            count=0;
            for (int l = 0; l < n; l++) {
                if (board[k][l] == 1){
                    count++;
                }
            }
            if (count == n){
                return true;
            }
        }

        for (int k = 0; k < n; k++) {
            count=0;
            for (int l = 0; l < n; l++) {
                if (board[l][k] == 1){
                    count++;
                }
            }
            if (count == n){
                return true;
            }
        }
             count =0;
        int count1=0;
        for (int k = 0; k < n; k++) {
            for (int l = 0; l < n; l++) {
                if (k == l && board[k][l] == 1){
                    count++;
                }
                if ( k + l == n && board[k][l] == 1){
                    count1++;
                }
            }
        }
        if (count == n  || count1 == n ){
            return true;
        }
        return false;
    }



    public static void add(StringBuffer a,StringBuffer b){
        a.append(b);
        b=a;
    }
}
