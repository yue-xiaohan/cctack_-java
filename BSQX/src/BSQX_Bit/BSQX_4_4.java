package BSQX_Bit;


import java.util.Arrays;
import java.util.Scanner;

public class BSQX_4_4 {
    public static void main1(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n= scanner.nextInt();
            System.out.println(ZS(n));
        }
    }
    public static int ZS(int n){
        int sum=0;
        while (n > 0){
            n=n&(n-1);
            sum++;
        }
        return sum;
    }

    public static void main(String[] args) {
        int[] left={0,7,1,6};
        int[] right={1,5,0,6};
        BSQX_4_4 s1=new BSQX_4_4();
        int ret=s1.findMinimum(4, left, right);
        System.out.println(ret);
    }
    public  int findMinimum2(int n, int[] left, int[] right) {
        /*解题思路:我的错误的
        * 答案应该是找到最少的左手套/右手套总数
        * 然后减去最少的手套数然后+1,
        * 然后再拿一只别的组的手套
        */
        /*找到那个*/
       if (n == 1)return 2;
       int sumL=0,sumR=0;
       int minL=left[0];
       int minR=right[0];
       int LL=0,LR=0;
        for (int i = 0; i < n; i++) {
            if (left[i] != 0){
                sumL+=left[i];
                if (minL > left[i]){
                    minL=left[i];
                }
            }else{
                LL++;
            }
            if (right[i] != 0){
                sumR+=right[i];
                if (minR > right[i]){
                    minR=right[i];
                }
            }else{
                LR++;
            }
        }
        int count=0;
        if (LL == LR){
            if (sumL > sumR){
                count=sumR-minR+1+1;
            }else {
                count=sumL-minL+1+1;
            }
        }else {
            if (LL > LR){
                count=sumR-minR+1+1;
            }else {
                count=sumL-minL+1+1;
            }
        }
        return count;
    }


        public int findMinimum(int n, int[] left, int[] right) {
            // write code here
            int sum=0;//存放某种颜色的一只手的手套个数为0的时候，另一只手的手套数量
            int leftSum=0;
            int rightSum=0;
            int leftMin=Integer.MAX_VALUE;
            int rightMin=Integer.MAX_VALUE;
            for(int i=0;i<n;i++){
                if(left[i]*right[i]==0){
                    sum=sum+left[i]+right[i];
                }else{
                    leftSum+=left[i];
                    if(leftMin>left[i]){
                        leftMin=left[i];
                    }
                    rightSum+=right[i];
                    if(rightMin>right[i]){
                        rightMin=right[i];
                    }
                }
            }
            return sum+Math.min(leftSum-leftMin+1,rightSum-rightMin+1)+1;

        }


}
