package BSQX_Bit;

import java.util.*;

public  class BSQX_3_20 {
    /*排列子序列:子序列是非递增或者非递减排序的*/
    public static void main(String[] args) {
      /*  Scanner  scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int [] array=new int[n];*/
        /*for (int i = 0; i < n; i++) {
            int k= scanner.nextInt();
            array[i]=k;
        }*/
        int n=6;
        int [] array={1,2,3,2,2,1,0};

        int k= func2(array);
        System.out.println(k);

    }
    public static  int func2(int[] array){
        int count=0;
        int i=0;
        boolean flg1=false;
        boolean flg2=false;
        int n=array.length;
        while ( i < array.length){
            /*核心思路是对的,但是代码写的有问题
            * 一下注释为:博哥写的代码,我是缺少了if判断条件吗?不光是这样,博哥处理了数组越界问题我没有处理,这才是问题所在
            * 博哥的数组定义为:n+1个空间,所以博哥的边界问题处理的比我好,具体不止体现在数组长度额外加1,还有
            * while循环博哥用的是n(代表输入的个数)而我用的是数组长度,这样依然会抛出异常.*/

            /*if(array[i] < array[i+1]) {
                while(i < n && array[i] <= array[i+1]) {
                    i++;
                }
                count++;
                i++;
            }else if(array[i] == array[i+1]) {
                i++;
            }else {
                while(i < n && array[i] >= array[i+1]) {
                    i++;
                }
                count++;
                i++;
            }*/
            /*我写的代码*/
            while (i < array.length && array[i] <= array[i+1]){
                i++;
                flg1=true;
            }
            if (flg1){
                count++;
                flg1=false;
                i++;
            }
            while (i < array.length && array[i] >= array[i+1]){
                i++;
                flg2=true;
            }
            if (flg2){
                count++;
                flg2=false;
                i++;
            }
        }
        return count;
    }
    /*逆置字符串*/
    /*博哥解题思路
     * 先将整个字符串逆置过来，再遍历字符串，找出每个单词，对单词逆置
     * 我的解题思路,利用空格分割,然后逆置*/
   /* public static void main(String[] args) {
        String str =new String();
        Scanner  scanner=new Scanner(System.in);
        str=scanner.nextLine();
       // str="I like beijing.";
        String str2=func(str);
        System.out.println(str2);
    }
    public  static  String func(String str){
        Stack<String> stack=new Stack<>();
       String[] chars=str.split(" ");
       StringBuilder stringBuilder=new StringBuilder();
        for (int i = chars.length-1; i >= 0; i--) {
            stringBuilder.append(chars[i]+" ");
        }
        return stringBuilder.toString();

    }*/


}


