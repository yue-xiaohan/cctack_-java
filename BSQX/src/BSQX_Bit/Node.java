package BSQX_Bit;

import java.util.ArrayList;

public class Node {
    /*点集的结构有向图(无向图只是特殊的有向图而已)*/

    public int value;//代表自己的这个点
    public int in;//代表这个点有多少的入度
    public int out;//代表这个点有多少的出度
    public ArrayList<Node> nexts;//代表当前这个点直接到达的邻居(而且边要属于我)有哪些
    public ArrayList<Edge> edges;//代表属于这个点的边有哪些

    public Node(int value) {
        this.value = value;
        in=0;
        out=0;
        nexts = new ArrayList<>();
        edges = new ArrayList<Edge>();
    }
}
