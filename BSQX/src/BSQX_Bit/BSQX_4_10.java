package BSQX_Bit;

import java.util.Scanner;

public class BSQX_4_10 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str1=scanner.nextLine();
        String str2=scanner.nextLine();
        String ret=lcst(str1,str2);
        System.out.println(ret.length());
    }
    public static String lcst(String str1, String str2) {
        if (str1 == null || str2 == null || str1.equals("") || str2.equals("")) {
            return "";
        }
        char[] chs1 = str1.toCharArray();
        char[] chs2 = str2.toCharArray();
        int row = 0;
        int col = chs2.length - 1;
        int max = 0;
        int end = 0;
        while (row < chs1.length) {
            int i = row;
            int j = col;
            int len = 0;

            while (i < chs1.length && j < chs2.length) {
                if (chs1[i] != chs2[j]) {
                    len = 0;
                } else {
                    len++;
                }
                if (len > max) {
                    end = i;
                    max = len;
                }
                i++;
                j++;
            }
            if (col > 0) {
                col--;
            } else {
                row++;
            }
        }
        if(max==0) return "-1";
        return str1.substring(end - max + 1, end + 1);
    }
    public static void main1(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.nextLine();
        System.out.println(func(str));
    }
    public static String func(String str){
        if (str == null){
            return " ";
        }
        StringBuilder stringBuilder=new StringBuilder();
        for (int i = str.length()-1; i >=0 ; i--) {
            stringBuilder.append(str.charAt(i));

        }
        return stringBuilder.toString();
    }
}
