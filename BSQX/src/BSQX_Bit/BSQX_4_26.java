package BSQX_Bit;

import java.util.Scanner;

public class BSQX_4_26 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()){
            int start = in.nextInt();
            int end = in.nextInt();
            if (start == end){
                long  ret = func(end);
                System.out.println(String.valueOf(ret));
            }else {
                long  ret = func(end+1);
                System.out.println(String.valueOf(ret));
            }
        }
    }
    public static long func(int end){
       if (end <= 2)return 1;
       long a=1,b=1,c=0;
        for (int i = 1; i < end; i++) {
            c = a+b;
            a =b;
            b =c;

        }
        return c;
    }


    public static void main1(String[] args) {

        Scanner in = new Scanner(System.in);
        while (in.hasNext()){
            String s1=in.next();
            String s2=in.next();
            int count =0;
            for (int i = 0; i < s1.length(); i++) {
                String cur = null;
                if (i+s2.length()-1 < s1.length()){
                    cur = s1.substring(i,i+s2.length());
                }
                if (cur!=null && cur.equals(s2)){
                    count++;
                    i+=s2.length()-1;
                }
            }
            System.out.println(count);

        }
    }
}
