package BSQX_Bit;

import java.util.ArrayList;
import java.util.Scanner;

public class BSQX_5_9 {
    public static void main(String[] args) {
        //蘑菇阵
        /*量太大,崩了*/
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()){
            int m = in.nextInt();
            int n = in.nextInt();
            int k = in.nextInt();
            int[][] arr = new int[m][n];
            for (int i = 0; i < k; i++) {
                int x = in.nextInt();
                int y = in.nextInt();
                arr[x-1][y-1] = 1;
            }
            dfsBP(arr,0,0);
            double conut= jC1(m)/jC2(m,n);
           double result = cS/conut;
            System.out.printf("%.2f",result);
        }
    }
    public static int jC1(int n){
        int conut=1;
        for (int i = 1; i <= n; i++) {
            conut*=i;
        }
        return conut;
    }
    public static int jC2(int n,int m){
        int conut=1;
        for (int i = 1; i <= n-m; i++) {
            conut*=i;
        }
        return conut;
    }
    static  double cS = 0;
    public static void dfsBP(int[][] arr , int m ,int n){
        if (m == arr.length-1 && n == arr.length-1 ){
            ++cS;
            return;
        }
        if (arr != null && m >= 0 && n >=0 && m < arr.length && n < arr[0].length && arr[m][n] != 1&& arr[m][n] != -1){
            arr[m][n] = -1;
            dfsBP(arr,m+1,n);
            dfsBP(arr,m,n+1);
            arr[m][n] = 0;

        }
    }
    public static void main1(String[] args) {
        //图的问题
        //深度优先遍历应该就行了
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()){
            //m行 , n列
            int m = in.nextInt();
            int n = in.nextInt();
            char[][] arr = new char[m][n];
            int mX = 0 ,nL = 0;
            for (int i = 0; i < m; i++) {
                String s = in.next();
                for (int j = 0; j < n; j++) {
                    arr[i][j] = s.charAt(j);
                    if (arr[i][j] == '@'){
                        mX = i;
                        nL = j;
                    }
                }
            }
            int count = dfs(arr,mX,nL);
            System.out.println(count);
        }
    }
    public static int dfs(char[][] arr , int m ,int n){
    //传过来,的m , n 是当前所在位置
        if (arr == null||m < 0 || n < 0 || m >=arr.length || n >= arr[0].length || arr[m][n] == '#'){
            return 0;
        }
        int k =1;
        arr[m][n] = '#';
        k+=dfs(arr,m+1,n);
        k+=dfs(arr,m-1,n);
        k+=dfs(arr,m,n+1);
        k+=dfs(arr,m,n-1);
        return k;
    }
}
