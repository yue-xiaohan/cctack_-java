package BSQX_Bit;

import java.util.*;

public class BSQX_4_21 {
    public int getFirstUnFormedNum(int[] arr) {
        int min=Integer.MAX_VALUE;
        int max=0;
        Arrays.sort(arr);
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            max += arr[i];
            if (min > arr[i])min = arr[i];
            map.put(arr[i],map.getOrDefault(arr[i],0)+1);
        }
        int k=0;
        for (int i = min; i <= max ; i++) {
            int ret =i;
           for (int j :arr){
               if(ret - j <= arr[arr.length-1]){

               }else {
                   ret = ret -j;
               }
           }
        }
        return k;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n =1;
        while (true){
            n = in.nextInt();

            if (n == 0)break;
            int count = Fun(n);
            System.out.println(count);
        }
    }


    public static int Fun(int n){
        if(n==1)
            return 0;
        if(n<=3)
            return 1;
        int metage,rest,times=0;
        // 3等分前，先加2，使得metage的值尽量大于rest
        // (metage,metage,rest)
        metage = (n+2)/3;
        rest = n-2*metage;

        times= 1 + Fun(Math.max(metage, rest));
        return times;
    }
}
