package BSQX_Bit;

import java.util.Scanner;

public class BSQX_3_28 {
    static int count=0;
    public static int fun_c(int[][] array,int n,int m ){
        /*解题思路应该是深度优先搜索*/
        if ( n < 0 || m < 0||n >= array.length || m >=array[0].length ){
            return 0;
        }else if ( array[n][m] == 1){
           count++;
        }

        fun_c(array,n+1,m);
        fun_c(array,n,m+1);

        return count;
    }
    public static void main3(String[] args) {
        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int m=in.nextInt();
        int[][] array=new int[n+1][m+1];
        array[n][m]=1;
        int ret=fun_c(array,0,0);
        System.out.println(ret);
    }

    public int add(int A, int B) {
        /*异或的性质与加一样,怎么解决进制转换?*/
        /*int c,d;
        while(B!=0){
            c = A^B;
            *//*因为&的性质是全1才1,所以&下来的结果是1就说明要往前进1,所以我们让&下来的结果往左
            * 移动1位,在与原异或结果相异或从而实现进1 的效果
            * 那么什么时候终止呢?答案在与:&的结果没有相同的值了(为0),代表者所有的进制都进1完成*//*
            d = (A&B)<<1;
            A=c;
            B=d;
        }
        return A;*/
        //  精简版
        return B==0?A:add(A^B,(A&B)<<1);
    }
    public int sub(int A, int B) {
        /*补充知识:正数:原码,反码,补码是本身,负数:反码=符号位不变,原码按位取反 补码=是在反码基础上加1*/
        /*减法:相当于:a+(-b)*/
     		return add(A, add(~B, 1));
    }
    public int mul(int A ,int B){
        /*乘法等于:循环调用加法:特殊条件任何数*1都等于1*/
        int ret=add(A,A);
        for (int i = 1; i < B-1; i++) {
             ret=add(ret,A);
        }
        return ret;
    }
    public int div(int A ,int B){
        /*除法等于:循环调用减法:特殊条件任何数/1都等于本身,0不能做除数*/
       if (B == 0){
           return -1;
       }if (B == 1){
           return A;
       }

       int count=0;
        int sum=add(B,0);
        count++;
       while (sum < A){
           sum=add(sum,B);
           count++;
       }
        return count;
    }
    /*一个int类型的数据，对应的二进制一共有32个比特位，可以采用位运算的方式一位一位的检测，具体如下
    */
    public int count_one_bit( int n)
    {
        int count = 0;
        while(n > 0)
        {
            n = n&(n-1);
            count++;
        }
        return count;
    }

    /*
思路：
1. 先将m和n进行按位异或，此时m和n相同的二进制比特位清零，不同的二进制比特位为1
2. 统计异或完成后结果的二进制比特位中有多少个1即可
*/
    int calc_diff_bit(int m, int n)
    {
        int tmp = m^n;
        int count = 0;
        while(tmp > 0)
        {
            tmp = tmp&(tmp-1);
            count++;
        }
        return count;
    }
    int Gcd(int a, int b)//最大公约数-递归。辗转相除法
    {
        if (a % b == 0)
            return b;
        else
            return Gcd(b, a % b);
    }
    int lcm(int n,int m)
    {
        return n*m/Gcd(n,m);
    }



    public static void main(String[] args) {
        BSQX_3_28 s=new BSQX_3_28();
      //  System.out.println(s.reduce(5,8));
        System.out.println(s.mul(2,5));

        System.out.println(  s.div(10,2));
    }



    public static void main2(String[] args) {
     /*   BSQX_3_28 s=new BSQX_3_28();
        s.add_(4,8);*/
        /*System.out.println(Integer.MAX_VALUE);
        System.out.println(15^Integer.MAX_VALUE);
        System.out.println(Integer.MAX_VALUE-15^Integer.MAX_VALUE);*/
        BSQX_3_28 s=new BSQX_3_28();


    }
  /*  String string=new String("hello");
    char[] ch={'a','b'};



    private  void test(){
       string="abc";
       ch[0]='c';
    }
    public static void main(String[] args) {
       int i=0;
       Integer j=new Integer(0);
        System.out.println(i == j);
        System.out.println(j.equals(i));
    }*/
}
/*class Person{
    String name="name";
    public Person(String name){
        this.name=name;
    }

}*/
