package BSQX_Bit;

import java.util.ArrayList;
import java.util.Scanner;

public class BSQX_4_9 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        String str1= scan.nextLine();
        String str2= scan.nextLine();

   //   String ret = fun2(str1,str2);

     //   System.out.println(ret);

    }




        public static String lcst(String str1, String str2) {
            if (str1 == null || str2 == null || str1.equals("") || str2.equals("")) {
                return "";
            }
            char[] chs1 = str1.toCharArray();
            char[] chs2 = str2.toCharArray();
            int row = 0;
            int col = chs2.length - 1;
            int max = 0;
            int end = 0;
            while (row < chs1.length) {
                int i = row;
                int j = col;
                int len = 0;

                while (i < chs1.length && j < chs2.length) {
                    if (chs1[i] != chs2[j]) {
                        len = 0;
                    } else {
                        len++;
                    }
                    if (len > max) {
                        end = i;
                        max = len;
                    }
                    i++;
                    j++;
                }
                if (col > 0) {
                    col--;
                } else {
                    row++;
                }
            }
            if(max==0) return "-1";
            return str1.substring(end - max + 1, end + 1);
        }


    public static void main1(String[] args) {
        Scanner scan=new Scanner(System.in);

        while (true){
            int n= scan.nextInt();
            if (n == 0){
                break;
            }
            int ret=fun(n);
            System.out.println(ret);
        }
    }
    public static int fun(int n){
        if (n < 3){
            return 0;
        }
        int count=0;
        int k=0;
        while (n >= 3){

            count=n%3+n/3;//空瓶的数量

            k+=n/3;//喝了的汽水
            n=count;
        }
        if (count == 2){
            return k+1;
        }else {
            return k;
        }
    }
}
