package BSQX_Bit;

import java.util.ArrayList;
import java.util.Scanner;

public class BSQX_4_27 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n =scanner.nextInt();
            System.out.println(fun(n));
        }
    }
    public static long fun(int n){
        if (n < 2)return n;
        long a = 1,b=1,c=0;
        for (int i = 2; i <= n; i++) {
            c = a+b;
            a = b;
            b = c;
        }
        return c;
    }
    public static void main1(String[] args) {

        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNext()){
            int n=scanner.nextInt();
            scanner.nextLine();
            ArrayList<String> arrayList=new ArrayList<String>();
            for(int i=0;i<n;i++){
                String name=scanner.nextLine();
                if(name.contains(" ")||name.contains(",")){
                    arrayList.add("\""+name+"\"");
                }else{
                    arrayList.add(name);
                }
            }
            for(int i=0;i<n-1;i++){
                System.out.print(arrayList.get(i)+", ");
            }
            System.out.println(arrayList.get(n-1));
        }

    }
}
