package BSQX_Bit;

import java.util.Scanner;

public class BSQX_4_13 {
    public static int fun(int a,int b){
        //求最大公约数
        if (a % b == 0)return b;
        else return fun(b,a%b);
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()){
            int n=in.nextInt();//怪物的数量
            int a= in.nextInt();//初始能力值
            int c=a;//当前能力值
            int[]  arr=new int[n];
            for (int i = 0; i < n; i++) {
                arr[i]=in.nextInt();
            }
            for (int i = 0; i < n; i++) {
                if (c > arr[i]){
                    c+=arr[i];
                }else {
                    c+=fun(c,arr[i]);
                }
            }
            System.out.println(c);
        }
    }
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String str=in.nextLine();
        int[] arr=new int[26];
        for (int i = 0; i < str.length(); i++) {
            arr[str.charAt(i) - 'a'] ++;
        }
        boolean flg=true;
        for (int i = 0; i < str.length(); i++) {
            int k= str.charAt(i)-'a';
            if (arr[k] == 1){
                System.out.println((char)(k+'a'));
                flg=false;
                break;
            }
        }
        if (flg) System.out.println("-1");


    }

}
