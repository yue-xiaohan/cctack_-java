package BSQX_Bit;

import java.util.Arrays;
import java.util.Scanner;

public class BSQX_4_18 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()){

            double n1=in.nextDouble();
            double n2=in.nextDouble();
            if(n1 < 2 * 3.14 * n2) System.out.println("Yes");
            else System.out.println("No");
        }
    }

    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextInt()){
            int n = in.nextInt();
            int ret = Fun(n);
            System.out.println(ret);
        }
    }
    public static int Fun(int n){
        if (n <= 2){
            return  n;
        }
        int[] dp = new int[n];
        dp[0]=1;
        dp[1]=2;
        for (int i=2; i < n ; i++){
            for (int j = i; j > 0 ;j--){
                dp[i]+=dp[j-1];
            }
            dp[i]++;
        }
        return dp[n-1];
    }
}
