package BSQX_Bit;

import java.util.HashSet;
import java.util.Scanner;

public class BSQX_5_5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNextInt()){
            //无人获奖的概率也就是所有人都抽错了
            //也就是 都抽错了概率 /  总的抽奖次数
            int n = scan.nextInt();
            double x = func2(n) / func(n);
            System.out.printf("%2.2f%%\n", x * 100);
        }
    }
    public static double func2(int n){
        if (n == 1 ) return 0;
        if (n == 2) return 1;
        return (n - 1) * (func2(n - 1) + func2(n - 2));
    }
    public static long func(int n){
        //这个函数返回阶层
        if ( n <= 2){
            return  n;
        }
        long sum =1 ;
        for (int i = 2 ; i <= n ;i++ ){
            sum *=i;
        }
        return sum;
    }
    public static void main2(String[] args) {
        Scanner scan = new Scanner(System.in);
        while(scan.hasNext()){
            String s1 = scan.nextLine();
            int n = 0;
            HashSet<String> set = new HashSet<>();
            while(n < s1.length()){
                if(s1.charAt(n) == '\"'){
                    int end = s1.indexOf('\"',n+1); //从pos+1位置往后查找第一个"
                    String tmp = s1.substring(n+1,end); //截取[pos,end)之间的字符串
                    set.add(tmp);
                    n = end + 2;  //到下一个名字的开头首字符
                }else{
                    int end = s1.indexOf(',',n+1);
                    if(end == -1){
                        end = s1.length()-1;
                        set.add(s1.substring(n,end+1));
                        break;
                    }
                    String tmp = s1.substring(n,end);
                    set.add(tmp);
                    n = end + 1;
                }
            }
            s1 = scan.nextLine();
            if(set.contains(s1)){
                System.out.println("Ignore");
            }else{
                System.out.println("Important!");
            }
        }
    }


}
