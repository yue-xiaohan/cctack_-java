package BSQX_Bit;


import java.util.*;

public class BSQX_4_3 {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int[] array=new int[n];
        HashSet<Integer> hashSet=new HashSet<>();

        for (int i = 0; i < n; i++) {
            array[i]= scanner.nextInt();
            if (!hashSet.contains(array[i]))hashSet.add(array[i]);
        }
        int count=0;
        /*判断*/
        int j=0;
        for (int i = n; i > 1; i--) {
            int [] ret=Truncate_Array(array,0,i);
            boolean flg=Judging_Luck(ret);
            if (flg)count++;
            if (j < i){
                ret=Truncate_Array(array,j,n);
                flg = Judging_Luck(ret);
                if (flg)count++;
                j++;
            }

        }
        System.out.println(count);

    }
    public static int[] Truncate_Array(int [] array,int start,int end){
        if (array == null && start < end)return null;
        //前闭后开区间
        int[] ret=new int[end-start];
        for (int i = start; i < end; i++) {
            ret[i]=array[i];
        }
        return ret;

    }
    public static boolean Judging_Luck(int[] array){
        /*用来判断数组的和是否大于乘积*/
        int sum=0,mlu=1;
        for (int a:array) {
            sum+=a;
            mlu*=a;
        }
        return sum>mlu;
    }
    public static void main1(String[] args) {
        /*输出这一年是第几天???*/
        Scanner scanner=new Scanner(System.in);
        int year=0,mouth=0,date=0;
        year= scanner.nextInt();
        mouth= scanner.nextInt();
        date= scanner.nextInt();
        /*判断是否为闰年*/
        boolean flg=false;
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0){
            flg=true;
        }
        int[] mouths={31,28,31,30,31,30,31,31,30,31,30,31};
        if (flg)mouths[1]+=1;
        int sum=0;
        for (int i = 0; i < mouth-1; i++) {
            sum+=mouths[i];
        }
        sum+=date;
        System.out.println(sum);
    }
}
