package BSQX_Bit;

import java.util.*;

public class BSQX_4_11 {
    public static void main1(String[] args) {
        Scanner sc = new Scanner(System.in);
        int count = sc.nextInt();
        while (count > 0){
            int n = sc.nextInt();
            int k = sc.nextInt();
            int[] res = new int[2*n];
            for(int i=0;i<2*n;i++){
                int tmp = i + 1;
                for(int j = 0; j < k;j++){
                    if (tmp <= n) tmp = 2*tmp - 1;
                    else tmp = 2 * (tmp - n);
                }
                res[tmp - 1]=sc.nextInt();
            }
            //输出
            if(res.length> 0) System.out.print(res[0]);
            for(int i = 1;i< 2*n;i++){
                System.out.print(" "+res[i]);
            }
            System.out.println();
            count--;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n= sc.nextInt();
        String str= sc.nextLine();

        /*将数据压入队列*/

        char[]  chars=str.toCharArray();
        Stack<Integer> stack=new Stack<>();
        int c=0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == 'U'){
              if (c == 0){
                  c=n;stack.add(c);c--;
              }else {
                  c--;stack.add(c);
              }
            }else if (chars[i] == 'D'){
               if (c == n){
                   c=1;
                   stack.add(c);
                   c++;
               }else {
                   c++;
                   stack.add(c);
               }
            }
        }
        int ret=0;
        if (!stack.isEmpty())ret=stack.peek();
        int size=stack.size();
        for (int i = 0; i < size; i++) {
            System.out.print(stack.pop()+" ");
        }
        System.out.println(ret);
    }

}
