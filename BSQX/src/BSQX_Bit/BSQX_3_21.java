package BSQX_Bit;

//import static sun.security.ssl.ExtensionType.e;

import java.util.*;


public class BSQX_3_21 {
    public int MoreThanHalfNum_Solution (int[] numbers) {
        /*找出数组中出现次数超过一半的数字
        * 三种解法:1:使用哈希map
        * 2:使用数组排序
        * 3:使用消除不同数字*/
        if (numbers == null){
            return -1;
        }
        HashMap<Integer,Integer> hashMap=new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            if (!hashMap.containsKey(numbers[i])){
                hashMap.put(numbers[i],1);
            }else {
                hashMap.put(numbers[i],hashMap.get(numbers[i])+1);
            }
        }
        for (int i = 0; i < numbers.length; i++) {
            if (hashMap.get(numbers[i]) > numbers.length/2){
                return numbers[i];
            }
        }
       /* 第二种写法
       Arrays.sort(numbers);
        return numbers[numbers.length/2];*/
        return -1;
    }


    public static void main3(String[] args) {
        /*找出字符串中最长的数字*/
        Scanner scanner=new Scanner(System.in);
        String str=new String();
        str=scanner.nextLine();
        str=str.toLowerCase(Locale.ROOT);
        char[] chars=str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= 'a' && chars[i] <= 'z'){
                chars[i]=' ';
            }
        }
        HashMap<Integer,String> hashMap=new HashMap<>();
        StringBuilder stringBuilder =new StringBuilder();
        boolean flag=false;
        int max=0;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] != ' '){
                stringBuilder.append(chars[i]);
                flag=true;
            }else if (flag){
                hashMap.put(stringBuilder.length(),stringBuilder.toString());
                if (max < stringBuilder.length()){
                    max=stringBuilder.length();
                }
                stringBuilder=new StringBuilder();
                flag=false;
            }
        }
        if (flag == true){
            hashMap.put(stringBuilder.length(),stringBuilder.toString());
            if (max < stringBuilder.length()){
                max=stringBuilder.length();
            }
        }
        System.out.println(hashMap.get(max));
    }



   // public String grade;
    public static void main2(String[] args) {
      /*  A a=new BSQX_3_21();
        System.out.println(a.name);*/
        String str1="hello";
        String str2="he"+new String("llo");
        System.out.println(str1==str2);
    }
    public static void main1(String[] args) {
        int i=11;
        //if (i > 10)throw new Exception e("asd");
        HashMap<String,String> map=new HashMap<>();
        map.put(null,null);
    }
}
class A{
    private String name="!@3";
    int age=0;
}
