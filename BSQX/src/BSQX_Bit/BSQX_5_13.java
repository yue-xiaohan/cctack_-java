package BSQX_Bit;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

public class BSQX_5_13 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = Integer.parseInt(sc.nextLine());
            String[] str = sc.nextLine().split(" ");
            Stack<String> stack = new Stack<>();
            int sum = 0;
            for(int i = 0; i < n; i++){
                if(!str[i].equals("+") && !str[i].equals("-")
                        && !str[i].equals("*") && !str[i].equals("/")){
                    stack.push(str[i]);
                }
                else{
                    int a = Integer.parseInt(stack.pop());
                    int b = Integer.parseInt(stack.pop());
                    if(str[i].equals("+")){
                        sum = a + b;
                    }
                    if(str[i].equals("-")){
                        sum = b - a;
                    }
                    if(str[i].equals("*")){
                        sum = a * b;
                    }
                    if(str[i].equals("/")){
                        sum = b / a;
                    }
                    stack.push(String.valueOf(sum));
                }
            }
            System.out.println(stack.pop());
        }
    }
    public static int suffix(char[] chars){
        Stack<Integer> stack = new Stack<>();
        int result = Integer.MAX_VALUE;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] >= '0' && chars[i] <= '9'){
                stack.push(Integer.parseInt(chars[i]+""));
            }else {
                if (result == Integer.MAX_VALUE){
                    result = 0;
                }
                if (chars[i] == '+'){
                    int a = stack.pop();
                    int b = stack.pop();
                  result =a+b;
                  stack.push(result);
                }else if (chars[i] == '-'){
                    int a = stack.pop();
                    int b = stack.pop();
                    result =a-b;
                    stack.push(result);
                }else if (chars[i] == '*'){
                    int a = stack.pop();
                    int b = stack.pop();
                    result =a*b;
                    stack.push(result);
                }else {
                    int a = stack.pop();
                    int b = stack.pop();
                    result =a/b;
                    stack.push(result);
                }
            }
        }
        if (stack.isEmpty()){
            return -1;
        }else {
            return stack.pop();
        }
    }
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            char[][] chars=new char[20][20];
            int n=20;
            int len=20;
            int k=0;
            while(n--!=0){
                String str=in.next();
                char[] cA = str.toCharArray();
                chars[k++]=cA;
            }
            boolean rsFlag=false;
            //横向
            for (int i = 0; i < len; i++) {
                boolean flag=false;
                int aste=0;
                int plus=0;
                for (int j = 0; j < len; j++) {
                    char c = chars[i][j];
                    if(c=='*'){
                        aste++;
                        plus=0;
                    } else if(c=='+'){
                        plus++;
                        aste=0;
                    } else{
                        plus=0;
                        aste=0;
                    }
                    if(aste>=5 || plus>=5){
                        System.out.println("Yes");
                        rsFlag=true;
                        flag=true;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }
            if(rsFlag){
                continue;
            }
            //纵向
            for (int i = 0; i < len; i++) {
                boolean flag=false;
                int aste=0;
                int plus=0;
                for (int j = 0; j < len; j++) {
                    char c = chars[j][i];
                    if(c=='*'){
                        aste++;
                        plus=0;
                    } else if(c=='+'){
                        plus++;
                        aste=0;
                    } else{
                        plus=0;
                        aste=0;
                    }
                    if(aste>=5 || plus>=5){
                        System.out.println("Yes");
                        rsFlag=true;
                        flag=true;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }
            if(rsFlag){
                continue;
            }
            //斜-八字撇-正数20
            for (int size = 1; size <= 20; size++) {
                boolean flag=false;
                int aste=0;
                int plus=0;
                for (int i = 0,j=size-1; i < size; i++,j--) {
                    char c=chars[i][j];
                    if(c=='*'){
                        aste++;
                        plus=0;
                    } else if(c=='+'){
                        plus++;
                        aste=0;
                    }
                    else{
                        plus=0;
                        aste=0;
                    }
                    if(aste>=5 || plus>=5){
                        System.out.println("Yes");
                        rsFlag=true;
                        flag=true;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }
            if(rsFlag){
                continue;
            }
            //斜-八字撇-倒数20
            for (int size = 1; size <= 19; size++) {
                boolean flag=false;
                int aste=0;
                int plus=0;
                for (int i = 19,j=20-size; i > 19-size; i--,j++) {
                    char c=chars[i][j];
                    if(c=='*'){
                        aste++;
                        plus=0;
                    }
                    else if(c=='+'){
                        plus++;
                        aste=0;
                    }
                    else{
                        plus=0;
                        aste=0;
                    }

                    if(aste>=5 || plus>=5){
                        System.out.println("Yes");
                        rsFlag=true;
                        flag=true;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }

            if(rsFlag){
                continue;
            }

            //斜-八字捺-正数20
            for (int size = 1; size <= 20; size++) {
                boolean flag=false;
                int aste=0;
                int plus=0;
                for (int i = 0,j=20-size; i < size; i++,j++) {
                    char c=chars[i][j];
                    if(c=='*'){
                        aste++;
                        plus=0;
                    }
                    else if(c=='+'){
                        plus++;
                        aste=0;
                    }
                    else{
                        plus=0;
                        aste=0;
                    }

                    if(aste>=5 || plus>=5){
                        System.out.println("Yes");
                        rsFlag=true;
                        flag=true;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }

            if(rsFlag){
                continue;
            }

            //斜-八字捺-倒数20
            for (int size = 1; size <= 19; size++) {
                boolean flag=false;
                int aste=0;
                int plus=0;
                for (int i = 19,j=size-1; i > 19-size; i--,j--) {
                    char c=chars[i][j];
                    if(c=='*'){
                        aste++;
                        plus=0;
                    }
                    else if(c=='+'){
                        plus++;
                        aste=0;
                    }
                    else{
                        plus=0;
                        aste=0;
                    }

                    if(aste>=5 || plus>=5){
                        System.out.println("Yes");
                        rsFlag=true;
                        flag=true;
                        break;
                    }
                }
                if(flag){
                    break;
                }
            }
        }
    }
}
