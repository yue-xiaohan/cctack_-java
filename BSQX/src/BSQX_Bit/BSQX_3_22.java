package BSQX_Bit;

import java.util.*;

public class BSQX_3_22 {


    public static void main2(String[] args) {
        /*进制转化
        * 注意考虑:超过10进制的情况,和m是负数的情况*/
        Scanner scanner=new Scanner(System.in);
        int m= scanner.nextInt();
        int n= scanner.nextInt();
        boolean flag=false;
        //没考虑类似于16进制的情况
        Stack<Character> stack=new Stack<>();
        if (m == 0){
            stack.push((char)(48));
        }else{
            if (m < 0){
               flag=true;
                m=m*-1;
            }
            while (m > 0){
                int k=m%n;//代表余数
                m = m/n;
                if ( k < 10){
                    stack.push((char)(k+48));
                }else {
                    stack.push((char)(k+55));
                }
            }
        }
        if (flag){
            stack.push('-');
        }
       while (!stack.isEmpty()){
           System.out.print(stack.pop());
       }

    }

    public static void main1(String[] args) {
        /*输出糖果
        * 考虑小数情况,得出的最终结果可能与原来对应不上*/
        Scanner scanner=new Scanner(System.in);
        int a_b= scanner.nextInt();
        int b_c= scanner.nextInt();
        int ab= scanner.nextInt();
        int bc= scanner.nextInt();
        //注意考虑,小数情况
        int A=(a_b+ab)/2;
        int B=(b_c+bc)/2;
        int C=bc-B;
        if (A >= 0 && B >= 0 && C >= 0 && A-B==a_b && B-C == b_c &&A+B==ab&&B+C==bc){
            System.out.println(A+" "+B+" "+C);
        }else {
            System.out.println("No");
        }

    }
}
