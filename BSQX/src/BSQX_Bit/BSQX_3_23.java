package BSQX_Bit;

import java.util.Scanner;

public class BSQX_3_23 {


    public static void main(String[] args) {
        /*连续最大和*/
        Scanner scanner=new Scanner(System.in);
        int n= scanner.nextInt();
        int[] array=new int[n];
        for (int i = 0; i < array.length; i++) {
            int k= scanner.nextInt();
            array[i]=k;
        }
        /*动态规划经典问题:
        * F(N)=max(arr[i]+dp[i-1],arr[i])*/
        int[] dp=new int[n];
        dp[0]=array[0];
        for (int i = 1; i < array.length; i++) {
            dp[i]=Math.max(array[i],array[i]+dp[i-1]);
        }
        int max=Integer.MIN_VALUE;
        for (int i = 0; i < dp.length; i++) {
            if (max<dp[i]){
                max=dp[i];
            }
        }
        if (max < 0){
            System.out.println(-1);
        }else {
            System.out.println(max);
        }

    }
    public static boolean fun2(String str){
        if (str == null){
            return false;
        }
        int i=0;
        int j=str.length()-1;
        while (i < j){
            if (str.charAt(i) != str.charAt(j)){
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
    public static void main3(String[] args) {
        /*测试回文串
        * 解题思路:挨个插入,判定是不是回文串*/
        Scanner scanner=new Scanner(System.in);
        String strA=new String();
        strA=scanner.nextLine();
        String strB=new String();
        strB=scanner.nextLine();
      /*  String strA="aa";
        String strB="a";*/
        int count=0;
        StringBuilder stringBuilder=new StringBuilder();
        for (int i = 0; i < strA.length(); i++) {
            String ret=new String();
            //subSting前闭后开区间
            ret=strA.substring(0,i)+strB+strA.substring(i,strA.length());
            boolean flag=fun2(ret);
            if (flag){
                count++;
            }
        }
        //处理插入到最前面的情况
        String ret=new String();
        ret=strB+strA;
        if (fun2(ret)){
            count++;
        }

        System.out.println(count);
    }

    public static void main2(String[] args) {
        /*String s;
        System.out.println(s);*/
        int x,y;
        x=5>>2;
        y=x>>>2;
        System.out.println(y);
    }
    public static void main1(String[] args) {
        StringBuffer a=new StringBuffer("A");
        StringBuffer b=new StringBuffer("B");
        fun(a,b);
        System.out.println(a+""+b);
    }
    public static void fun(StringBuffer a,StringBuffer b){
        a.append(b);
        b=a;
    }
}
