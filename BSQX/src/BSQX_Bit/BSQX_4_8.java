package BSQX_Bit;

import java.util.Locale;
import java.util.Scanner;

public class BSQX_4_8 {
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        String str1= scan.nextLine();
        String str2= scan.nextLine();
        str1=str1.toLowerCase();
        str2=str2.toLowerCase();
        boolean ret=Func2(str1,str2);
        System.out.println(ret);
    }
    public static boolean Func2(String s1,String s2){

        int j =0;
        int count=0;

        boolean flg=false;

        for (int i = 0; i < s1.length(); i++) {
            char ch1=s1.charAt(i);
            if (ch1 == '*'){
                flg=true;
                continue;
            }else if (ch1 == '?'){
                if ((s2.charAt(j) >='0' && s2.charAt(j)<='9') ||
                        (s2.charAt(j) >='a'&&s2.charAt(j)<='z')) {
                    j++;
                    count++;
                } else {
                    return false;
                }
            }else if (ch1 >='0' || ch1<='9' || ch1 >='a'||ch1<='z'){
                count++;
                for ( ; j < s2.length(); j++) {
                    char ch2=s2.charAt(j);
                    if (ch2 == ch1){
                        j++;
                        break;
                    }else if (i-1>=0 && s1.charAt(i-1)=='*'){
                        continue;
                    } else  {
                        return false;
                    }
                }
            }else {
                return false;
            }
        }
        if (!flg){
            if (count != s2.length())return false;
        }

        return true;
    }




    public static void main1(String[] args){
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        int ret=Fun(n);
        System.out.println(ret);


    }
    public static int Fun(int n){
        if(n <= 2){
            return 1;
        }
        int a=1;
        int b=1;
        int c=0;
        while(n > 2){
            c=a+b;
            a=b;
            b=c;
            n--;
        }
        return c;
    }
}
