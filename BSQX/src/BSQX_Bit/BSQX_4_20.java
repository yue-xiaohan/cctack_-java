package BSQX_Bit;

import java.util.Scanner;
class BNode {
    int val;
    BNode next=null;

    public BNode(int val) {
        this.val = val;
    }
}
public class BSQX_4_20 {
    /*反转指定链表*/
    public static void main2(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int[] arr=new int[n];
        for (int i = 0; i < n; i++) {
            arr[i]=scanner.nextInt();
        }
        BNode head=Gzhao(arr);
        Pring(head);
        int start= scanner.nextInt();
        int end= scanner.nextInt();
        head=reverseBetween(head,start,end);
        Pring(head);


    }
    public static BNode Gzhao(int[] arr){
        BNode head=new BNode(arr[0]);
        BNode cur=head;
        for (int i = 1; i < arr.length; i++) {
            BNode ret=new BNode(arr[i]);
            cur.next=ret;
            cur=ret;
        }
        return head;
    }
    public static void Pring(BNode head){
        while (head != null){
            System.out.print(head.val+" ");
            head=head.next;
        }
    }
    public static BNode reverseBetween (BNode head, int m, int n) {
        //加个表头
        BNode res = new BNode(-1);
        res.next = head;
        //前序节点
        BNode pre = res;
        //当前节点
        BNode cur = head;
        //找到m
        for(int i = 1; i < m; i++){
            pre = cur;
            cur = cur.next;
        }
        //从m反转到n
        for(int i = m; i < n; i++){
            BNode temp = cur.next;
            cur.next = temp.next;
            temp.next = pre.next;
            pre.next = temp;
        }
        //返回去掉表头
        return res.next;
    }
    /*数学题-猴子分桃
    * 写对了,但是复杂度过大了*/
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n=in.nextInt();//代表几只猴子
        while (n != 0){
            boolean flg=false;
            Let let=new Let(n);

            BSQX_4_20 s1=new BSQX_4_20();
            while (!let.flg){
                let.b=0;
                s1.Fun(n,let);
                if (let.flg){
                    break;
                }else {
                    let.a++;
                }
            }
            //这个循环走出来的时候,老猴子的桃是没有加最后的余数的
            System.out.println(let.a+" "+ let.b);
            let.a= let.b=0;let.flg=false;
           n =in.nextInt();

        }


    }
    static class Let{
        int a=0;//代表桃子总数
        int b=0;//代表老猴子最后得到多少
        boolean flg=false;//增加一个判定位
        public Let(int a){
            this.a=a;//代表最少的桃子
        }
    }


    public Let Fun(int n,Let let){//k 代表
        int s= let.a;
        while (n > 0){
            if ((s-1)%5 == 0){
                int k=(s-1)/5;
                s=k*4;
                let.b++;
            }else {
                break;
            }
            n--;
        }
        if (n == 0){
            let.flg=true;
            let.b+=s;
        }
        return let;
    }
}
