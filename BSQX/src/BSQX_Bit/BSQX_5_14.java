package BSQX_Bit;

import javax.security.sasl.SaslClient;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Stack;


public class BSQX_5_14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()){
            String ret = in.nextLine();
            char[][] chars =new char[10][10];
            for (int i = 0; i < 10; i++) {
                ret = in.nextLine();
                chars[i] = ret.toCharArray();
            }
            ArrayList<Integer> list = new ArrayList<>();
            dfs(chars,0,1,list,0);
            int size = list.size();
            int k = Integer.MAX_VALUE;
            for (int i = 0; i < size; i++) {
                if ( k < list.get(i))k = list.get(i);
            }
            System.out.println( k );
        }
    }
    public static void DFS(Node node){
        if (node == null)return;
        Stack<Node> stack=new Stack<>();
        HashSet<Node> set=new HashSet<>();
        /*这个栈永远保持这个深度优先的路径*/
        /*这个set表示有没有走过,去重*/
        stack.add(node);
        set.add(node);
        System.out.println(node.value);
        while (!stack.isEmpty()){
            Node cur =stack.pop();
            for (Node next : cur.nexts){
                if (!set.contains(next)){
                    /*注意这里的细节,当邻居不在表set中,就要把原来的节点也要重新押入栈
                     * 在把邻居压入栈中,表中*/
                    stack.push(cur);
                    stack.push(next);
                    set.add(next);
                    System.out.println(next.value);
                    //输出语句同样可以换成别的数据操作语句
                    break;
                    //压入进去后,直接返回,继续查询
                }
            }
        }
    }

    public static void   dfs(char[][] chars, int R , int C, ArrayList<Integer> list,int bs){
//        入口: (0 ,1)  -  出口 (9 ,8)
        if (R <0 || C <0 || R > chars.length || C > chars.length || chars[R][C] == '#'){
            return ;
        }
        if (R == 9 && C == 8){
            //说明找到了出口,
            list.add(bs);
            return;
        }
        bs++;
        dfs(chars,R+1,C,list,bs);
        dfs(chars,R-1,C,list,bs);
        dfs(chars,R,C+1,list,bs);
        dfs(chars,R,C-1,list,bs);
    }
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()){
            String ret = in.nextLine();
            for (int i = 0; i < ret.length(); i++) {
                if (ret.charAt(i) >= '0' && ret.charAt(i) <='9'){
                    System.out.print(ret.charAt(i));
                }
            }
            System.out.println();
        }
    }
}
