package BSQX_Bit;

import javafx.scene.transform.Scale;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class BSQX_5_10 {
    public static void main2(String[] args) {
        Scanner scan =  new Scanner(System.in);
        while(scan.hasNext()) {
            String s1 = scan.next();
            String s2 = scan.next();
            HashSet<Character> set = new HashSet<>();
            for (Character ch : s1.toCharArray()){
                if (!set.contains(ch)){
                    set.add(ch);
                }
            }
            int count=0;
            HashSet<Character> set2 = new HashSet<>();
            for (Character ch : s2.toCharArray()){
                if (set.contains(ch) && !set2.contains(ch)){
                    count++;
                }
                set2.add(ch);
            }
            System.out.print(count+" ");

        }
    }
    //错的
    public static void main(String[] args) {
        Scanner scan =  new Scanner(System.in);
        while(scan.hasNext()) {
            String s1 = scan.next();
            String s2 = scan.next();
            HashMap<Character,Integer> map =new HashMap<>();
            for (Character ch : s1.toCharArray()){
                map.put(ch,map.getOrDefault(ch,0)+1);
            }
            int count=0;
            HashMap<Character,Integer> map2 =new HashMap<>();
            for (Character ch : s2.toCharArray()){
               if (map.get(ch)!=null&&map.get(ch) > 0){
                   count++;
                   map.put(ch,map.getOrDefault(ch,0)-1);
               }
            }
            System.out.print(count+" ");

        }
    }
    public static void main1(String[] args){
        Scanner scan =  new Scanner(System.in);
        while(scan.hasNext()){
            String s1 =scan.next();
            String s2 = scan.next();
            int len1 =scan.nextInt();
            int len2 = scan.nextInt();
            // 先拿到s1的长度，如果小于len2，就给它补相应的位数，由于它是起始位，所以补a
            for(int i=s1.length();i<len2;i++){
                s1 +='a';
            }
            // 拿到s2的长度，如果小于len2，也给它补相应的位数，由于它是结束位，所以补'z'+1
            for(int i=s2.length();i<len2;i++){
                s2+='z'+1;
            }
            // 用一个数组，记录两个字符串对应位相减的值， 比如s1补位后为 aba，s2补位后为 ce('z'+1)，arr数组记录的值是 {2，3，26}
            int[] arr = new int[len2];
            // 由于下标比长度小1，所以是取不到len2的
            for(int i=0;i<len2;i++){
                arr[i] = s2.charAt(i)-s1.charAt(i);
            }
            // 用一个变量记录总的数值
            int sum = 0;
            // 从长度len1开始，依次计算每个长度下的字符串个数，这里能取到len2
            for(int i= len1;i<=len2;i++){
                // 每个长度下，都从arr数组中取值，让它乘以相应的26的次方，比如数组中是{2，3}，len为2时，就是2*Math.pow(26,1)+3*Math.pow(26,0)。
                // len为3时，就是 2*Math.pow(26,2)+3*Math.pow(26,1)
                for(int j=0;j< i;j++){
                    // 次方既受 len的影响，也受位数的影响，所以是 i-j-1
                    sum+=arr[j]* Math.pow(26,i-1-j);
                }
            }
            System.out.println((sum-1)%1000007);
        }
    }



}
