package BSQX_Bit;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Scanner;

public class BSQX_3_30 {
    /*二叉树最近公共祖先:LCA*/
    /*解题思路:
    * 左孩子:2i 右孩子:2i+1
    * 这是层序遍历*/
    public static int getLCA(int a, int b) {
        //先判断根节点
        if (a == 1 || b == 1)return 1;

        int max=a >= b?a:b;
        int min=a <= b?a:b;
        int c=0;
        if (min  != max){
            c=getLCA(max/2,min);
        }else {
            c=min;
        }
        return c;
    }
    public static void main(String[] args) {
        int ret=getLCA(3,11);
        System.out.println(ret);
    }
    /*连续最大二进制数*/
    public static void main1(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        System.out.println(func(n));
    }
    public static int func(int n){
        int[] arr=new int[32];
        int count=0;
        int k=1;
        for (int i = 0; i < 32; i++) {
            if ((n & k) == 1){
                count++;
            }else {
                arr[i]=count;
                count=0;
            }
           n= n>>1;
        }
        int max=Integer.MIN_VALUE;
        for (int i = 0; i < 32; i++) {
            if (max < arr[i])max=arr[i];
        }
        return max;
    }

}
