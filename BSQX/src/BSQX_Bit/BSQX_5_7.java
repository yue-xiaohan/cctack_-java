package BSQX_Bit;

import javax.security.sasl.SaslClient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class BSQX_5_7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            int n = sc.nextInt();
            String[] strings = new String[n];
            for (int i = 0; i < n; i++) {
                strings[i] = sc.next();
            }
            Arrays.sort(strings);
            for(int i = 1; i < n; i++){
                if(strings[i].contains(strings[i-1]) && strings[i].charAt(strings[i-1].length()) == '/'){
                    strings[i-1] = "-1";
                }
            }
            for(int i = 0; i < n; i++){
                if(!"-1".equals(strings[i])){
                    System.out.println("mkdir -p "+strings[i]);
                }
            }
            System.out.println();
        }

    }
    public static void main1(String[] args) {

        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();

            int pre = 0;
            int max = 0;
            for(int i = 0;i < n;i++){
                String s2 = sc.next();
                String s =sc.next();
                if(s.equals("connect")){
                    pre++;
                    max = Math.max(max,pre);
                }else if(s.equals("disconnect")){
                    pre--;
                }
            }
            System.out.println(max);
        }
    }
}
