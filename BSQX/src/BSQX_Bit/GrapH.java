package BSQX_Bit;

import java.util.HashMap;
import java.util.HashSet;

/*这个要补充一点说明,这个图结构是为了兼顾所有的图*/
public class GrapH {


    /*注意:这里面实现的都是有向图*/
    //key表示点的编号,Node表示实际结构
    public HashMap<Integer,Node> nodes;//
    //边集,装着所有的边
    public HashSet<Edge> edges;
    public GrapH(){
        nodes=new HashMap<Integer, Node>();
        edges=new HashSet<Edge>();
    }
}
