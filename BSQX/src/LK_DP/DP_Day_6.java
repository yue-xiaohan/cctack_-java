package LK_DP;

public class DP_Day_6 {
    public static void main(String[] args) {
        int [] arr={2,3,-2,4};
    }
    /*给你一个整数数组 nums ，请你找出数组中乘积最大的非空连续子数组（该子数组中至少包含一个数字），并返回该子数组所对应的乘积。
    测试用例的答案是一个 32-位 整数。
    子数组 是数组的连续子序列。
    f(n)=max(num[i]*dp[i-1],num[i])这个是正数
    dp[0]=num[0],这个想法是错误的因为没考虑负数的情况
    所以:应该还有一个动态规划方程-
    ,f(n)= min{fmax(i−1)×ai,fmin(i−1)×ai,ai }*/
    public int maxProduct(int[] nums) {
        int length = nums.length;
        int[] maxF = new int[length];
        int[] minF = new int[length];
        System.arraycopy(nums, 0, maxF, 0, length);
        System.arraycopy(nums, 0, minF, 0, length);
        for (int i = 1; i < length; ++i) {
            maxF[i] = Math.max(maxF[i - 1] * nums[i], Math.max(nums[i], minF[i - 1] * nums[i]));
            minF[i] = Math.min(minF[i - 1] * nums[i], Math.min(nums[i], maxF[i - 1] * nums[i]));
        }
        int ans = maxF[0];
        for (int i = 1; i < length; ++i) {
            ans = Math.max(ans, maxF[i]);
        }
        return ans;
    }


    /*给你一个整数数组 nums ，请你求出乘积为正数的最长子数组的长度。
    一个数组的子数组是由原数组中零个或者更多个连续数字组成的数组。
    请你返回乘积为正数的最长子数组长度。*/
//    public int getMaxLen(int[] nums) {
//        int length = nums.length;
//        int count=0;
//        int cj=0;
//        int js=0;
//        int[] dp=new int[length];
//        if (nums[0] < 0){
//            cj++;
//        }
//        dp[0]=nums[0];
//        for (int i = 1; i < length; ++i) {
//            if (nums[i] > 0){
//                js=Math.max(nums[i],nums[i]*dp[i-1]);
//                dp[i]=
//            }else {
//                cj++;
//            }
//        }
//
//        return count;
//    }


}
