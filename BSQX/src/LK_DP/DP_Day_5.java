package LK_DP;

public class DP_Day_5 {
    /*给你一个整数数组 nums ，请你找出一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。
    子数组 是数组中的一个连续部分。*/
    public int maxSubArray(int[] nums) {
//        解题思路:动态规划
        /*动态规划的解题方法:1定义状态,2编写状态转移方程,3设置初始值
         * f(n) =f(n-1)+num[i]/num[i]*/
        int[] dp=new int[nums.length];
        dp[0]=nums[0];
        int res = dp[0];
        for (int i = 1; i < nums.length; i++) {
            if (dp[i - 1] > 0) {
                dp[i] = dp[i - 1] + nums[i];
            } else {
                dp[i] = nums[i];
            }
            if (res < dp[i]){
                res=dp[i];
            }
        }
        return res;
    }
    /*给定一个长度为 n 的环形整数数组 nums ，返回 nums 的非空 子数组 的最大可能和 。
    环形数组 意味着数组的末端将会与开头相连呈环状。形式上， nums[i] 的下一个元素是 nums[(i + 1) % n] ，
     nums[i] 的前一个元素是 nums[(i - 1 + n) % n] 。
    * 直接两种情况，1：最大数组和在中间，和平时一样解法 2：最大数组和是跨越头尾，回头了，
    * 麻烦第二种，从两边出发往中间靠拢必须都是最大，那就说明中间段就是最小，找最小不就行了，*/
    public int maxSubarraySumCircular(int[] nums) {
        int dp = nums[0],max = dp, sum = dp,min = 0;
        for(int i = 1; i < nums.length; i++){
            sum += nums[i];
            dp = nums[i] + Math.max(dp, 0);
            max = Math.max(dp, max);
        }
        dp = nums[0];
        for(int i = 1; i < nums.length -1; i++){
            dp = nums[i] + Math.min(0,dp);
            min = Math.min(dp,min);
        }
        return Math.max(sum-min,max);
    }
}
