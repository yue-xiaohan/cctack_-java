package LK_DP;

public class DP_Day_7 {
    /*你只能选择 某一天 买入这只股票，并选择在 未来的某一个不同的日子 卖出该股票。设计一个算法来计算你所能获取的最大利润。
        返回你可以从这笔交易中获取的最大利润。如果你不能获取任何利润，返回 0*/
    public int maxProfit(int prices[]) {
        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice) {
                minprice = prices[i];
            } else if (prices[i] - minprice > maxprofit) {
                maxprofit = prices[i] - minprice;
            }
        }
        return maxprofit;
    }
    /*给你一个正整数数组 values，其中 values[i] 表示第 i 个观光景点的评分，并且两个景点 i 和 j 之间的 距离 为 j - i。
    一对景点（i < j）组成的观光组合的得分为 values[i] + values[j] + i - j ，也就是景点的评分之和 减去 它们两者之间的距离。
    返回一对观光景点能取得的最高分。*/
//    public int maxScoreSightseeingPair(int[] values) {
//
//    }
    /*你在任何时候 最多 只能持有 一股 股票。你也可以先购买，然后在 同一天 出售。
    返回 你能获得的 最大 利润 。*/
    public int maxProfit2(int[] prices) {
        if (prices == null){
            return 0;
        }
        int maxprofit=0;
        int minprice = prices[0];
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] - minprice > 0) {
                maxprofit += prices[i] - minprice;
            }
            minprice = prices[i];
        }
        return maxprofit;
    }
    public int maxScoreSightseeingPair(int[] values) {
        int ans = 0, mx = values[0] + 0;
        for (int j = 1; j < values.length; ++j) {
            ans = Math.max(ans, mx + values[j] - j);
            // 边遍历边维护
            mx = Math.max(mx, values[j] + j);
        }
        return ans;
    }



    public static void main(String[] args) {
        int[] arr={7,1,5,3,6,4};
        DP_Day_7 s1=new DP_Day_7();
        s1.maxProfit2(arr);
    }
}
