package LK_DP;

public class DP_Day_8 {
    public int maxProfit(int[] prices) {
        if (prices == null || prices.length == 1){
            return 0;
        }
        int max=0;
        boolean flag=false;
        int fit=0;
        for (int i = 0; i < prices.length; i++) {
            if (max < prices[i] && !flag){
                fit +=prices[i]-max;
                max=prices[i];
               flag=true;
            }else {
                flag = false;
                continue;
            }
        }
        return fit;
    }
}
