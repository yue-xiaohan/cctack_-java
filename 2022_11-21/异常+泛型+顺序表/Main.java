import java.util.Scanner;

public class Main {
    // 注意类名必须为 Main, 不要有任何 package xxx 信息
    public static void main(String[] args) {
        //输入两个正整数a和b，输出这两个正整数的和，差，积，商，模（若a>b则输出a-b，a/b，a%b的值反之输出b-a，b/a，b%a的值，不考虑小数，请使用int类型）
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        System.out.print(a + b+" ");
        System.out.print(Math.abs(a - b)+" ");
        System.out.print(a * b);
        if(a/b<=0){
            System.out.print(" "+(int)(b / a));
        }
        else{
            System.out.print(" "+(int)(a / b));
        }

        System.out.print(" "+(int)(a % b));

    }
        public static void main1(String[] args) {
            Scanner in = new Scanner(System.in);
            // 注意 hasNext 和 hasNextLine 的区别
                double a=in.nextDouble();
                int b=(int) a;
                System.out.println(b);

         }
}
