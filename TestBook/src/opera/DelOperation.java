package opera;

import book.Book;
import book.BookList;

import java.util.Scanner;


public class DelOperation implements IOPeration{
    @Override
    public void work(BookList bookList) {
        System.out.println("删除图书！");
        System.out.println("请输入你要删除的图书的名字：");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        int currentSize = bookList.getUsedSize();
        //找到要删除书的下标

        int index = -1;
        for (int i = 0; i < currentSize; i++) {
            Book tmp = bookList.getBook(i);
            if(tmp.getName().equals(name)) {
                index = i;
                break;
            }
        }

        //挪动数据-从后先前覆盖
        for (int j = index; j < currentSize-1; j++) {//注意循环条件
            //bookList[j] = bookList[j+1];
            Book book = bookList.getBook(j+1);
            bookList.setBook(j, book);
        }
        //修改size
        bookList.setUsedSize(currentSize-1);
        //因为删除的是对象  所以 把最后一个置为null
        bookList.setBook(currentSize-1,null);
        System.out.println("删除成功！");
    }
}