import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/hello")
//将这个helloServlet这个类,与http请求中的URL里面带有/hello这样的请求给关联起来了
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*不能调用父类的duget*/
       // super.doGet(req, resp);

        //让服务器在自己的控制台中打印
        System.out.println("hello world");

        //让在页面上输出getWrite返回了一个write对象,write是往http响应的body中写入
        resp.getWriter().write("hello world"+"时间戳"+System.currentTimeMillis());
    }
}
