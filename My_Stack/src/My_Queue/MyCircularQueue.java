package My_Queue;
import java.util.*;
class MyCircularQueue {
//    设计循环队列
    /*
 MyCircularQueue(k): 构造器，设置队列长度为 k 。
Front: 从队首获取元素。如果队列为空，返回 -1 。
Rear: 获取队尾元素。如果队列为空，返回 -1 。
enQueue(value): 向循环队列插入一个元素。如果成功插入则返回真。
deQueue(): 从循环队列中删除一个元素。如果成功删除则返回真。
isEmpty(): 检查循环队列是否为空。
isFull(): 检查循环队列是否已满。
* */
    private int[] elem;
    private int front;
    private int rear;
   // private int usedSize;//这样写太简单了
    public MyCircularQueue(int k) {
        elem=new int[k+1];
    }
    
    public boolean enQueue(int value) {
        if (isFull()){
            return false;
        }
        elem[rear]=value;
        rear=(rear+1)% elem.length;
       // usedSize++;
        return true;
    }
    
    public boolean deQueue() {
        if (isEmpty()){
            return false;
        }
        front=(front+1)%elem.length;
       // usedSize--;
        return true;
    }
    
    public int Front() {
        if (isEmpty()){
            return -1;
        }
        return elem[front];
    }
    
    public int Rear() {
        if (isEmpty()){
            return -1;
        }
        int index=rear!=0?rear-1:elem.length-1;
        return elem[index];

    }
    
    public boolean isEmpty() {
        //return usedSize==0;
        return rear==front;
    }
    
    public boolean isFull() {
        //return  usedSize==elem.length;
        return (rear+1)%elem.length==front;
    }


}