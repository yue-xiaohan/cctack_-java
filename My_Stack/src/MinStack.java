import java.util.Stack;

public class MinStack {
   /* 力扣：155. 最小栈
   设计一个支持 push ，pop ，top 操作，并能在常数时间内检索到最小元素的栈。
    实现 MinStack 类:*/
   private Stack<Integer> stack ;
    private Stack<Integer> minStack ;
    public MinStack() {
        stack = new Stack<>();
        minStack = new Stack<>();
    }
    public void push(int val) {
        stack.push(val);
        if (minStack.isEmpty()){
            minStack.push(val);
        }else {
            if (minStack.peek()>=val){
                minStack.push(val);
            }
        }
    }

    public void pop() {
        if(!stack.empty()) {
            Integer val = stack.pop();
            //维护最小栈
            if (val.equals(minStack.peek())) {
                minStack.pop();
            }
        }
    }

    public int top() {
        if (!stack.isEmpty()){
            return stack.peek();
        }
        return -1;
    }

    public int getMin() {
        if (!minStack.isEmpty()){
            return minStack.peek();
        }
        return -1;
    }
}
