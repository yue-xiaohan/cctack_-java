import java.util.*;
//用队列实现栈
class MyStack1 {
  /*  请你仅使用两个队列实现一个后入先出（LIFO）的栈，并支持普通栈的全部四种操作（push、top、pop 和 empty）*/
    private Queue<Integer> queue1;
    private Queue<Integer> queue2;
    public MyStack1() {
        queue1=new LinkedList<>();
        queue2=new LinkedList<>();
    }
    
    public void push(int x) {
        if (!queue1.isEmpty()){
            queue1.offer(x);
        }else if (!queue2.isEmpty()){
            queue2.offer(x);
        }else {
            queue1.offer(x);//如果俩个队列都是为空，则默认进入qu1中
        }
    }
    
    public int pop() {
      if (empty()){
           return -1;
        }
      if (!queue1.isEmpty()){
          int size=queue1.size();
          for (int i = 0; i < size-1; i++) {
              int tmp=queue1.poll();
              queue2.offer(tmp);
          }
          return queue1.poll();
      }else {
          int size=queue2.size();
          for (int i = 0; i < size-1; i++) {
              int tmp=queue2.poll();
              queue1.offer(tmp);
          }
          return queue2.poll();
      }
    }
    
    public int top() {
        if (empty()){
            return -1;
        }
        int k=pop();
        if (!queue1.isEmpty()){
           queue1.offer(k);
        }else {
            queue2.offer(k);
        }
        return k;
    }
    
    public boolean empty() {
        return queue1.isEmpty()&& queue2.isEmpty();
    }

    public static void main(String[] args) {
        MyStack myStack=new MyStack();
        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);

        myStack.push(5);
        System.out.println(myStack.pop());
        System.out.println(myStack.top());

    }
}
//官方解答
class MyStack {
    private Queue<Integer> a;  // a是用来辅助导元素的
    private Queue<Integer> b;  // 元素全部放在b中
    public MyStack() {
        a = new LinkedList<>();
        b = new LinkedList<>();
    }

    public void push(int x) {
        // 先把元素放入a队列中
        a.offer(x);

        while(!b.isEmpty()){
            a.offer(b.poll());
        }

        Queue<Integer> temp = a;
        a = b;
        b = temp;
    }

    public int pop() {
        return b.poll();
    }

    public int top() {
        return b.peek();
    }

    public boolean empty() {
        return b.isEmpty();
    }
}