import java.util.Arrays;
import java.util.Random;

/**
 * @Author 12629
 * @Description：
 */
public class Test {

    public static void initArrayOrder(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
            //array[i] = array.length-i;
        }
    }

    public static void initArrayNotOrder(int[] array) {
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10_0000);
        }
    }

    public static void testInsertSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
       // Sort.insertSort1(array);
        long endTime = System.currentTimeMillis();
        System.out.println("直接插入排序耗时："+ (endTime-startTime));
    }

    public static void testShellSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
       // Sort.shellSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("希尔排序耗时："+ (endTime-startTime));
    }

    public static void testSelectSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
       // Sort.selectSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("选择排序耗时："+ (endTime-startTime));
    }

    public static void testHeapSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.heapSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("堆排序耗时："+ (endTime-startTime));
    }

    public static void testQuickSort(int[] array) {
        array = Arrays.copyOf(array,array.length);
        long startTime = System.currentTimeMillis();
        Sort.quickSort(array);
        long endTime = System.currentTimeMillis();
        System.out.println("快速排序耗时："+ (endTime-startTime));
    }

    public static void main2(String[] args) {
        int[] array = new int[100_0000];
        initArrayOrder(array);
        //initArrayNotOrder(array);
        //testInsertSort(array);

        //testSelectSort(array);

        //testShellSort(array);//n^1.3

        //testHeapSort(array);//n*logn

        testQuickSort(array);
    }

    public static void main(String[] args) {
        int[] array = {1,21,3,14,51,6,17};
        //Sort.insertSort(array);
        //Sort.shellSort(array);
        //Sort.selectSort2(array);
        //Sort.heapSort(array);
        //Sort.bubbleSort(array);
        //Sort.quickSort(array);
        Sort.mergeSort(array);
        System.out.println(Arrays.toString(array));
    }
}