package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;

@RestController
//http://localhost:8080/hi本地访问
/*这个注解 表示
* @Controller(控制器)与ResponseBody(返回的是数据而非页面) 俩个注解的合一 */
public class TestController {
    @Value("${mytest}")
    private String mytest;
    @Value("${mytest2}")
    private String mytest2;

    @Value("${myString}")
    private String myString;
    @Value("${myString2}")
    private String myString2;
    @Value("${myString3}")
    private String myString3;

    @Autowired
    private Student student;






    @PostConstruct
    public void postConstruct(){
        System.out.println(student);
//        System.out.println("mystring" + myString);
//        System.out.println("mystring2" + myString2);
//        System.out.println("mystring3" + myString3);
    }



    @RequestMapping("/hi") // 代表路由URL(效果,映射)
    public String sayH(String name){
//        if (name == null || name.equals("")){
//
//        }
        if (!StringUtils.hasLength(name)) {
            //效果等同于上面的if语句
            name = "李四";
        }
        return "你好" +name;
    }
    @RequestMapping("/getconf")
    public String getConfig(){
        return mytest +"|||"+ mytest2+"|||";
    }
}
