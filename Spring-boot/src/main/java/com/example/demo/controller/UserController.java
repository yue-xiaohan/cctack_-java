package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.xml.ws.RespectBinding;

@Controller
@ResponseBody // 用来设置当前类所有的方法返回的是数据而非页面
@RequestMapping("/user")//一级
public class UserController {
    // 1.得到日志对象
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    //使用日志对来打印
    @RequestMapping("/hi")//最小的指定路径是方法路由
    public String sayH(){
        //写日志
        logger.trace("我是trace 日志");
        logger.debug("我是debug 日志");
        logger.info("我是info 日志");
        logger.warn("我是 warn");
        return "Hi,Spring Boot ";

    }




}
