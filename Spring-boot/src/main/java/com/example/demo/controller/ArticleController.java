package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/art")
@Slf4j
public class ArticleController {
    // 1:得到日志对象
   // private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);
    @RequestMapping("/hi")
    public String sayH(){
        log.trace("我是sjf4j 的trace");
        return "Hi , RI zhi";
    }
}

