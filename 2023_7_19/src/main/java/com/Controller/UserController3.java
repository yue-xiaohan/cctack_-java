package com.Controller;


import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 用户控制器 - 版本3
 * 作者：李四
 */
@Controller
public class UserController3 {

    @Autowired
    private User user;

    public void doMethod() {
        System.out.println("UserController3：user -> " + user);
    }

}
