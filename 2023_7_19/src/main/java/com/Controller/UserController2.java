package com.Controller;


import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * 用户控制器 - 版本2
 * 作者：张三
 */
@Controller
public class UserController2 {

    @Autowired
    private User user;

    public void doMethod() {
        User user2 = user;
        System.out.println("UserController2 修改之前：User -> " + user);
        user2.setName("三三");
        System.out.println("UserController2 修改之后：User -> " + user);
    }


}
