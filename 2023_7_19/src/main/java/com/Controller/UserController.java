package com.Controller;

import com.Service.USerServices;
import com.model.User;

import com.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class UserController {
    // 一般是给前端页面返回对像
    @Autowired
    private USerServices uSerServices;

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-congif.xml");
        ApplicationContext context2 = new ClassPathXmlApplicationContext("spring-congif.xml");
        Users user1 = context.getBean("user",Users.class);
        Users user2 = context2.getBean("user",Users.class);
        System.out.println(user1==user2);

    }
}
