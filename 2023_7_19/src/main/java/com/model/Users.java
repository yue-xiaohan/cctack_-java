package com.model;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * 公共类
 */
@Component
public class Users {
    /**
     * 公共对象 -> 默认单例模式
     * @return
     */
    @Bean(name = "user")
    @Scope("prototype") // 原型模式|多例模式
    public User getUser(){
        User user = new User();
        user.setId(666);
        user.setName("孙猴子");
        return user;
    }
}
