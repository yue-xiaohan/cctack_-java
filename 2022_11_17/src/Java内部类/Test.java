package demo1;


class OuterClass {
    public int data1 = 1;//
    private int data2 = 2;
    public static int data3 = 3;
    //静态内部类
    static class InnerClass {
        public int data4 = 4;
        private int data5 = 5;
        public static int data6 = 6;

        public void func() {
            System.out.println("static->InnerClass::func()");
            OuterClass outerClass = new OuterClass();
            System.out.println(outerClass.data1);
            System.out.println(outerClass.data2);
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);
        }
    }

    public void test() {
        InnerClass innerClass = new InnerClass();
        System.out.println(data1);
        System.out.println(data2);
        System.out.println(data3);
        System.out.println(innerClass.data4);
        System.out.println(innerClass.data5);//外部类 可以访问静态内部类当中的所有成员
        System.out.println(InnerClass.data6);
    }
}

class OuterClass2 {
    public int data1 = 1;
    private int data2 = 2;
    public static int data3 = 3;
    //非静态内部类
    class InnerClass2 {
        public int data1 = 11111;
        public int data4 = 4;
        private int data5 = 5;
        //此时当外部类加载的时候，这个非静态内部类不会加载
        public static final int data6 = 6;

        public void func() {
            System.out.println("InnerClass::func()");
            System.out.println(this.data1);//????
            System.out.println("===== "+ OuterClass2.this.data1);//????

            System.out.println(data2);
            System.out.println(data3);
            System.out.println(data4);
            System.out.println(data5);
            System.out.println(data6);
        }
    }

    public void test() {
        InnerClass2 innerClass2 = new InnerClass2();
        System.out.println(data1);
        System.out.println(data2);
        System.out.println(data3);
        System.out.println(innerClass2.data4);
        System.out.println(innerClass2.data5);
        System.out.println(InnerClass2.data6);
    }
}

class OuterClass3 {

    public void func() {
        class InnerClass {
            public int a = 1;
            public void test() {
                System.out.println("hello!");
            }
        }
        InnerClass innerClass = new InnerClass();
        innerClass.test();
    }
}

interface IA {
    void func();
}
class AA implements IA {
    @Override
    public void func() {
        System.out.println("hello!!! ");
    }
}
public class Test {

    public static void main(String[] args) {
        new IA(){
            @Override
            public void func() {
                System.out.println("我去！");
            }
        }.func();
        //new IA() {};
    }

    public static void main4(String[] args) {
        IA ia = new AA();
        ia.func();
    }

    public static void main3(String[] args) {
        OuterClass3 outerClass3 = new OuterClass3();
        outerClass3.func();
    }

    public static void main2(String[] args) {
        OuterClass2 outerClass2 = new OuterClass2();
        OuterClass2.InnerClass2 innerClass2 = outerClass2.new InnerClass2();
        innerClass2.func();
    }
    public static void main1(String[] args) {
        OuterClass.InnerClass innerClass = new OuterClass.InnerClass();
        innerClass.func();
    }
}