import java.util.*;

/**
 * @Author 12629
 * @Description：
 *///
 class MyStack {
    public int[] elem;
    public int usedSize;

    public MyStack() {
        this.elem = new int[10];
    }
    //压栈
    public void push(int val) {
        if(isFull()) {
            //扩容
            elem = Arrays.copyOf(elem,2*elem.length);
        }
        elem[usedSize++] = val;
    }
    public boolean isFull() {
        return usedSize == elem.length;
    }
    //出栈
    public int pop() {
        if(isEmpty()) {
            throw new EmptyException("栈是空的！");
        }
        /*int val = elem[usedSize-1];
        usedSize--;
        return val;*/
       /* usedSize--;
        return elem[usedSize];*/
        return elem[--usedSize];
    }
    public boolean isEmpty() {
        return usedSize == 0;
    }
    public int peek() {
        if(isEmpty()) {
            throw new EmptyException("栈是空的！");
        }
        return elem[usedSize-1];
    }
}
