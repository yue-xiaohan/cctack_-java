import java.util.*;

/**
 * @Author 12629
 * @Description：
 */
public class Test {

    public static void main(String[] args) {
        Deque<Integer> deque1 = new LinkedList<>();
        Deque<Integer> deque2 = new ArrayDeque<>();


        Stack<Integer> stack = new Stack<>();

        Deque<Integer> stack2 = new ArrayDeque<>();
        stack2.push(1);
        stack2.push(2);
        stack2.push(3);
    }

    public static void main6(String[] args) {
        Deque<Integer> deque = new LinkedList<>();
        Queue<Integer> queue = new LinkedList<>();
        LinkedList<Integer> stack = new LinkedList<>();
        List<Integer> list = new LinkedList<>();
    }
    public static void main5(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        queue.offer(1);//尾插法
        queue.offer(2);
        queue.offer(3);
        queue.offer(4);
        queue.offer(5);
        System.out.println(queue.peek());//1
        System.out.println(queue.poll());//1
        System.out.println(queue.poll());//2
    }
    public static void main4(String[] args) {
        MyQueue myQueue = new MyQueue();
        myQueue.offer(1);
        myQueue.offer(2);
        myQueue.offer(3);
        System.out.println(myQueue.peek());//1
        System.out.println(myQueue.poll());//1
        System.out.println(myQueue.poll());//2
        System.out.println(myQueue.poll());//3
    }

    public static void main3(String[] args) {
        LinkedList<Integer> stack = new LinkedList<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        System.out.println(stack.pop());
        System.out.println(stack.peek());
    }

    public int evalRPN(String[] tokens) {
        Stack<Integer> stack = new Stack<>();
        for(String x : tokens){
            if(!isOperation(x)) {
                stack.push(Integer.parseInt(x));
            }else {
                int num2 = stack.pop();
                int num1 = stack.pop();
                switch (x) {
                    case "+":
                        stack.push(num1+num2);
                        break;
                    case "-":
                        stack.push(num1-num2);
                        break;
                    case "*":
                        stack.push(num1*num2);
                        break;
                    case "/":
                        stack.push(num1/num2);
                        break;
                }
            }
        }
        return stack.pop();
    }

    private boolean isOperation(String x) {
        if(x.equals("+") || x.equals("-") || x.equals("/")||x.equals("*")) {
            return true;
        }
        return false;
    }


    // 栈里面放的一定是左括号
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if(ch == '(' || ch == '{' || ch == '[') {
                stack.push(ch);
            }else {
                //遇到了右括号
                if(stack.empty()) {
                    return false;
                }
                char ch2 = stack.peek();//左括号
                if(ch == ')' && ch2 == '(' || ch == '}' && ch2 == '{' || ch == ']' && ch2 == '[') {
                    stack.pop();
                }else {
                    return false;//不匹配
                }
            }
        }
        if(!stack.empty()) {
            return false;
        }
        return true;
    }

    public boolean isPopOrder(int [] pushA,int [] popA) {

        Stack<Integer> stack = new Stack<>();
        int j = 0;
        for (int i = 0; i < pushA.length; i++) {
            stack.push(pushA[i]);
            while (j < popA.length && !stack.empty() && stack.peek().equals(popA[j])) {
                stack.pop();
                j++;
            }
        }
        return stack.empty();
    }



    public static void main2(String[] args) {
        MyStack stack = new MyStack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        Integer a = stack.pop();//3
        System.out.println(a);
        Integer b = stack.peek();
        System.out.println(b);//2
        Integer b2 = stack.peek();
        System.out.println(b2);//2
        System.out.println(stack.isEmpty());
    }
    public static void main1(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        Integer a = stack.pop();//3
        System.out.println(a);
        Integer b = stack.peek();
        System.out.println(b);//2
        Integer b2 = stack.peek();
        System.out.println(b2);//2
        System.out.println(stack.size());
        System.out.println(stack.isEmpty());
    }
}