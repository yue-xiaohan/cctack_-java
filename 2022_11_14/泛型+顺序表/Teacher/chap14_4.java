package Teacher;


interface A {//
    void funcA();
}
interface B {
    void funcB();
}
//CC这个接口 不仅仅具备func这个功能，还具备了A和B接口的功能
interface CC extends A,B {
    void funcC();
}

class C implements CC {
    public void funcA() {

    }

    public void funcB() {

    }

    public void funcC() {

    }

}
public class chap14_4 {
}
