package Teacher;
interface IShape {

    String name3 = "1";//
    void draw();
    /*default public void func() {
        System.out.println("默认的");//
    }*/
    public static void staticFunc() {
        System.out.println("staticFunc");
    }
}
class Rect implements IShape {

    @Override
    public void draw() {
        System.out.println("矩形！");
    }

    /*@Override
    public void func() {
        System.out.println("重写这个方法！");
    }*/
}
class Flower implements IShape {
    @Override
    public void draw() {
        System.out.println("❀！");
    }//
}

public class chap14_2  {
    public static void drawMap(IShape shape) {
        shape.draw();
    }
    public static void main(String[] args) {
        //IShape shape = new IShape();
        IShape shape = new Rect();//向上转型
        //Rect rect = new Rect();//向上转型
        IShape shape2 = new Flower();//向上转型
        drawMap(shape);
        drawMap(shape2);

        IShape.staticFunc();
    }
}
