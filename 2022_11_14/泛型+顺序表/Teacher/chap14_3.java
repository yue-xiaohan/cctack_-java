package Teacher;



abstract class Animal {//
    public String name;

    public Animal(String name) {
        this.name = name;
    }
}
interface IRunning {
    void run();
}
interface ISwimming {
    void swim();
}

interface IFly {
    void fly();
}

class Dog extends Animal implements IRunning {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(name +" 正在用四条狗腿跑！");
    }
}

class Fish extends Animal implements ISwimming{

    public Fish(String name) {
        super(name);
    }

    @Override
    public void swim() {
        System.out.println(name + "正在游泳！");
    }
}
class Bird extends Animal implements IFly {

    public Bird(String name) {
        super(name);
    }

    @Override
    public void fly() {
        System.out.println(name + "正在飞！");
    }
}
class Duck extends Animal implements IRunning,IFly,ISwimming {

    public Duck(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(name + "正在用两条腿跑！");
    }

    @Override
    public void swim() {
        System.out.println(name + "正在用两条腿在游泳！");
    }

    @Override
    public void fly() {
        System.out.println(name + "正在用翅膀飞！");
    }
}

class Robot implements IRunning{

    @Override
    public void run() {
        System.out.println("机器人在用机器腿跑步！");
    }
}

public class chap14_3 {

    /**
     * 只要实现了 IRunning 接口的 都可以接收
     * @param iRunning
     */
    public static void walk(IRunning iRunning) {
        iRunning.run();
    }

    public static void swim(ISwimming iSwimming) {
        iSwimming.swim();
    }

    /**
     * 一定是一个动物
     * @param animal
     */
    public static void func(Animal animal) {

    }

    public static void main(String[] args) {
        walk(new Dog("旺财"));
        walk(new Duck("唐老鸭"));
        walk(new Robot());

        System.out.println("======");
        swim(new Fish("七秒"));
        swim(new Duck("唐老鸭2号"));
    }
}
