
//接口

/**
 * 接口关键字：interface
 * 注意点：
 * 1：接口当中的成员默认是public static final修饰的
 * 2：接口不能被实例化
 * 3：接口当中的方法不写也是默认为public abstract的
 * 4：接口的方法不能有具体的方法,但从jdk8开始可以有一个默认的default实现
 * 5：类通过关键字implements 来引用接口名
 * 6：类必须重写接口中的方法：重写的shi'h子类的访问权限一定要大于等于父类的权限
 * 7: 接口中也可以有static修饰的方法
 * 8：接口中不能有构造方法，和静态代码块
 * 9：如果类没有实现接口的所有方法，类必须设置为抽象类
 */

interface IShape{
    void draw();//
    static void  func(){
        System.out.println("static方法");
    }
    default void funb(){
        System.out.println("default修饰的方法");
    }
}
class Rect implements IShape{
    @Override
    public void draw() {
        System.out.println("❀");

    }
}
public class chap14_0 {
    public static void drawMap(IShape shape){
        shape.draw();
    }

    public static void main(String[] args) {
        IShape shape=new Rect();//向上转型
        shape.draw();
        drawMap(shape);
        shape.funb();
        IShape.func();//静态用类来调用
    }

}
