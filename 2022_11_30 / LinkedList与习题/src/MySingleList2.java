


public class MySingleList2 {
    // 2、无头双向链表实现
    static class ListNode2 {
        public int val;//存储的数据
        public ListNode2 prev;//存储下一个节点的地址
        public ListNode2 next;//存储下一个节点的地址
        public ListNode2(int val) {
            this.val = val;
        }
    }
    public ListNode2 head;
    public ListNode2 last;
    //头插法
    public void addFirst(int data){
       ListNode2 cur=new ListNode2(data);
        if (head==null){
            head=cur;
            last=cur;
        }else {
            cur.next=head;
            head.prev=cur;
            head=cur;
        }

    }
    //尾插法
    public void addLast(int data){
        ListNode2 cur=new ListNode2(data);
        if (head==null){
            head=cur;
            last=cur;
        }else {
            last.next=cur;
            cur.prev=last;
            last=cur;
        }
    }
    //任意位置插入,第一个数据节点为0号下标
    public boolean addIndex(int index,int data){
        ListNode2 node2=new ListNode2(data);
        if (index<0||index>size()){
            throw new RuntimeException("空指针");
        }
        if (index==0){
            addFirst(data);
        }
        if (index==size()){
            addLast(data);
        }
        ListNode2 cur=head;
        while (index>0){
            cur=cur.next;
            index--;
        }
        node2.next=cur;
        cur.prev.next=node2;
        node2.prev=cur.prev;
        cur.prev=node2;
        return true;
    }
    //查找是否包含关键字key是否在单链表当中
    public boolean contains(int key){
        ListNode2 cur=head;
        while (cur!=null){
            if (cur.val==key){
                return true;
            }
            cur=cur.next;
        }
        return false;
    }
    //删除第一次出现关键字为key的节点
    public void remove(int key){
        ListNode2 cur=head;
        while (cur!=null){
            if (cur.val==key){
                if (cur==head){
                    head=head.next;
                    head.prev=null;
                }else {
                    cur.prev.next=cur.next;
                    if (cur.next!=null){
                        cur.next.prev=cur.prev;
                    }else {
                        last=last.prev;
                    }
                }
                return;
            }
            cur=cur.next;
        }
    }
    //删除所有值为key的节点
    public void removeAllKey(int key){
        ListNode2 cur=head;
        while (cur!=null){
            if (cur.val==key){
                if (cur==head){
                    head=head.next;
                    head.prev=null;
                }else {
                    cur.prev.next=cur.next;
                    if (cur.next!=null){
                        cur.next.prev=cur.prev;
                    }else {
                        last=last.prev;
                    }
                }
            }
            cur=cur.next;
        }

    }
    //得到单链表的长度
    public int size(){
       ListNode2 cur=head;
        int count=0;
        while (cur!=null){
            count++;
            cur=cur.next;
        }
        return count;
    }
    public void display(){
       ListNode2 cur=head;
        while (cur!=null){
            System.out.print(cur.val+" ");
            cur=cur.next;
        }
        System.out.println();
    }
    public void clear(){
       /*不能简单的置为空，因为前驱还在
       head = null;
        last=null;*/
        //循环遍历置为空
        ListNode2 cur=head;
        while (cur!=null){
            ListNode2 curnext=cur.next;
            cur.prev=cur.next=null;
            cur=curnext;
        }
//        将最后俩个引用置为空
        head=last=null;
    }
}
