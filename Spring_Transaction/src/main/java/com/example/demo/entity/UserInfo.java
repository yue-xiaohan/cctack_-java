package com.example.demo.entity;

import lombok.Data;

@Data
public class UserInfo {
//    注意我的数据库中是没有UserInfo这张表的,所以不能直接运行
    private Integer id;
    private String username;
    private String password;
    private String photo;
    private String createtime;
    private String updatetime;
    private Integer state;
}
