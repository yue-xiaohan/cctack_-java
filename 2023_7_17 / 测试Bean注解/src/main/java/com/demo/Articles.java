package com.demo;

import com.demo.model.ArticleInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;


@Component
public class Articles {

    @Bean("article") // 将当前方法返回的对象存储到 IoC 容器
    public ArticleInfo getArt(){
        // 伪代码
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setAid(1);
        articleInfo.setTitle("标题？");
        articleInfo.setContent("今天周一！！！");
        articleInfo.setCreatetime(LocalDateTime.now());
        return articleInfo;
    }

    public void sayHi(){
        System.out.println("Hi, Articles~");
    }

}
