package com.demo;

import com.demo.model.ArticleInfo;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.applet.AppletContext;

public class Test {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        Articles articles = context.getBean("articles",Articles.class);
        articles.sayHi();
        System.out.println(articles.getArt().toString());
//        ArticleInfo articleInfo = context.getBean("articleInfo",ArticleInfo.class);
//        System.out.println(articleInfo.toString());
    }
}
