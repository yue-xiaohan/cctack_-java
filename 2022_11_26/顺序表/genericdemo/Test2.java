package genericdemo;

/**
 * @Author 12629
 * @Description：
 */
class Alg<E extends Comparable<E>> {
    public E findMax(E[] array) {
        E max = array[0];
        for(int i = 1; i < array.length;i++) {
            if(max.compareTo(array[i]) < 0 ) {
                max = array[i];
            }
        }
        return max;
    }
}
class A<E extends Number> {

}

class Alg2 {

    public <E extends Comparable<E>> E findMax(E[] array) {
        E max = array[0];
        for(int i = 1; i < array.length;i++) {
            if(max.compareTo(array[i]) < 0 ) {
                max = array[i];
            }
        }
        return max;
    }
}

class Alg3 {
    public static <E extends Comparable<E>> E findMax(E[] array) {
        E max = array[0];
        for(int i = 1; i < array.length;i++) {
            if(max.compareTo(array[i]) < 0 ) {
                max = array[i];
            }
        }
        return max;
    }
}

public class Test2 {
    public static void main(String[] args) {
        Integer[] array = {1,4,2,10,9,8,17,5};
        Integer val = Alg3.findMax(array);
        System.out.println(val);
    }
    public static void main3(String[] args) {
        Alg2 alg2 = new Alg2();
        Integer[] array = {1,4,2,10,9,8,17,5};
        Integer val = alg2.<Integer>findMax(array);
        System.out.println(val);
    }




    public static void main2(String[] args) {
        A<Number> a1 = new A<>();
        A<Integer> a2 = new A<>();
        A<Double> a3 = new A<>();
        //A<String> a4 = new A<>();
    }
    public static void main1(String[] args) {
        Alg<Integer> alg = new Alg<>();
        Integer[] array = {1,4,2,10,9,8,17,5};
        Integer val = alg.findMax(array);
        System.out.println(val);
    }
}