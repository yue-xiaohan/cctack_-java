package genericdemo;

import java.lang.reflect.Array;

/**
 * @Author 12629
 * @Description：
 */
class MyArray<E> {
    //T t;
    //public T[] obj = new T[3];
    //public E[] obj = (E[] ) new Object[3];

    /*
    以后常用的 ！！！！ 官方 自己在用的手段
    public Object[] obj = new Object[3];

    public E getPos2(int pos) {
        return (E)obj[pos];
    }*/

    public E[] obj;
    public MyArray(Class<E> clazz, int capacity) {
        //反射
        obj = (E[]) Array.newInstance(clazz, capacity);
    }


    public E getPos(int pos) {
        return obj[pos];
    }
    public void setObj(int pos, E val) {
        obj[pos] = val;
    }
    public E[] getArray() {
        return obj;
    }
}
public class Test {
    public static void main(String[] args) {
        MyArray<Integer> myArray = new MyArray<>(Integer.class,10);
        myArray.setObj(0,10);
        myArray.setObj(1,78);
        Integer[] integers = (Integer[])myArray.getArray();

    }
    /*public static void main1(String[] args) {
        //实例化对象的同时 指定当前泛型类 的指定参数类型是Integer
        MyArray<Integer> myArray = new MyArray<>();
        //就可以存储 指定的数据类型
        myArray.setObj(0,10);
        myArray.setObj(1,78);
        myArray.setObj(2,66);

        Integer d = myArray.getPos(2);

        System.out.println("=================");

        MyArray<String> myArray2 = new MyArray<>();
        myArray2.setObj(0,"gaobo");
        myArray2.setObj(1,"bit");

        String str = myArray2.getPos(0);
    }*/
}