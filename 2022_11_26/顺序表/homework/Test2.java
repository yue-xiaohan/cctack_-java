package homework;

import java.util.Arrays;

public class Test2 {
    //给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
    //不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
    //元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

    //双指针
    public int removeElement2(int[] nums, int val) {//力扣官方
        int n = nums.length;
        int left = 0;
        for (int right = 0; right < n; right++) {
            if (nums[right] != val) {
                nums[left] = nums[right];
                left++;
            }
        }
        return left;
    }


    public int removeElement1(int[] nums, int val) {//老师的办法
        int count = 0;
        for(int i = 0; i < nums.length; ++i){
            if(nums[i] == val){
                count++;
            }else{
                nums[i - count] = nums[i];
            }
        }

        return nums.length - count;
    }


    public int removeElement(int[] nums, int val) {
        int size=nums.length;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == val){
                if (i == nums.length-1){
                    size--;
                    nums[i]=0;
                }
                else{
                    for (int j = i; j < nums.length-1; j++) {
                        nums[j]=nums[j+1];
                    }
                    nums[size-1]=val+1;
                    size--;
                    i--;
                }
            }
        }
        return  size;

    }

    public static void main(String[] args) {
        int [] array=new int[]{4,4,0,1,0,2};
        int val=0;
        Test2 test2=new Test2();
       int size= test2.removeElement(array,val);
        System.out.println("size="+size);
        for (int i = 0; i < size; i++) {
            System.out.print(array[i]+"  ");
        }
        //System.out.println(Arrays.toString(array));


    }

}
