package homework;

import java.util.Arrays;

public class Test {
    //数组nums包含从0到n的所有整数，但其中缺了一个。请编写代码找出那个缺失的整数。你有办法在O(n)时间内完成吗？
    public static  int missingNumber(int[] nums) {

        int i = 0;
        Arrays.sort(nums);
       if (nums.length==1){
           if (nums[0]==1){
               return 0;
           }else{
               return nums[0]+1;
           }
       }
       else{
           for (; i < nums.length; i++) {
               if(nums[i] != 0 && nums[i] != i){
                   return i;
               }
           }
       }
       return i+1;
    }



    public static void main(String[] args) {
        int [] array=new int[]{1};
        System.out.println(missingNumber(array));




    }
}
