package homework;

import java.util.Arrays;
import java.util.Scanner;

public class Test1 {
    //给你一个数组，将数组中的元素向右轮转 k 个位置，其中 k 是非负数。
    public static void rotate(int[] nums, int k) {
        int n = nums.length;
        int[] newArr = new int[n];
        for (int i = 0; i < n; ++i) {
            newArr[(i + k) % n] = nums[i];
        }

    }

    public static void main(String[] args) {
        int [] array=new int[]{1,2,3,4,5,6,7};
        Scanner sc=new Scanner(System.in);
        int k=sc.nextInt();
        rotate(array,k);
        System.out.println(Arrays.toString(array));

    }
}
