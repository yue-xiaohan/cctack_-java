package homework;

import java.util.Arrays;

public class Test5 {
    public void merge(int A[], int m, int B[], int n) {
        int b1=0;
        for(int i=m;i<m+n;i++){
            A[i]=B[b1];
            b1++;
        }
        Arrays.sort(A);

    }
    public static void main(String[] args) {
        int [] A={1,2,3,0,0,0};
        int [] B={2,5,6};
        Test5 t=new Test5();
        t.merge(A,3,B,3);
        System.out.println(Arrays.toString(A));

    }
}
