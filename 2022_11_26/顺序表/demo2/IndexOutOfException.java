package demo2;

/**
 * @Author 12629
 * @Description：
 */
public class IndexOutOfException extends RuntimeException{
    public IndexOutOfException() {

    }
    public IndexOutOfException(String msg) {
        super(msg);
    }
}