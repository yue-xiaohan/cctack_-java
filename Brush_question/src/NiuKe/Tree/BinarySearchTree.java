package NiuKe.Tree;

class Node{
    int val;
    Node left;
    Node right;
    public Node(int val){
        this.val=val;
    }
}


public class BinarySearchTree {
    public Node root=null;

    public void remove(int key){
        Node cur=root;
        Node parent=null;
        while (cur!=null){
            if (cur.val==key){
                removeNode(cur,parent);
            }else if (cur.val<key){
                parent=cur;
                cur=cur.right;
            }else{
                parent=cur;
                cur=cur.left;
            }
        }
    }
    public void removeNode(Node cur,Node parent){
        if (cur.left==null){
            if (cur==root){
                root=cur.right;
            }else if(cur==parent.left){
                parent.left=cur.right;
            }else {
                parent.right=cur.right;
            }
        }else if (cur.right ==null){
            if (cur==root){
                root=cur.left;
            }else if(cur==parent.left){
                parent.left=cur.left;
            }else {
                parent.right=cur.left;
            }
        }else{
            Node targetParent=cur;
            Node tatget=cur.right;
//            找左树的最大值
            while (tatget.left!=null){
                targetParent=tatget;
                tatget=tatget.left;
            }
            cur.val=tatget.val;
            if (tatget == targetParent.left){
                targetParent.left=tatget.left;
            }else {
                targetParent.right=tatget.right;
            }
        }
    }








    public boolean insert(int val){
        if (root==null){
            root=new Node(val);
            return true;
        }
        Node pre=null;
        Node cur=root;
        //确定pre最终在哪里
        while (cur!=null){
            if (val>cur.val){
                pre=cur;
                cur=cur.right;
            }else if (val==cur.val){
               return false;//不允许有相同节点
            }else{
                pre=cur;
                cur=cur.left;
            }
        }
//        进行插入
        Node node=new Node(val);
        if (pre.val<val){
            pre.right=node;
        }else {
            pre.left=node;
        }
        return true;
    }


    public Node search(int key){

      if (root==null)return null;

        while (root!=null){
            if (root.val==key){
                return root;
            }
            else if(key>root.val){
                root=root.right;
            }else{
                root=root.left;
            }
        }
        return null;
    }
}
