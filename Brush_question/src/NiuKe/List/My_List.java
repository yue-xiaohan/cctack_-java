package NiuKe.List;

import java.util.List;

public class My_List  {
    static class Node{
        int val;
        Node next;
        public Node() {

        }
        public Node(int val) {
            this.val = val;
        }
    }
    public Node head;//定义链表的头结点
    public void addFirst(int data){
        Node cur = new Node(data);
        cur.next = head;
        head = cur;

    }
    public void addLast(int data){
        Node cur = head;
        while (cur.next != null){
            cur = cur.next;
        }
        Node ret =new Node(data);
        cur.next = ret;
    }
    public boolean update(int k,int data){
        Node cur =head;
        while (k > 0){
            cur = cur.next;
            k--;
        }
        if (cur == null){
            return false;
        }
        cur.val = data;
        return true;
    }
    public boolean serach(Node node){
        while (head != null){
            if (head.val != node.val){
                head=head.next;
            }
            else return true;
        }
        return false;
    }
}
