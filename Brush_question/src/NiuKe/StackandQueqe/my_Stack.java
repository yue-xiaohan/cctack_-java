package NiuKe.StackandQueqe;

import java.util.Arrays;

public class my_Stack {
    // 栈用什么来实现吧 ,  数组
   int[] elem ;
   int size;
   public my_Stack(int size){
       elem = new int[size];
   }
    public my_Stack(){
       elem = new int[10];
    }

   public void initStack(int[] array){
       elem =Arrays.copyOf(array,elem.length);
       size = Math.min(array.length,elem.length);
   }
   //入栈
   public void push(int val){
       if (isFull()){
           elem = Arrays.copyOf(elem,elem.length*2);
       }
       elem[size++] = val;
   }
   //出栈
   public int  pop(){
       if (isEmpty()){
           throw new RuntimeException("栈为空,没有元素错误");
       }
       int k = --size;

       return elem[k];
   }
//   判断栈是否空
   public boolean isEmpty(){
       return size == 0;
   }
//   判断栈是否满
   public boolean isFull(){
       return size == elem.length;
   }


    public static void main(String[] args) {
        //测试代码
        int[] arr = {1,3,5,7,5};
        my_Stack stack =new my_Stack();
        for (int i = 0; i < arr.length; i++) {
            stack.initStack(arr);
        }
        int size =stack.size;
        for (int i = 0; i < size; i++) {
            System.out.println(stack.pop());
        }
    }
}
