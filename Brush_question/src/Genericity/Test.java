package Genericity;


class MyArray<T>{
    /**
     * 泛型：自动进行类型检查，不需要进行类型的强制转换
     */
    public T[]  objects=(T[])new Object[10];

    public T getObjects(int pos) {
        return  objects[pos];
    }

    public void setObjects(int pos,T val) {
        this.objects[pos] = val;
    }
}
public class Test {
    public static void main(String[] args) {

    }

}
