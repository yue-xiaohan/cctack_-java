package Lambda;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @Author 12629
 * @Description：
 */
@FunctionalInterface
interface NoParameterNoReturn {
    //注意：只能有一个抽象方法
    void test();
   /* static void test2() {
    }
    default void test3() {
    }*/
}
//无返回值一个参数
@FunctionalInterface
interface OneParameterNoReturn {
    void test(int a);
}
//无返回值多个参数
@FunctionalInterface
interface MoreParameterNoReturn {
    void test(int a,double b);
}
//有返回值无参数
@FunctionalInterface
interface NoParameterReturn {
    int test();
}
//有返回值一个参数
@FunctionalInterface
interface OneParameterReturn {
    int test(int a);
}
//有返回值多参数
@FunctionalInterface
interface MoreParameterReturn {
    int test(int a,int b);
}
class Demo<T> {

}
public class Test {
    public static void main8(String[] args) {
        //<>当中的数据类型 不参与类型的组成 JVM当中没有泛型的概念 泛型 只存在于编译阶段
        Demo<String> demo = new Demo<>();
        System.out.println(demo);
        Demo<Integer> demo2 = new Demo<>();
        System.out.println(demo2);

    }

    public static void main6(String[] args) {
        Map<String,Integer> hashMap = new HashMap<>();
        hashMap.put("hello",3);
        hashMap.put("abc",2);
        hashMap.put("this",5);

        /*hashMap.forEach(new BiConsumer<String, Integer>() {
            @Override
            public void accept(String s, Integer integer) {
                System.out.println("key: "+s +" val: "+ integer);
            }
        });*/
        hashMap.forEach((String s, Integer integer)-> System.out.println("key: "+s +" val: "+ integer));


    }

    public static void main5(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("hello");
        list.add("this");
        list.add("day");

        /*list.forEach(new Consumer<String>() {
            @Override
            public void accept(String s) {
                System.out.println(s);
            }
        });*/
        list.forEach(s -> System.out.println(s));
        System.out.println("=====");
        /*list.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });*/
        list.sort(((o1, o2) ->  {return o1.compareTo(o2);}));
        list.forEach(s -> System.out.println(s));
    }

    public static void main4(String[] args) {
        int a = 10;
        NoParameterNoReturn noParameterNoReturn = new NoParameterNoReturn() {
            @Override
            public void test() {
                //a = 99;
                System.out.println("hello->"+a);
            }
        };
        noParameterNoReturn.test();

        NoParameterNoReturn noParameterNoReturn2 = ()->{
            //a = 199;
            System.out.println("hello"+a);
        };
        noParameterNoReturn.test();
    }

    public static void main3(String[] args) {
        //可以省略return
        NoParameterReturn noParameterReturn = ()->10;
        int ret = noParameterReturn.test();
        System.out.println(ret);

        OneParameterReturn oneParameterReturn = x ->  x+10;
        ret = oneParameterReturn.test(9);
        System.out.println(ret);

        MoreParameterReturn moreParameterReturn = (x,y) -> x*y;
        ret = moreParameterReturn.test(10,20);
        System.out.println(ret);

    }


    public static void main2(String[] args) {
        NoParameterNoReturn noParameterNoReturn = ()->System.out.println("hello");
        noParameterNoReturn.test();
        //参数的类型 只有一个的时候 可以省略 ，还可以省略参数的括号
        OneParameterNoReturn oneParameterNoReturn = x-> System.out.println(x+10);
        oneParameterNoReturn.test(19);
        //类型不可以单独省略，除非一起省略！
        MoreParameterNoReturn moreParameterNoReturn = (x,y)-> System.out.println(x+y);
        moreParameterNoReturn.test(10,20);
    }


    public static void main1(String[] args) {
        //函数式接口
        NoParameterNoReturn noParameterNoReturn = new NoParameterNoReturn() {
            @Override
            public void test() {
                System.out.println("hello");
            }
        };
        noParameterNoReturn.test();
    }

}
