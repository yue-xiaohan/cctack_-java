package Lambda;

class Food {
}
class Fruit extends Food {
}
class Apple extends Fruit {
}
class Banana extends Fruit {
}
class Plate<T> {
    private T message;

    public T getMessage() {
        return message;
    }
    public void setMessage(T message) {
        this.message = message;
    }

}
public class Test3 {

    public static void main(String[] args) {
        Plate<Fruit> fruitPlate = new Plate<>();
        fruitPlate.setMessage(new Fruit());
        func2(fruitPlate);

        Plate<Food> foodPlate = new Plate<>();
        foodPlate.setMessage(new Food());
        func2(foodPlate);
    }
    public static void func2(Plate<? super Fruit > tmp) {
        tmp.setMessage(new Apple());
        tmp.setMessage(new Banana());
        //Fruit fruit = tmp.getMessage();
    }

    public static void main1(String[] args) {
        Plate<Apple> applePlate = new Plate<>();
        applePlate.setMessage(new Apple());
        func1(applePlate);

        Plate<Banana> bananaPlate = new Plate<>();
        bananaPlate.setMessage(new Banana());
        func1(bananaPlate);
    }

    public static void func1(Plate<? extends Fruit> tmp) {
        //这里仍然不可以进行修改！

        Fruit fruit = tmp.getMessage();
        System.out.println(tmp.getMessage());
    }

}