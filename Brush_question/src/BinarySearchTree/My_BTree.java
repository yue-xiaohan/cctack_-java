package BinarySearchTree;

import java.util.HashSet;
import java.util.Locale;

class BinarySearchTree2 {

    static class TreeNode {
        public int key;
        public TreeNode left;
        public TreeNode right;

        TreeNode(int key) {
            this.key = key;
        }
    }
     private  TreeNode prev=null;
     private void  InOrder(TreeNode pCur){
         if (pCur == null)return;
         InOrder(pCur.left);
         pCur.left=prev;
         if (prev!=null){
             prev.right=pCur;
         }
         prev=pCur;
         InOrder(pCur.right);
     }
     public TreeNode Convert(TreeNode pRootOfTree) {
         if (pRootOfTree == null)return null;
         InOrder(pRootOfTree);
         TreeNode head=pRootOfTree;
//        返回链表的头结点
         while (head.left!=null){
             head= head.left;
         }
         return head;

     }

    public TreeNode root;

    /**
     * 插入一个元素
     * @param key
     */
    public boolean insert(int key) {
        if (root == null){
            TreeNode node=new TreeNode(key);
            root=node;
            return true;
        }
        TreeNode cur=root;
        TreeNode pre=null;

        while (cur != null){
            if (cur.key > key){
                pre=cur;
                cur=cur.left;
            }else if (cur.key < key){
                pre=cur;
                cur=cur.right;
            }else {
                return false;
            }
        }
        TreeNode node=new TreeNode(key);
        if (key > pre.key){
            pre.right=node;
        }else {
            pre.left=node;
        }
        return true;
    }
    //查找key是否存在
    public TreeNode search(int key) {
        TreeNode cur=root;
        while (cur != null){
            if (cur.key > key){
                cur=cur.left;
            }else if (cur.key < key){
                cur=cur.right;
            }else {
                return cur;
            }
        }
        return null;
    }
    //删除key的值
    public boolean remove(int key) {
        TreeNode cur = root;
        TreeNode parent = null;
        while (cur != null) {
            if(cur.key == key) {
                //这里开始删除
                removeNode(cur,parent);
                break;
            }else if(cur.key < key) {
                parent = cur;
                cur = cur.right;
            }else {
                parent = cur;
                cur = cur.left;
            }
        }
       return true;
    }
     public void removeNode(TreeNode cur, TreeNode parent) {
        if (cur.left == null){
            if (cur == root){
                root = cur.right;
            }else if (cur == parent.left){
                parent.left = cur.right;
            }else {
                parent.right = cur.right;
            }
        }else if (cur.right == null){
            if (cur == root){
                root = cur.right;
            }else if (cur == parent.left){
                parent.left = cur.left;
            }else {
                parent.right=cur.left;
            }
        }else {
            TreeNode targetParent = cur;
            TreeNode target = cur.right;
            while (target.left != null) {
                targetParent = target;
                target = target.left;
            }
            cur.key = target.key;
            if(target == targetParent.left) {
                targetParent.left = target.right;
            }else {
                targetParent.right = target.right;
            }
        }
     }
     public static void func(String str1,String str2) {
         HashSet<Character> set=new HashSet<>();
         HashSet<Character> set2=new HashSet<>();

         for (int i = 0; i < str2.length(); i++) {
             set.add(str2.toUpperCase(Locale.ROOT).charAt(i));
         }
         for (int i = 0; i < str1.length(); i++) {
             char ch=str1.toUpperCase(Locale.ROOT).charAt(i);
             if (!set.contains(ch)&&!set2.contains(ch)){
                 set2.add(ch);

                 System.out.print(ch+" ");
             }
         }
     }


}
public class My_BTree {
}
