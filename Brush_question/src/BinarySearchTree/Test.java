package BinarySearchTree;
import java.util.*;


public class Test {
    //数据结构考试编程题
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param S string字符串
     * @param T string字符串
     * @return bool布尔型
     */
    public boolean isSubsequence2 (String S, String T) {
        char[] sArray= S.toCharArray();
        char[] tArray= T.toCharArray();
        int j=0;
        for (int i = 0; i < sArray.length; i++,j++) {
            while (j<tArray.length&& sArray[i]!=tArray[j]){
                j++;
            }
        }
        return j<=tArray.length;
    }


    public static void main(String[] args) {
         String  str=new String();
         Scanner sc = new Scanner(System.in);
       while (sc.hasNextLine()){
           str=sc.nextLine();
           String str2=func(str);
           if (str2 == null){
               str2="0";
           }
           System.out.println(str2);
       }
        /*str="abba";
        String str2=func(str);
        System.out.println(str2);*/
    }

    public static String func(String str){
        StringBuilder stringBuilder = new StringBuilder();
        Stack<Character> stack=new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            char ch=str.charAt(i);
            if (!stack.isEmpty()&&stack.peek() == ch){
                stack.pop();

            }else {
                stack.push(ch);
            }
        }
        int k=stack.size();
        if (k == 0){
            return null;
        }
        for (int i = 0; i < k; i++) {
            stringBuilder.append(stack.pop());
        }

        return stringBuilder.reverse().toString();
    }

}

