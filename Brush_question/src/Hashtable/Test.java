package Hashtable;
class hashtable{
    public int firstUniqChar(String s){
        //字符串只出现一次的字符
        if (s ==null)return -1;
        int[] array=new int[26];
        for (int i = 0; i < s.length(); i++) {
            char ch=s.charAt(i);
            array[ch-97]++;
        }
        for (int i = 0; i < s.length(); i++) {
            char ch=s.charAt(i);
            if (array[ch-97]==1)return i;
        }
        return -1;
    }
}

public class Test {

    public static void main(String[] args) {

    }
}
