package Hashtable;

import java.util.Arrays;

class HashBucket {
    private static class Node {
        private int key;
        private int value;
        Node next;

        public Node(int key, int value) {
            this.key = key;
            this.value = value;
        }
    }


    private Node[]  array;
    private int size;   // 当前的数据个数
    private static final double LOAD_FACTOR = 0.75;
    private static final int DEFAULT_SIZE = 8;//默认桶的大小

    public void put(int key, int value) {
        int index=key % array.length;
        Node cur=array[index];
        while (cur != null){
            if (cur.key == key){
                cur.value = value;
                return ;
            }
            cur= cur.next;
        }
        Node node=new Node(key,value);
        node.next=array[index];
        array[index]=node;

        size++;
        if (loadFactor() > LOAD_FACTOR){
            resize();
        }

    }

    private void resize() {
        // write code here
        Node[] newarray= new Node[array.length*2];
        for (Node cur:array) {
            while (cur != null){
                int index=cur.key % newarray.length;
                Node curNext=cur.next;
                cur.next=newarray[index];
                newarray[index]=cur;
                cur=curNext;
            }
        }

    }
    public int get(int key) {
        int index=key % array.length;
        Node cur=array[index];
        while (cur != null){
            if (cur.key == key){
                return cur.value;
            }
            cur= cur.next;
        }
        return -1;
    }
    public HashBucket() {
       array=new Node[DEFAULT_SIZE];
    }

    private double loadFactor() {
        return size * 1.0 / array.length;
    }




}
public class My_Work {
}
