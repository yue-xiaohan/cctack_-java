package Reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class ReflectClassDemo {

    public static void reflectNewInstance() {
        try {
            Class<?> c1 = Class.forName("Reflect.Student");
            Student student = (Student)c1.newInstance();
            System.out.println("学生对象："+student);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void reflectPrivateConstructor() {
        try {
            Class<?> c1 = Class.forName("Reflect.Student");
            Constructor<?> constructor =  c1.getDeclaredConstructor(String.class,int.class);

            constructor.setAccessible(true);

            Student student = (Student)constructor.newInstance("sss",19);

            System.out.println(student);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void reflectPrivateField() {
        try {
            Class<?> c1 = Class.forName("Reflect.Student");
            Field field = c1.getDeclaredField("name");

            field.setAccessible(true);

            Student student = (Student)c1.newInstance();
            field.set(student,"唐老鸭");

            System.out.println(student);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static void reflectPrivateMethod() {
        try {
            Class<?> c1 = Class.forName("Reflect.Student");
            Method method = c1.getDeclaredMethod("function",String.class);

            method.setAccessible(true);

            Student student = (Student)c1.newInstance();

            method.invoke(student,"我是一个参数！");
            //System.out.println(student);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }  catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }


    public static void main(String[] args) {
        reflectNewInstance();
        reflectPrivateConstructor();
        reflectPrivateField();
        reflectPrivateMethod();
    }
}