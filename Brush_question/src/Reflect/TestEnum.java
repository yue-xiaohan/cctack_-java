package Reflect;


public enum TestEnum {
    RED(1,"red"),
    BLACK(2,"black"),
    GREEN(3,"green");

    private String color;
    private int ordinal;
    //默认是私有的
    TestEnum(int ordinal,String color) {
        this.color = color;
        this.ordinal = ordinal;
    }







    public static void main(String[] args) {
        TestEnum[] testEnums = TestEnum.values();
        for (int i = 0; i < testEnums.length; i++) {
            System.out.println(testEnums[i]+" 索引："+testEnums[i].ordinal());
        }
        System.out.println("==========");
        TestEnum testEnum = TestEnum.valueOf("GREEN");
        System.out.println(testEnum);
        System.out.println(RED.compareTo(GREEN));
    }

    public static void main1(String[] args) {
        TestEnum color = RED;
        switch (color) {
            case GREEN:
                System.out.println("GREEN");
                break;
            case BLACK:
                System.out.println("BLACK");
                break;
            case RED:
                System.out.println("RED");
                break;
            default:
                System.out.println("color error");
                break;
        }
    }
}