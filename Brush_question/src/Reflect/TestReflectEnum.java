package Reflect;

import java.lang.reflect.Constructor;


public class TestReflectEnum {
    public static void reflectPrivateConstructor() {
        try {
            Class<?> classStudent = Class.forName("testenmu.TestEnum");

            Constructor<?>[] constructors = classStudent.getDeclaredConstructors();
            for (int i = 0; i < constructors.length; i++) {
                System.out.println("构造方法打印："+constructors[i]);
            }
            //注意传入对应的参数,获得对应的构造方法来构造对象,当前枚举类是提供了两个参数分别是String和int。
            Constructor<?> declaredConstructorStudent =
                    classStudent.getDeclaredConstructor(String.class,int.class,
                            int.class,String.class);

            //设置为true后可修改访问权限
            declaredConstructorStudent.setAccessible(true);

            Object objectStudent = declaredConstructorStudent.newInstance("父类参数",111,666,"绿色");
            TestEnum testEnum = (TestEnum) objectStudent;

            System.out.println("获得枚举的私有构造函数："+testEnum);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        reflectPrivateConstructor();
    }
}