package TestClass;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import ToolClass.Encode;
import ToolClass.ProtocolUtils;
import ToolClass.Request;
import ToolClass.Response;

/**
 * Server端代码
 */
public final class Server {
    public static void main(String[] args) throws IOException{
        ServerSocket server = new ServerSocket(4567);//创建绑定到指定端口的服务器套接字。
        while (true) {
            System.out.println("正在监听······");
            Socket client = server.accept();
            //读取请求数据
            InputStream input = client.getInputStream();
            Request request = ProtocolUtils.readRequest(input);
            System.out.println("收到的请求参数为: " + request.toString());
            OutputStream out = client.getOutputStream();
            //组装响应数据
            Response response = new Response();
            response.setEncode(Encode.UTF8);
            if(" ".equals(request.getCommand())){
                response.setResponse("你没有输入任何信息");
            }else{
                response.setResponse(response.getResponse());
            }
            response.setResponseLength(response.getResponse().length());
            ProtocolUtils.writeResponse(out, response);
        }
    }
}