package TestClass;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

import ToolClass.Encode;
import ToolClass.ProtocolUtils;
import ToolClass.Request;
import ToolClass.Response;


/**
 * 客户端代码

 */
public final class Client {
    public static void main(String[] args) throws IOException{
        Scanner scanner=new Scanner(System.in);
        String  str;
        //请求
        while(true) {
            Request request = new Request();
            System.out.println("请输入你要发送的信息");
            str=scanner.nextLine();
            request.setCommand(str);
            request.setCommandLength(request.getCommand().length());
            request.setEncode(Encode.UTF8);

            Socket client = new Socket("127.0.0.1", 4567);//127.0.0.1被称为本地环回地址,
            OutputStream out = client.getOutputStream();

            //发送请求
            ProtocolUtils.writeRequest(out, request);

            //读取响应数据
            InputStream in = client.getInputStream();
            Response response = ProtocolUtils.readResponse(in);
            System.out.println("获取的响应结果信息为: " + response.toString());
        }
    }
}
