package ToolClass;



/**
 * 协议响应的定义
 */
public class Response {

    private byte encode;//编码
    private String response;//响应内容
    private int responseLength;//响应长度

    public Response() {
        super();
    }

    public Response(byte encode, String response, int responseLength) {
        super();
        this.encode = encode;
        this.response = response;
        this.responseLength = responseLength;
    }

    public byte getEncode() {
        return encode;
    }

    public void setEncode(byte encode) {
        this.encode = encode;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public int getResponseLength() {
        return responseLength;
    }

    public void setResponseLength(int responseLength) {
        this.responseLength = responseLength;
    }

    @Override
    public String toString() {
        return "Response [encode=" + encode + ", response=" + response
                + ", responseLength=" + responseLength + "]";
    }

}
