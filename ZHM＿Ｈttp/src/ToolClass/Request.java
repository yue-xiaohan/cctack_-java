package ToolClass;


/**
 * 协议请求的定义
 */
public class Request {


    private byte encode;//协议编码
    private String command; //命令
    private int commandLength;//命令长度

    public Request() {
        super();
    }
    public Request(byte encode, String command, int commandLength) {
        super();
        this.encode = encode;
        this.command = command;
        this.commandLength = commandLength;
    }
    public byte getEncode() {
        return encode;
    }
    public void setEncode(byte encode) {
        this.encode = encode;
    }
    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public int getCommandLength() {
        return commandLength;
    }
    public void setCommandLength(int commandLength) {
        this.commandLength = commandLength;
    }

    @Override
    public String toString() {
        return "Request [encode=" + encode + ", command=" + command
                + ", commandLength=" + commandLength + "]";
    }

}
