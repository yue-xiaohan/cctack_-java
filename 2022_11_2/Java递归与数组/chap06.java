import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class chap06 {
    //递归与数组实例。。。。


    //*******************************
    public static void main11(String[] args) {
        //创建一个 int 类型的数组, 元素个数为 100, 并把每个元素依次设置为 1 - 100
        int []arr=new int[100];
        for (int i=1;i <= 100;i++){
            arr[i-1]=i;
        }
        printArray(arr);
    }
    //***********************************************
    public static void     printArray(int[] arr){

        for(int i:arr){
            System.out.printf("%d ",i);
            if (i%10==0)
                System.out.println();
        }
        /*String arr1=Arrays.toString(arr);
        System.out.println(arr1);

         */

    }
    public static void main10(String[] args) {
        //实现一个方法 printArray, 以数组为参数, 循环访问数组中的每个元素, 打印每个元素的值.
        int [] arr={1,2,3,4,5,6,7,8,9};
        printArray(arr);

    }
    //**********************************************
    public static int    fun6(int n){
        if(n<=2) {
            return 1;
        }else{
            return fun6(n-1)+fun6(n-2);
        }
    }
    public static void main9(String[] args) {
        //递归求斐波那契数列的第 N 项
        Scanner sc=new Scanner(System.in);
        int n=0;
        while(sc.hasNextInt()){
            n= sc.nextInt();
            System.out.println(fun6(n));
        }

    }
    //************************************
    public static int    fun5(int n){
        if(n<10)
        {
            return n;
        }
        else
        {
            return  fun5(n/10)+n%10;
        }
    }
    public static void main8(String[] args) {
        //写一个递归方法，输入一个非负整数，返回组成它的数字之和
        System.out.println(fun5(12345));
    }
    //***********************************************
    public static void   fun4(int n){
        if(n < 10){
            System.out.println(n);
            return;
        }else {
            fun4(n/10);
            System.out.println(n%10);
        }
    }
    public static void main7(String[] args) {
        //递归打印数字的每一位
        fun4(1234);
    }
    //***************************************************

    public static int  fun3(int n){
        if (n==1)
        {
            return 1;
        }
        else
        {
            return n*fun3(n-1);
        }
    }
    public static void main6(String[] args) {
        //递归求 N 的阶乘
        Scanner scanner=new Scanner(System.in);
        int n=0;
        while (scanner.hasNextInt()){
            n=scanner.nextInt();
            System.out.println(fun3(n));
        }

    }

    //***************分割线*****************
    public static void main5(String[] args) {
        int [] array={1,2,3,4,5,6};//最常见的数组初始化、
       // boolean [] array1=new boolean[10];
       // char [] array2=new char[10];
        System.out.println("====");
        for(int i:array){//第一种打印方法
            System.out.println(i);
        }
        String ret= Arrays.toString(array);//第二种打印方法
        System.out.println(ret);



    }




    //*******************************************************
    public static  void   hanota(int n,char pos1,char pos2,char pos3){
       //pos1 起始位置 pos2中转位置 pos3 最终位置
        if ( n == 1){
            move(pos1,pos3);
            return;
        }
        hanota(n-1,pos1,pos3,pos2);
        move(pos1,pos3);
        hanota(n-1,pos2,pos1,pos3);


    }
    public static  void move(char pos1,char pos2){
        System.out.print(pos1+"->"+pos2 +"  " );
        return;

    }
    public  static  int hanota(int n) {
        //自己写的 规律 2*前一位+1
        //n代表盘子
        int count=0;
        for(int i=1;i <= n;i++){
            count=2*count+1;
        }
        return count;//返回移动的次数

    }
    public static void main(String[] args) {
        int n=3;
        //System.out.println(hanota(n));
        hanota(n,'A','B','C');

    }





    //***************************************************

    public static  int  fun2(int n){

        if(n<10) {
            return n;
        }else{
            return fun2(n/10)+n%10;
        }
    }
    public static void main3(String[] args) {
        //求12345每位相加的和
        int n=12345;
        System.out.println(fun2(n));
    }



    //*********************************************************
    public static  int  fun1(int n){
        if(n==1)
        {
            return 1;
        }
        else
        {
            return n+fun1(n-1);
        }
    }
    public static void main2(String[] args) {
        //求1+2+3+......+10
        int n=10;
        System.out.println(fun1(n));

    }


    //********************************************************
    public static  void  fun(int n){
        if(n <= 9){
            System.out.println(n);
            return;
        }else{
            fun(n/10);
            System.out.println(n%10);
        }
    }
    public static void main1(String[] args) {
        int n=1234;//打印1234递归
        fun(n);

    }
}
