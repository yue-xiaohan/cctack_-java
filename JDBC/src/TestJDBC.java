import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class TestJDBC {
    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);

        // 1. 创建好数据源
        DataSource dataSource = new MysqlDataSource();
        // 设置数据库所在的地址
        ((MysqlDataSource) dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java102?characterEncoding=utf8&useSSL=false");
        // 设置登录数据库的用户名
        ((MysqlDataSource) dataSource).setUser("root");
        // 这个是设置登录数据库的密码
        ((MysqlDataSource) dataSource).setPassword("2222");

        // 2. 让代码和数据库服务器建立连接~~ 相当于到达了菜鸟驿站
        Connection connection = dataSource.getConnection();

        // 2.5 让用户通过控制台输入一下待插入的数据.
        System.out.println("请输入学号: ");
        int id = scanner.nextInt();
        System.out.println("请输入姓名: ");
        String name = scanner.next();

        // 3. 操作数据库. 以插入数据为例.
        //    关键所在就是构造一个 SQL 语句~
        //    在 JDBC 中构造的 SQL, 不必带上 ;
        //    ; 只是在命令行中用来区分不同的语句. 现在是直接在代码中操作~~
        String sql = "insert into student values(?, ?)";
        // 此处光是一个 String 类型的 sql 还不行, 需要把这个 String 包装成一个 "语句对象"
        PreparedStatement statement = connection.prepareStatement(sql);
        // 进行替换操作.
        statement.setInt(1, id);
        statement.setString(2, name);
        System.out.println("statement: " + statement);

        // 4. 执行 SQL , 相当于扫码取件
        //    SQL 里面如果是 insert, update, delete, 都使用 executeUpdate 方法.
        //    SQL 里面如果是 select, 则使用 executeQuery 方法.
        //    返回值就表示这个操作, 影响到了 几行. 就相当于在控制台里输入 sql 之后, 得到的数字~
        int ret = statement.executeUpdate();
        System.out.println(ret);

        // 5. 此时 SQL 已经执行完毕. 然后还需要释放资源.
        statement.close();
        connection.close();
    }
}