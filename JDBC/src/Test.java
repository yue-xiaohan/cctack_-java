import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.Executor;

public class Test {
    public static void main(String[] args) throws SQLException {
        //1:创建数据源
        DataSource dataSource=new MysqlDataSource();
        ((MysqlDataSource)dataSource).setURL("jdbc:mysql://127.0.0.1:3306/java102?characterEncoding=utf8&useSSL=false");
        ((MysqlDataSource)dataSource).setUser("root");
        ((MysqlDataSource)dataSource).setPassword("111111");
        //2:链接数据库
        Connection connection = dataSource.getConnection();

       //3:输入信息
        Scanner scanner=new Scanner(System.in);
        //通过相同类型的值来赋值
        System.out.println("请输入学号: ");
        int id = scanner.nextInt();
        System.out.println("请输入姓名: ");
        String name = scanner.next();
        //4:拼接sql
        String sql = "insert into student values(?, ?)";
        // 此处光是一个 String 类型的 sql 还不行, 需要把这个 String 包装成一个 "语句对象"
        PreparedStatement statement = connection.prepareStatement(sql);
        // 进行替换操作.
        statement.setInt(1, id);
        statement.setString(2, name);
        System.out.println("statement: " + statement);
        //5.执行sql语句:增删改都是用executeUpdate,查找是用的executeQuery
        int ret = statement.executeUpdate();
        //6:释放资源
        statement.close();
        connection.close();

    }
}
