package model;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import util.OrderSystemException;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDao {
    /*实现三个功能
    * 1: 插入用户 - 注册的 时候使用
    * 2: 按名字查找用户 - 登录时使用
    * 3 : 按照用户Id查找 - 展示信息时使用
    */
    public void add(User user) throws SQLException, OrderSystemException {
        // 接下来JDBC的基本流程
        /*1:先获取与数据库的链接 DataSouce
        * 2:链接数据库
        * 3:拼装SQL语句 PrepareStatement
        * 4:执行SQL语句 (executeQuery,executeUpdate)
        * 5:关闭链接close(从后往前关)*/

        Connection connection = DBUtil.getConnection();
        String sql = "insert into user values(null,?,?,?)";
        PreparedStatement statement = null;

        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1, user.getName());
            statement.setString(2, user.getPasword());
            statement.setInt(3, user.getIsAdmin());
            //拼接SQL
            int ret = statement.executeUpdate();
            if (ret != 1){
                throw new OrderSystemException("插入异常失败");
            }
            System.out.println("插入用户成功");
        }catch (SQLException e){
            e.printStackTrace();
            throw new OrderSystemException("插入用户失败2");
        }finally {
            DBUtil.close(connection, statement,null);
        }
    }



    public User selectByName(String name) throws OrderSystemException, SQLException {
        Connection connection = DBUtil.getConnection();
        String sql = "select * from user where name = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
          statement = connection.prepareStatement(sql);
          statement.setString(1,name);
          resultSet = statement.executeQuery();
         //遍历结果集,按照名字查找,只能查到一个,要求名字不能重复
            if (resultSet.next()){
                User user = new User();
                user.setUserId(resultSet.getInt("userId"));
                user.setName(resultSet.getString("name"));
                user.setPasword(resultSet.getString("password"));
                user.setIsAdmin(resultSet.getInt("isAdmin"));
                return user;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new OrderSystemException("按姓名查找失败");
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    public User selectById(int userId) throws OrderSystemException, SQLException {
        Connection connection =DBUtil.getConnection();
        String sql = "select * from user where userId = ?";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
           statement = connection.prepareStatement(sql);
           statement .setInt(1,userId);
           resultSet = statement.executeQuery();
           if (resultSet.next()){
               User user = new User();
               user.setUserId(resultSet.getInt("userId"));
               user.setName(resultSet.getString("name"));
               user.setPasword(resultSet.getString("password"));
               user.setIsAdmin(resultSet.getInt("isAdmin"));
               return user;
           }
        } catch (Exception e) {
            e.printStackTrace();
            throw new OrderSystemException("按ID查找用户失败");
        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }
    public static void main(String[] args) throws SQLException, OrderSystemException {
        UserDao userDao = new UserDao();
        User user = new User();
        user.setName("汤老湿");
        user.setPasword("123456");
        user.setIsAdmin(0);
        userDao.add(user);
       //  2. 验证按照 名字 查找数据
        User user2 = userDao.selectByName("汤老湿");
        System.out.println("按照名字查找");
        System.out.println(user2);
        user2 = userDao.selectById(2);
        System.out.println("按照 id 查找");
        System.out.println(user2);
    }

}
