package model;

import java.sql.Timestamp; // 对应datatime包
import java.util.List;
//一个类包含了俩张表
public class Order {
//    Order 表示一个完整的订单,包括订单中有哪些菜
    private int orderId;
    private int userId;
    private Timestamp time;//用到了Java.sql这个包 - 就是一个时间戳
    private int isDone; // 是否完结
    private List<Dish> dishes ;//一个订单中包含那些菜

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public int getIsDone() {
        return isDone;
    }

    public void setIsDone(int isDone) {
        this.isDone = isDone;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }
}
