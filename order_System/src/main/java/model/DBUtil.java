package model;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.sql.DataSource;
import java.sql.Connection; // 必须是JavaSQL版本的
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBUtil {
//    用单例模式来实现

    public static final String URL = "jdbc:mysql://127.0.0.1:3306/order_system?characterEncoding=utf-8&useSSL=true";
    public static final String USERNAME = "root";
    public static final String PASSWORD = " "; // 云服务器上是没有密码的

    private static volatile DataSource dataSource = null;

    public static DataSource getDataSource(){
        // 线程安全写法
        if (dataSource == null){
            synchronized (DBUtil.class){
                if (dataSource == null){
                    dataSource = new MysqlDataSource();
                    ((MysqlDataSource)dataSource).setURL(URL);
                    ((MysqlDataSource)dataSource).setUser(USERNAME);
                    ((MysqlDataSource)dataSource).setURL(PASSWORD);
                }
            }
        }
        return dataSource;
    }
    /*链接失败是常见的问题,如果发现connect为null 说明数据库链接失败
    * 就需要查看错误信息,看Tomcat日志
    * 常见的问题就是,URL USERname password等信息写错了,或者数据库没有启动 */
    public static Connection getConnection()  {
        try {
            return getDataSource().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("数据库链接失败 , 请检查数据库是否启动正确,URL是否正确");
        return null;
    }
    public static void close(Connection connection , PreparedStatement statement , ResultSet resultSet) throws SQLException {
        // 链接, 语句,结果集

        try {
            if (resultSet != null){
                resultSet.close();
            }
            if (statement != null){
                statement.close();
            }
            if (connection != null){
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }


}
