package model;

import util.OrderSystemException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class DishDao {
    /*1 :新增菜品
    * 2 : 删除菜品
    * 3 :查询所有菜品
    * 4 :查询指定菜品
    * 5 :修改菜品信息 - 改价格*/
    public void add(Dish dish) throws SQLException, OrderSystemException {
        Connection connection = DBUtil.getConnection();
        String sql = "insert into dishes values(null,?,?,?)";
        PreparedStatement statement = null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setString(1,dish.getName());
            statement.setInt(2,dish.getPrice());

            int ret = statement.executeUpdate();
            if (ret != 1){
                throw new OrderSystemException("插入菜品失败");
            }
            System.out.println("插入菜品成功");
        } catch (SQLException | OrderSystemException e) {
            e.printStackTrace();
            throw new OrderSystemException("插入菜品失败");
        } finally {
            DBUtil.close(connection,statement,null);
        }
    }


    public void delete(int dishId) throws SQLException, OrderSystemException {
        Connection connection = DBUtil.getConnection();
        String sql = "delete from dishes where dishId = ? ";
        PreparedStatement statement =null;
        try {
            statement = connection.prepareStatement(sql);
            statement.setInt(1,dishId);
            int ret = statement.executeUpdate();
            if (ret != 1){
                throw new OrderSystemException("删除菜品失败");
            }
            System.out.println("删除成功");
        } catch (OrderSystemException e) {
            e.printStackTrace();
            throw new OrderSystemException("删除菜品失败");

        } finally {
            DBUtil.close(connection,statement,null);
        }
    }

    public List<Dish> selectALL() throws SQLException, OrderSystemException {
        List<Dish> results = new LinkedList<>();
        Connection connection = DBUtil.getConnection();
        String sql = "select * from dishes";
        PreparedStatement statement = null;
        ResultSet resultSet =null;
        try {
            statement = connection.prepareStatement(sql);
            resultSet = statement.executeQuery();
            //这次是希望查找到多条记录
            while (resultSet.next()){
                Dish dish = new Dish();
                dish.setDishId(resultSet.getInt("dishId"));
                dish.setName(resultSet.getString("name"));
                dish.setPrice(resultSet.getInt("price"));
                results.add(dish);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new OrderSystemException("查找出错");

        } finally {
            DBUtil.close(connection,statement,resultSet);
        }
        return results;
    }
    public Dish selectById(int dishId) throws SQLException, OrderSystemException {
        Connection connection =DBUtil.getConnection();
        String sql = "select * from dishes where dishId = ? ";
        PreparedStatement statement = null;
        ResultSet resultSet =null;
        try {
            statement =connection.prepareStatement(sql);
            statement.setInt(1,dishId);
            resultSet = statement.executeQuery();
            if (resultSet.next()){
                Dish dish = new Dish();
                dish.setDishId(resultSet.getInt("dishId"));
                dish.setName(resultSet.getString("name"));
                dish.setPrice(resultSet.getInt("price"));
                return dish;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new OrderSystemException("按照ID查找出错");
        } finally {
        DBUtil.close(connection,statement,resultSet);
        }
        return null;
    }

    public static void main(String[] args) throws SQLException, OrderSystemException {
        //测试新增
        DishDao dishDao = new DishDao();

//        Dish dish = new Dish();
//        dish.setName("红烧肉");
//        dish.setPrice(3000);//价格是分
//         dishDao.add(dish);
            //测试查找
//        List<Dish> list =dishDao.selectALL();
//        System.out.println("查看所有");
//        System.out.println(list);
//        Dish dish = dishDao.selectById(1);
//        System.out.println("查看单个");
//        System.out.println(dish);
            //测试删除
//        dishDao.delete(3);

    }
}
