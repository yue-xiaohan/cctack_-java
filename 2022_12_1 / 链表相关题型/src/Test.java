import java.util.Scanner;

public class Test {
    //输入一串字符串，将重复的去掉.以后做题一定要处理循环输入。
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        while (scan.hasNextLine()){
            String str=scan.nextLine();
            String ret=func(str);
            System.out.println(ret);
        }
    }
    private  static  String func(String str){
        boolean [] flg=new boolean[127];
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char ch=str.charAt(i);

            if(flg[ch]==false){
               sb.append(ch);
               flg[ch]=true;
            }
        }
        return sb.toString();
    }
}
