public class Test2 {
    //给出一个有序的整数数组A ，和有序的整数数组B，请将数组B合并到数组A中，
    // 变成一个有序的升序数组

    /**
     * 要求：不能使用Arrays。sort方法
     * 不能申请额外的存储空间
     */
    //第一种解法，双指针（从后往前走）。每次i下标和j下标比较，然后结果给你K下标
    public void merge(int A[], int m, int B [],int n){
        int i=m-1;
        int j=n-1;
        int k=m+n-1;
        //俩个数组当中  都有数据
        while(i>=0 && j>=0){
            if (A[i]<= B[j]){
                A[k]=B[j];
                k--;
                j--;
            }else{
                A[k]=A[i];
                k--;
                i--;
            }
        }
        while( j >= 0){
            A[k]=B[j];
            k--;
            j--;
        }
    }
    public static void main(String[] args) {

    }
}
