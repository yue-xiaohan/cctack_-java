package demo;

import java.util.LinkedList;

/**
 * @Author 12629
 * @Description：
 */
public class Test {
    public static void main(String[] args) {
        Integer a = 200;
        Integer b = 200;
        System.out.println(a == b);
    }

    public static void main2(String[] args) {
        Integer val1 = 10;
        int a = val1;//拆箱-》 引用类型 拆箱为了 基本数据类型
        System.out.println(a);

        int b = val1.intValue();//显示拆箱
        System.out.println(b);

        double d = val1.doubleValue();

    }

    public static void main1(String[] args) {
        int a = 10;
        //Integer val = a;//自动
        Integer val = Integer.valueOf(a);//显示装箱
        Integer val2 = new Integer(a);//显示装箱

        System.out.println(val);
    }
}
