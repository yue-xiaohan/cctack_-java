
import java.util.Scanner;

/**
 * Error//错误：逻辑上的问题
 * 异常就是类
 * Exception//异常：1：编译时异常：必须处理
 *                 2：运行时异常：
 *                 3：可以通过逗号声明多个异常
 *                 4:异常是谁先抛出异常就捕获那个
 *                 5：不管怎样抛出异常，final最终都会被执行
 */
class Person implements Cloneable{

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
public class Test {




    public static int test3(int a) {
        int[] array = null;
        return array[0];
    }

    public static void main8(String[] args) {
        test3(1);
    }

    public static int test2() {
        try {
            int a = 10;
            return a;
        }catch (NullPointerException e) {
            return 1;
        }finally {
            return 9;
        }
    }

    public static void main7(String[] args) {
        System.out.println(test2());
    }

    public static void main6(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            //存放可能抛出异常的代码
            //test1(null);
            /*int[] array = null;
            System.out.println(array.length);
            System.out.println(10/0);*/
            int a = scanner.nextInt();
            int[] array = {1, 2, 3};
            System.out.println(array[1]);
            System.out.println("after");
        } catch (ArithmeticException e) {
            //test2();//发送邮件的 服务
            e.printStackTrace();
            System.out.println("捕捉到了 ArithmeticException 异常，进行处理异常的逻辑");
        } catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("捕获到了 NullPointerException 异常");
        } finally {
            //JDBC  con.close()
            System.out.println("finally一般用于资源的释放.......");
        }
        System.out.println("正常的逻辑........");
    }


    public static void main4(String[] args) {
        try {
            int[] array = null;
            System.out.println(array.length);
            System.out.println("after");
        }catch (ArithmeticException | NullPointerException e) {
            e.printStackTrace();
            System.out.println("捕捉到了 NullPointerException | ArithmeticException 异常，" +
                    "进行处理异常的逻辑");
        }finally {
            System.out.println("finally.......");
        }
        System.out.println("正常的逻辑........");
    }


    public static void test1(int[] a) throws CloneNotSupportedException
    {
        if(a == null) {
            throw new CloneNotSupportedException();//被中断了
        }
    }

    public static void main3(String[] args) {
        try {
            //存放可能抛出异常的代码
            //test1(null);
            int[] array = null;
            System.out.println(array.length);
            System.out.println("after");
        }catch (ArithmeticException e) {
            e.printStackTrace();
            System.out.println("捕捉到了 ArithmeticException 异常，进行处理异常的逻辑");
        }catch (NullPointerException e) {
            e.printStackTrace();
            System.out.println("捕获到了 NullPointerException 异常");
        }
        System.out.println("正常的逻辑........");
    }

    public static void func() {
        func();
    }

    public static void main2(String[] args) {
        func();
    }

    //https://stackoverflow.com/
    public static void main1(String[] args) throws CloneNotSupportedException {
        //System.out.println(10/0);
        /*int[] array = null;
        System.out.println(array.length);
        System.out.println("fdsfaafa");*///运行时异常/非受查异常
        /**/int[] array = {1,2,3};
        System.out.println(array[10]);

        Person person = new Person();
        Person person1 = (Person)person.clone();//编译时异常/受查异常
    }
}



