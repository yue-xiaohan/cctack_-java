-- 一般对于建表的SQL . 都会单独保存
-- 后序程序可能需要在不同的主机上部署 , 部署的时候需要在对应的主机上数据库也创建号
-- 把建表的SQL 保存好 ,方便在不同机器上建库建表

create database if exists java107_blog_system;
use java107_blog_system;
drop table if exists blog;
create table blog(
    blog_id int primary  key auto_increment,
    title varchar(128),
    content varchar(4096),
    userId  int ,
    postTime datetime
);
drop table if exists user;
create table user(
    userId int primary key auto_increment,
    username varchar(50),
    password varchar(50)
);
