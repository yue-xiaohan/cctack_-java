public class DataUtil {
    public int year;
    public int month;
    public int day;
    //构造方法初始化
    public DataUtil(){
        //构造方法名字必须类名相同，没有返回值类型void也不行。构造方法有多个，重载。。
        //构造方法只会被调用一次
        //重载：方法名必须相同，参数列表不同，返回值没有影响。
        //this不能形成环
        this(1800,1,1);
        System.out.println("不带参数");
    }
    public DataUtil(int year,int month,int day){
        this.year=year;
        this.month=month;
        this.day=day;

    }
    public void setDate(int year,int month,int day){
        this.year=year;
        this.month=month;
        this.day=day;

    }
    public void show(){
        System.out.println(year+"年"+month+"月"+day+"日");
    }
}

