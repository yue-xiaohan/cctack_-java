//import ;导入包
//import static//导入静态包和静态方法都可以直接用


//
public class chap09 {

    private int count;

    public static void main(String[] args) {

        chap09 test=new chap09(88);

        System.out.println(test.count);

    }

    chap09(int a) {

        count=a;

    }

    public static void main6(String[] args){

        String s=null;

        System.out.println("s="+s);

    }

    public static void main5(String[] args) {
        DataUtil dataUtil=new DataUtil();
        dataUtil.show();
    }
    public static void main4(String[] args) {
        //this引用的学习。
        //每个成员方法第一个参数默认为this：隐式参数
        DataUtil data=new DataUtil();
        data.setDate(2022,11,7);
        data.show();
        DataUtil data1=new DataUtil();

    }
    public static void main3(String[] args) {
        //一个引用不能同时指向多个对象,最终也只是指向一个对象。改变的是对象的值
        Person person1=new Person();
        person1=new  Person();
        person1=new  Person();
        person1=new  Person();
        person1=new  Person();
    }
    public static void main2(String[] args) {
        //引用指向引用的例子
        //当一个引用赋值为null时，代表：这个引用不指向任何对象；
       // Person person1=null;
        //引用不能指向引用，改变的是指向对象。

        Person person1=new Person();
        person1.Person("asda",18);
        Person person2=new Person();
        person2.Person("zhangsan",28);
        person1=person2; //引用不能指向引用，改变的是指向对象。
        person1.show();
        person2.show();



    }
    public static void main1(String[] args) {
        //类和对象
        String str="张俊铭";
        Person person=new Person();
        person.name = str;
        person.age=22;
        person.eat();
        person.show();
        Person person1=new Person();
        person1.Person("asda",18);
        person1.eat();
        person1.show();



    }
}
 class Person{
    public String name;
    public  int age;
    public  void Person(String name,int age){

        this.name=name;
        this.age=age;
    }

    public void eat(){
        System.out.println(name+"正在吃饭");

     }
     public void show(){
         System.out.println("姓名"+name+"年龄"+age);
     }


}


