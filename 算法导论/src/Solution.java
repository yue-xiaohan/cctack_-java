public class Solution {

    /*用来记录算法导论当中得知识*/

    /*
    * 1：插入排序
    * 思想：假定第一个数据是有序的，后面数据寻找合适的位置插入
    * 使用场景：需排序的数据，少量数据。
    * 时间复杂度：O（N^2）
    * */
    public static void insertSort(int[] array){

        for (int i = 1; i < array.length; i++) {
            int tmp=array[i];
            int j = i-1;
            for (; j >=0 ; j--) {
                if (tmp > array[j]){ //升序
                    array[j+1]=array[j];
                }else {
                    break;
                }
            }
            array[j+1]=tmp;
        }
    }
    /*
     * 2：选择排序
     * 思想：每次从数组中选择最大或最小的元素，放到合适的位置
     * 使用场景：需排序的数据，少量数据。
     * 时间复杂度：O（N^2）
     * */
    public static void selectSort(int[] array){

        for (int i = 0; i < array.length; i++) {
            int tmp=i;//记录当前下标
            for (int j = i+1; j < array.length; j++) {
                if (array[tmp] > array[j]){
                   tmp=j;//找到最小元素的下标
                }
            }
           swap(array,i,tmp);//交换
        }

    }
    public static void swap(int[] array,int left,int right){
        int tmp=array[left];
        array[left]=array[right];
        array[right]=tmp;
    }
    /**归并排序
     * 时间复杂度：N*logN
     * 空间复杂度：N
     * 稳定性：稳定的排序
     *       插入排序    冒泡排序  归并排序
     * @param array
     */
    public static void mergeSort1(int[] array) {
        mergeSortFunc(array,0,array.length-1);
    }

    private static void mergeSortFunc(int[] array,int left,int right) {
        if(left >= right) {
            return;
        }
        int mid = (left+right) / 2;
        mergeSortFunc(array,left,mid);
        mergeSortFunc(array,mid+1,right);
        merge(array,left,right,mid);
    }

    private static void merge(int[] array,int start,int end,int mid) {
//        原理其实就是合并俩个有序数组
        int s1 = start;
        //int e1 = mid;
        int s2 = mid+1;
        //int e2 = end;
        int[] tmp = new int[end-start+1];
        int k = 0;//tmp数组的下标
        while (s1 <= mid && s2 <= end) {
            if(array[s1] <= array[s2]) {
                tmp[k++] = array[s1++];
            }else {
                tmp[k++] = array[s2++];
            }
        }
        while (s1 <= mid) {
            tmp[k++] = array[s1++];
        }
        while (s2 <= end) {
            tmp[k++] = array[s2++];
        }

        for (int i = 0; i < tmp.length; i++) {
            array[i+start] = tmp[i];
        }

    }
    //    归并排序得非递归方式
    public static void mergeSort(int[] array) {
        int gap = 1;
        while (gap < array.length) {
            // i += gap * 2 当前gap组的时候，去排序下一组
            for (int i = 0; i < array.length; i += gap * 2) {
                int left = i;
                int mid = left+gap-1;//有可能会越界
                if(mid >= array.length) {
                    mid = array.length-1;
                }
                int right = mid+gap;//有可能会越界
                if(right>= array.length) {
                    right = array.length-1;
                }
                merge(array,left,right,mid);
            }
            //当前为2组有序  下次变成4组有序
            gap *= 2;
        }
    }

}

}
