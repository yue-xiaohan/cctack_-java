package com.example.demo.config;

import lombok.Data;
// 公共文件

/*
* 统一数据的格式返回
* */
@Data // 避免写set 与get方法
public class AjaxResult {
    //状态码
    private Integer code;
    //状态码的描述信息
    private String msg;
    //返回的数据
    private Object data;
    /*
    * 操作成功返回的结果
    * */
    public static AjaxResult successs(Object data){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(200);
        ajaxResult.setMsg("");
        ajaxResult.setData(data);
        return ajaxResult;
    }
    public static AjaxResult successs(int code,Object data){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg("");
        ajaxResult.setData(data);
        return ajaxResult;
    }
    public static AjaxResult successs(int code , String msg,Object data){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(data);
        return ajaxResult;
    }
    /*
    * 返回失败的格式*/
    public static AjaxResult fail(int code,String msg){
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.setCode(code);
        ajaxResult.setMsg(msg);
        ajaxResult.setData(null);
        return ajaxResult;
    }

}
