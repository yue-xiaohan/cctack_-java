package com.example.demo.controller;

import com.example.demo.common.UserSessionUtils;
import com.example.demo.config.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVo;
import com.example.demo.mapper.ArticleMapper;
import com.example.demo.service.ArticleServer;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleServer articleServer;
    @RequestMapping("/reg")
    public AjaxResult reg(Userinfo userinfo){
        // 非空校验 与 数据性有效性校验 -永远不要相信前端
        if (userinfo ==null || !StringUtils.hasLength(userinfo.getUsername())||
        !StringUtils.hasLength(userinfo.getPassword())){
            return AjaxResult.fail(-1,"非法参数");
        }
        return AjaxResult.successs(userService.reg(userinfo));
    }
    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest request, String username , String  password){
        //非空校验
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password)) {
            return AjaxResult.fail(-1, "非法请求");
        }
        //查询数据库
        Userinfo userinfo = userService.getUserByname(username);
        if (userinfo != null && userinfo.getId() > 0){
            //有效的用户 判断密码是否正确
            if (password.equals(userinfo.getPassword())){
                // 登录成功
                HttpSession session =request.getSession();
                session.setAttribute(AppVariable.USER_SESSION_KEY,userinfo);
                userinfo.setPassword("");//返回前端之前隐藏密码

                return AjaxResult.successs(userinfo);
            }
        }
        return AjaxResult.fail(0,null);
    }

    @RequestMapping("/showinfo")
    public AjaxResult showInfo(HttpServletRequest request){
        UserinfoVo userinfoVo =new UserinfoVo();
        // 1. 得到当前的登录用户 从session中拿
        Userinfo userinfo = UserSessionUtils.getSessUser(request);
        if (userinfo == null){
            return AjaxResult.fail(-1,"非法请求");
        }
        // SPring提供的深克隆的方法 , 字段一样就复制
        BeanUtils.copyProperties(userinfo,userinfoVo);
        // 2. 得到用户发表文章的总数
        userinfoVo.setArtCount(articleServer.getArtCountByUid(userinfo.getId()));
        return AjaxResult.successs(userinfoVo);
    }
    /**
     * 注销（退出登录）
     *
     * @param session
     * @return
     */
    @RequestMapping("/logout")
    public AjaxResult logout(HttpSession session) {
        session.removeAttribute(AppVariable.USER_SESSION_KEY);
        return AjaxResult.successs(1);
    }
    // URL 必须小写
    @RequestMapping("/getuserbyid")
    public AjaxResult getUserById(Integer id) {
        if (id == null || id <= 0) {
            // 无效参数
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo = userService.getUserById(id);
        if (userinfo == null || userinfo.getId() <= 0) {
            // 无效参数
            return AjaxResult.fail(-1, "非法参数");
        }
        // 去除 userinfo 中的敏感数据，ex：密码
        userinfo.setPassword("");
        UserinfoVo userinfoVO = new UserinfoVo();
        BeanUtils.copyProperties(userinfo, userinfoVO);
        // 查询当前用户发表的文章数
        userinfoVO.setArtCount(articleServer.getArtCountByUid(id));
        return AjaxResult.successs(userinfoVO);
    }





}
