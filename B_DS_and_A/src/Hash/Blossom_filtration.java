package Hash;

public class Blossom_filtration {
    public static void main(String[] args) {
        int a =0;// a 32 bit
        int [] arr =new int[10];// 32*10 -> 320bits数组
        /* arr [0] int 0 - 31
        *  arr [1] int 32 - 63 */
        int i =178;//想取第178个bit 状态

        int numIndex = 178/32;//找到178所在的数字
        int bitIndex = 178%32;//找到178所在数字中的位数

        //拿到178位的状态
        int s = (   (arr[numIndex] >> (bitIndex))       & 1);

        //将178位的状态改成 1
        arr[numIndex] = arr[numIndex] | ( 1 << (bitIndex) );


        //将178位的状态改成 0
        arr[numIndex] = arr[numIndex] & ( ~ 1 << (bitIndex) );

    }
}
