package TanXin;

import java.util.Arrays;
import java.util.Comparator;

public class BestArrange {
    /*最短的会议时间*/
    public static class Progam{
        public int start;//开始时间
        public int end;//结束时间

        public Progam(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }
    public static class ProgramCompare implements Comparator<Progam>{
        @Override
        public int compare(Progam o1,Progam o2){
            return o1.end-o2.end;//小跟堆
        }
    }
    public static int bestArrange(Progam[] progams,int timePoint){
        Arrays.sort(progams);
        //想法,就是从小往大,尽可能的安排多的会议
        int result = 0;
        //从左往右一次遍历所有会议
        for (int i = 0; i < progams.length; i++) {
            if (timePoint <= progams[i].start){
                //如果开始时间比现在会议的时间早,就安排它,不是就不安排它
                //然后等于这个会议的结束时间,在去看
                result++;
                timePoint = progams[i].end;
            }
        }
        return result;
    }

}
