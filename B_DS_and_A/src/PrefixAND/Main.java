package PrefixAND;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

// 注意类名必须为 Main, 不要有任何 package xxx 信息
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int q = in.nextInt();
       int[][] arr = new int[n][m];
        int[][] dp = new int[n+1][m+1];
        int i =0;
        for (int j = 1; j <= m; j++) {
            if (i < n){
                dp[i][j] =dp[i][j-1]+arr[i][j-1];
            }
            if (j == m){
                j=0;
                i++;
            }
        }
        for (int j = 0; j < q; j++) {
            int x1 = in.nextInt();
            int y1 = in.nextInt();
            int x2 = in.nextInt();
            int y2 = in.nextInt();
            process(dp,x1,y1,x2,y2);
        }
    }
    private static void process(int[][] dp , int x1 ,int y1 , int x2 , int y2){
        int sum =0 ;
        for (int i = x1; i <=x2; i++) {
            for (int j = y1; j <=y2 ; j++) {
                sum+=dp[i][j];
            }
        }
        System.out.println(sum);
    }
    public int subarraysDivByK(int[] nums, int k) {
        int sum=0,ret=0;
        Map<Integer,Integer> map = new HashMap<>();
        map.put(0%k,1);
        for (int i = 0; i < nums.length; i++) {
            sum+=nums[i];
            int r = (sum%k+k)%k;
            ret+=map.getOrDefault(r,0);
            map.put(sum,map.getOrDefault(sum,0)+1);
        }
        return ret;
    }
    public int findMaxLength(int[] nums) {

        int sum =0 ;
        int ret =0 ;
        HashMap<Integer,Integer> map = new HashMap<>();
        map.put(0,-1);
        for (int i = 0; i < nums.length; i++) {
            sum+=nums[i]==0?-1:1;
            if (map.containsKey(sum))ret=Math.max(ret,i-map.get(sum));
            else map.put(sum,i);

        }
        return ret;
    }
   
}