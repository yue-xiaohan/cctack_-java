package Graph;

public class Edge {

    public int weight;//代表这个边的权重
    public Node from;//代表这个边来自那个点
    public Node to ;//代表这个边去向哪里

    public Edge(int weight, Node from, Node to) {
        this.weight = weight;
        this.from = from;
        this.to = to;
    }
}
