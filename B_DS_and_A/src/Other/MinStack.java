package Other;

import java.util.Stack;

public class MinStack {
    private Stack<Integer> s1;
    private Stack<Integer> res;
    public MinStack(){
        s1 = new Stack<>();
        res = new Stack<>();
    }
    public void push(Integer k){
        s1.push(k);
        if (res.isEmpty() || k < res.peek()){
            res.push(k);
        }else {
            res.push(res.peek());
        }
    }
    public int pop(){
        res.pop();
        return s1.pop();
    }
    public int getMin(){
        return res.peek();
    }
}
