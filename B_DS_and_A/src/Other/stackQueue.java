package Other;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class stackQueue {
    // 用栈实现队列
    private Stack<Integer> stackPush;
    private Stack<Integer> stackPop;


    public stackQueue(){
        stackPush =new Stack<>();
        stackPop = new Stack<>();

    }
    public void push(int push){
        stackPush.push(push);
        dao();
    }
    public int poll(){
        if (stackPop.empty() && stackPush.empty()){
            throw new RuntimeException("队列是空的");
        }
        dao();
        return stackPop.pop();
    }
    public int peek(){
        if (stackPop.empty() && stackPush.empty()){
            throw new RuntimeException("队列是空的");
        }
        dao();
        return stackPop.peek();
    }
    private void dao(){
        if (stackPop.isEmpty()){
            while (!stackPush.isEmpty()){
                stackPop.push(stackPush.pop());
            }
        }
    }
}
