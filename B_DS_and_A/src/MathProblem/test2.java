package MathProblem;

import java.util.Scanner;

public class test2 {
    //判断是不是2的幂和4的幂
    public static boolean is2Power(int n){
        return ( n & (n -1)) == 0;
    }
    public static boolean is4Power(int n){
        //0x55555555是32位01交替到最后的到的一个数,01交替就是4的位数
        return ( n & (n -1)) == 0 && (n & 0x55555555) != 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()){
            int n =scanner.nextInt();
            if (is4Power(n)){
                System.out.println("yes");
            }else {
                System.out.println("no");
            }
        }
    }
}
