package MathProblem;

import java.util.Stack;

public class Solution {
    // 数学中的位运算 - 力扣
    //各位相加
    public int addDigits(int num) {
        while (num >= 10) {
            int sum = 0;
            while (num > 0) {
                sum += num % 10;
                num /= 10;
            }
            num = sum;
        }
        return num;
    }



    public int reverse(int x) {
        int res = 0;
        while(x!=0) {
            //每次取末尾数字
            int tmp = x%10;
            //判断是否 大于 最大32位整数
            if (res>214748364 || (res==214748364 && tmp>7)) {
                return 0;
            }
            //判断是否 小于 最小32位整数
            if (res<-214748364 || (res==-214748364 && tmp<-8)) {
                return 0;
            }
            res = res*10 + tmp;
            x /= 10;
        }
        return res;
    }





    public static boolean isPowerOfThree(int n) {
        // 判断是不是三的幂
        while (n != 0 && n % 3 == 0) {
            n /= 3;
        }
        return n == 1;
    }
    public String convertToBase7(int num) {
        //给定一个整数 num，将其转化为 7 进制，并以字符串形式输出。
        //用除留取余数法
        if (num == 0){
            return "0";
        }
        int n =num;
        Stack<Integer> stack = new Stack<>();
        while (n != 0){
            stack.add(Math.abs(n%7));
            n=n/7;
        }
        StringBuilder stringBuilder = new StringBuilder();
        while (!stack.isEmpty()){
            stringBuilder.append(stack.pop());
        }
        if (num < 0)return "-"+stringBuilder.toString();
        return stringBuilder.toString();
    }
    public boolean isUgly(int n) {
        //丑数 就是只包含质因数 2、3 和 5 的正整数
        if (n <= 0) {
            return false;
        }
        int[] factors = {2, 3, 5};
        for (int factor : factors) {
            while (n % factor == 0) {
                n /= factor;
            }
        }
        return n == 1;
    }
    public int reverseBits(int n) {
        //颠倒给定的 32 位无符号整数的二进制位。
        int rev = 0;
        for (int i = 0; i < 32 && n != 0; ++i) {
            rev |= (n & 1) << (31 - i);
            n >>>= 1;
        }
        return rev;
    }
    public int hammingWeight(int n) {
        // 二进制1的个数
        int ret = 0;
        while (n != 0){
            n=n&n-1;
            ret++;
        }
        return ret;
    }
    public int findComplement(int n) {
        int k = 0;
        while (n != 0){
            k |=  ~(n & 1);
            k=k<<1;
            n =n>>>1;
        }
        return k;
    }
    public int hammingDistance(int x, int y) {
        // 算两个整数之间的 汉明距离 指的是这两个数字对应二进制位不同的位置的数目。
        int s = x ^ y, ret = 0;
        while (s != 0) {
            ret += s & 1;
            s >>= 1;
        }
        return ret;
    }

    public static boolean hasAlternatingBits(int n) {
        int prev = 2;
        while (n != 0) {
            int cur = n % 2;
            if (cur == prev) {
                return false;
            }
            prev = cur;
            n /= 2;
        }
        return true;
    }

    public static int trailingZeroes(int n) {
        int ans = 0;
        for (int i = 5; i <= n; i += 5) {
            for (int x = i; x % 5 == 0; x /= 5) {
                ++ans;
            }
        }
        return ans;
    }

    public String toHex(int _num) {
        if (_num == 0) return "0";
        long num = _num;
        StringBuilder sb = new StringBuilder();
        if(num < 0) num = (long)(Math.pow(2, 32) + num);
        while (num != 0) {
            long u = num % 16;
            char c = (char)(u + '0');
            if (u >= 10) c = (char)(u - 10 + 'a');
            sb.append(c);
            num /= 16;
        }
        return sb.reverse().toString();
    }
    public static int titleToNumber(String columnTitle) {
        int number = 0;
        int multiple = 1;
        for (int i = columnTitle.length() - 1; i >= 0; i--) {
            int k = columnTitle.charAt(i) - 'A' + 1;
            number += k * multiple;
            multiple *= 26;
        }
        return number;
    }

    public String convertToTitle(int columnNumber) {
        StringBuffer sb = new StringBuffer();
        while (columnNumber > 0) {
            int a0 = (columnNumber - 1) % 26 + 1;
            sb.append((char)(a0 - 1 + 'A'));
            columnNumber = (columnNumber - a0) / 26;
        }
        return sb.reverse().toString();
    }
    public static int countDigitOne(int n) {
        int digit = 1, res = 0;
        int high = n / 10, cur = n % 10, low = 0;
        while(high != 0 || cur != 0) {
            if(cur == 0) res += high * digit;
            else if(cur == 1) res += high * digit + low + 1;
            else res += (high + 1) * digit;
            low += cur * digit;
            cur = high % 10;
            high /= 10;
            digit *= 10;
        }
        return res;
    }
    //构造一个矩阵乘积等于面积
    public static int[] constructRectangle(int area) {
        int[] arr = new int[2];
        if (area == 1 || area == 0)return new int[]{area,area};
        int dif =Integer.MAX_VALUE ; // 记录差值, 差值最小的就是最好的
        for (int i = area/2; i >=1 ; i--) {
            if (area % i == 0 && Math.abs(i-(area/i)) < dif){
                arr[0] = i > area/i?i:area/i;
                arr[1] =i > area/i?area/i:i;
                dif = i-(area/i);
            }
        }
        return arr;
    }
    // 判断一个数的因数加起来是否等于它本身
    public boolean checkPerfectNumber(int num) {
        if (num <= 1)return false;//考虑边界
        int isNum =0;
        for (int i = num/2; i > 1 ; i--) {
            if (num % i == 0){
                isNum+=i;
            }
        }
        return isNum+1 == num;
    }

    public double myPow(double x, int n) {
        long N = n;
        return N >= 0 ? quickMul(x, N) : 1.0 / quickMul(x, -N);
    }

    public double quickMul(double x, long N) {
        if (N == 0) {
            return 1.0;
        }
        double y = quickMul(x, N / 2);
        return N % 2 == 0 ? y * y : y * y * x;
    }
    //
    static final int MOD = 1337;

    public int superPow(int a, int[] b) {
        int ans = 1;
        for (int i = b.length - 1; i >= 0; --i) {
            ans = (int) ((long) ans * pow(a, b[i]) % MOD);
            a = pow(a, 10);
        }
        return ans;
    }

    public int pow(int x, int n) {
        int res = 1;
        while (n != 0) {
            if (n % 2 != 0) {
                res = (int) ((long) res * x % MOD);
            }
            x = (int) ((long) x * x % MOD);
            n /= 2;
        }
        return res;
    }
    //剑指 Offer 14- II. 剪绳子 II
    public static int cuttingRope(int n) {
        int maxValue = 0;
        for(int i=2;i<=n/2;i++){
            int y =n%i;
            int x = n/i;
            int currentMax=1;
            for(int j = 0;j<i;j++){
                int z = x;
                if(y>0){
                    z++;
                    y--;
                }
                currentMax*=z;
            }
            maxValue = Math.max(maxValue,currentMax);
        }
        return maxValue;
    }

    public static void main(String[] args) {
        cuttingRope(10);
    }





















}
