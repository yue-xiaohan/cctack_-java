package MathProblem;

public class test {
    //给你俩个数,你如何不通过比较的方式下,得到那个数较大
    public static int flip(int n){
        //在保证参数不是 1 就是 0 的情况下
        // 1 -> 0   0 - > 1
        return n^1;
    }
    public static int sign(int n){
//        得到符号位,负数返回1  正数返回 0
        return flip( (n >> 31) & 1);
    }
    public static int getMax1(int a , int b){
        /*此方法存在的问题 -  a -b可能会溢出
        * 但能说明 :  互斥条件是可以做if else 的意思的*/
        int c = a -b;
        /*sca和scb 就是说明了 a和b 谁大,谁对应的为1*/
        int scA = sign(c);//a-b非负 , scA 为1 ; a-b是负数, sca = 0
        int scB = flip(scA);//得到与sca相反的符号位
        //sca为0 scb为1 ; sca=1  scb=0
        return a*scA+b*scB;
    }
    public static int getMax2(int a,int b){
        int c= a -b;
        int sa = sign(a);//a的符号状态
        int sb = sign(b);//b的符号状态
        int sc = sign(c);//c的符号状态

        int difSqb = sa ^ sb;//判断 a 和 b 的符号相同不 ,相同 0 不相同 1
        int sameSab = flip(difSqb);//得到互斥符号.

        int returnA =difSqb *sa +sameSab *sc ;
        //如果 a 和 b的符号相同,就不溢出, 如果a和b的符号不同的情况下,a>0返回 a . 这里的 + 号表示或者
        int returnB = flip(returnA);
        return a * returnA +b *returnB;


    }
}
