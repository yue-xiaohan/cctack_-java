package TowSerach;

class Solution {
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        int left = 0,right =matrix[0].length-1;
        while (left < matrix.length && right>=0){
            if (matrix[left][right] > target){
                right--;
            }else if (matrix[left][right] < target){
                left++;
            }else {
                return true;
            }
        }
        return false;
    }
}