package Http2;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetAddress;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.File;

public class HttpServer {

    /**
     * WEB_ROOT是HTML和其它文件存放的目录. 这里的WEB_ROOT为工作目录下的webroot目录
     */
    public static final String WEB_ROOT = System.getProperty("user.dir") + File.separator + "webroot";
    //System.getProperty确定当前的系统属性。File.separator与系统相关的默认名称
    // 关闭服务命令
    private static final String SHUTDOWN_COMMAND = "/SHUTDOWN";
    public static void main(String[] args) {
        HttpServer server = new HttpServer();
        //等待连接请求
        server.await();
    }
    public void await() {
        ServerSocket serverSocket = null;
//      这个类实现了服务器套接字。服务器套接字等待来自网络的请求。它基于该请求执行某些操作，然后可能向请求者返回结果。
        int port = 8080;
        try {
            //服务器套接字对象
            serverSocket = new ServerSocket(port, 1, InetAddress.getByName("127.0.0.1"));
            //构造方法：创建一个具有指定端口的服务器，侦听backlog和本地IP地址绑定。
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        // 循环等待一个请求
        while (true) {
            Socket socket = null;
//            这个类实现了客户端套接字
            InputStream input = null;
//            该抽象类是所有的类表示字节输入流的父类。
            OutputStream output = null;
//            该抽象类是所有类的字节输出流的父类。
            try {
                //等待连接，连接成功后，返回一个Socket对象
                socket = serverSocket.accept();
                input = socket.getInputStream();
                output = socket.getOutputStream();

                // 创建Request对象并解析
                Request request = new Request(input);
                request.parse();
                // 检查是否是关闭服务命令
                if (request.getUri().equals(SHUTDOWN_COMMAND)) {
                    break;
                }

                // 创建 Response 对象
                Response response = new Response(output);
                response.setRequest(request);
                response.sendStaticResource();

                // 关闭 socket 对象
                socket.close();

            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }
    }
}

