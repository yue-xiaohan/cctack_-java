import java.util.Locale;
import java.util.Scanner;

class Person{
//
}
public class Test {
    public static int countSegments(String s) {
        s = s.trim();
        if( s == null || s.isEmpty() ){
            return 0;
        }
        String[] str = s.split(" ");
        int count = 0;
        for (int i = 0; i < str.length; i++) {
            //里面是空格的不算，有可能多个空格
            if(str[i].isEmpty()) {
                continue;
            }
            count ++;
        }
        return count;
    }

    public static void main(String[] args) {
        int ret=countSegments(", , , ,        a, eaefa");
        System.out.println(ret);

    }

    public static void main16(String[] args) {
        Scanner sc=new Scanner(System.in);

        while(sc.hasNextLine())
        {
            String str1=sc.nextLine();
            String str2=str1.toLowerCase(Locale.ROOT);
            System.out.println(str2);
        }
    }
    public static void main15(String[] args) {
        Scanner sc=new Scanner(System.in);
        int count=0;
        String str1=sc.nextLine();
        if (str1.equals("") ){
            System.out.println("0");
            return;
        }
        String[] strs=str1.split(" ");//以多个分隔符分割，用|分开
        System.out.println(strs.length);
        for (int i = 0; i < strs.length; i++) {
           if (strs.equals("")){
               count++;
           }
        }


    }

    public static void main14(String[] args) {
        //双引号引起来的值，就存在字符串常量池当中，如果有就不存储，直接返回字符串常量池的对象即可。
        //用new是创建新的对象中，存储的是常量池对象的地址。
        //不同编译器中的汇编语言不一样
    }
    public static void main13(String[] args) {
        String str="   hello   abcd  ";
        System.out.println(str);
        System.out.println(str.trim());//去掉多余的空格
    }
    public static void main12(String[] args) {
        String str1="abcdefg";
        String str2=str1.substring(2);//返回从2位置开始，截取的所有字符[)
        String str3=str1.substring(2,4);//返回从2位置开始，到位置4的取的所有字符[)
        System.out.println(str2);
        System.out.println(str3);
    }
    public static void main11(String[] args) {
        String str1="192&168=1";
        String[] strs=str1.split("&|=");//以多个分隔符分割，用|分开
        for (int s = 0; s < strs.length; s++) {
            System.out.println(strs[s]);
        }
    }
    public static void main10(String[] args) {
        //字符串的拆分
        String str1="hello little Bitter";
       String []str2=str1.split(" ");

        System.out.println(str2.length);
        for (int s = 0; s < str2.length; s++) {
            System.out.println(str2[s]);
        }


    }
    public static void main9(String[] args) {
        //字符串替换
        String str1="abcdabcd";
        String str2=str1.replace('a','s');//所有对字符串的改变都不是在原来的改变
        System.out.println(str1);
        System.out.println(str2);
    }
    public static void main8(String[] args) {
        //字符串转数组
        String str1="abcdef";
       char[] ch=str1.toCharArray();
        System.out.println(ch);
    }
    public static void main7(String[] args) {
        //字符串大小写的转换
        String str1="asdc";
        String str2="QWER";
        System.out.println(str1.toUpperCase(Locale.ROOT));
        System.out.println(str2.toLowerCase(Locale.ROOT));
    }
    public static void main6(String[] args) {
        //字符串与数值的转化
        String str=String.valueOf(123);//可直接调用valuof将任何基本数据类型转换为字符串
        //tr=String.valueOf(false);
        System.out.println(str);
        String str2=String.valueOf(new Person());
        System.out.println(str2);
        //基本类型对应的类类型是包装类，也提供了面向对象的思想。
        int data=Integer.parseInt(str);
        System.out.println(data);//将字符串转换为数字。

    }
    public static void main5(String[] args) {
        //字符串的查找
        String str1=new String("hello");
        char s=str1.charAt(1);
        System.out.println(s);
        String str2=new String("asdsadasdasdsadcsrweq");
        System.out.println(str2.indexOf('c'));//返回第一次出现字符的位置
        System.out.println(str2.indexOf("csr"));//在主串中查找子串，返回第一次出现的位置


    }
    public static void main4(String[] args) {
        //字符串比较大小通过compereto比较
        //按字典序比较
        String str3=new String("helloasdasd");
        String str4=new String("hello");
        System.out.println(str3.compareTo(str4));
        String str5=new String("Hello");
        String str6=new String("hello");
        System.out.println(str5.compareToIgnoreCase(str6));//忽略大小写比较，防止没有大小写

    }
    public static void main3(String[] args) {
        //字符串的比较
        //俩种场景：1相不相同 2大小
        String str="hello";
        String str2="asd";
      //  System.out.println(str.equals(str2));
        String str3=new String("helllo");
        String str4=new String("helllo");
        System.out.println(str3==str4);//这种比较方式一定是错误的
        System.out.println(str3.equals(str4));//string重写了equals可以直接调用

    }
    public static void main2(String[] args) {
        String str="hello";
        System.out.println(str.length());
        int[] a=new int[10];
        System.out.println(a.length);
        //String字符串.length是方法，而数组的length是属性，区别在于有没有小括号
        String str2="";
        System.out.println(str2.length());
        System.out.println(str2.isEmpty());
        String str3=null;
        System.out.println("asdasd".length());//字符串调用也可以是常量调用
    }
    public static void main1(String[] args) {
        //常用的三种方式来构造字符串
        //String是引用数据类型，并不直接存储对象本身
        String str="hello";
        String str2=new String("hello");
        //从结果上来说没有区别。
        System.out.println(str);
        System.out.println(str2);
        char[] values={'a','b','c','d'};
        String str3=new String(values);
        System.out.println(str3);
    }

}
