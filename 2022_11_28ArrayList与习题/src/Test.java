import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;


public class Test {

    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> ret = new ArrayList<>();
        List<Integer> row = new ArrayList<>();
        row.add(1);
        ret.add(row);
        for (int i = 1; i < numRows; i++) {
            List<Integer> prevRow = ret.get(i-1);//前一行
            List<Integer> curRow = new ArrayList<>();
            curRow.add(1);//第一个1
            //中间curRow list的赋值
            for (int j = 1; j < i; j++) {
                int x = prevRow.get(j)+prevRow.get(j-1);
                curRow.add(x);
            }
            curRow.add(1);//最后一个1
            ret.add(curRow);
        }
        return ret;
    }


    public static void main2(String[] args) {
        ArrayList<Character> list = new ArrayList<>();
        String s1 = "welcome to bit";
        String s2 = "come";

        for (int i = 0; i < s1.length(); i++) {
            char ch = s1.charAt(i);
            if(!s2.contains(ch+"")) {
                list.add(ch);
            }
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i));
        }
    }





    public static void main1(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(11);
        list.add(12);
        list.add(13);
        list.add(14);
        list.add(15);
        list.add(16);
        System.out.println("=============");
        for (int i = 0; i < list.size(); i++) {
            System.out.print(list.get(i)+" ");
        }
        System.out.println();
        System.out.println("=============");
        for (Integer x : list) {
            System.out.print(x+" ");
        }
        System.out.println();
        System.out.println("=============");
        ListIterator<Integer> it =  list.listIterator();
        while (it.hasNext()) {
            System.out.print(it.next()+" ");
        }

        /*List<Integer> sub = list.subList(1,3);//[1,3)
        System.out.println(sub);

        System.out.println("=============");
        sub.set(0,999);

        System.out.println(sub);
        System.out.println(list);*/

        /*ArrayList<Number> arrayList1 = new ArrayList<>();

        arrayList1.add(1);

        arrayList1.add(0,10);

        arrayList1.remove(new Integer(10));

        arrayList1.get(0);
        System.out.println(arrayList1);*/

    }
}