package Work;

import java.util.Arrays;

/**
 * 给你一个 升序排列 的数组 nums ，请你 原地 删除重复出现的元素，使每个元素 只出现一次 ，返回删除后数组的新长度。
 * 元素的 相对顺序 应该保持 一致 。
 * 由于在某些语言中不能改变数组的长度，所以必须将结果放在数组nums的第一部分。更规范地说，如果在删除重复项之后有 k 个元素，
 * 那么 nums 的前 k 个元素应该保存最终结果。将最终结果插入 nums 的前 k 个位置后返回 k 。
 * 不要使用额外的空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 */


/*
     解题思路：
       1. 设置一个计数，记录从前往后遍历时遇到的不同元素的个数
          由于不同的元素需要往前搬移，那count-1就是前面不同元素
          搬移之后，最后一个元素的位置，下一次在遇到不同元素就应该
          搬移到count位置
       2. 遍历数组，如果nums[i]与nums[count-1]不等，就将nums[i]搬移
          到nums[count]位置，不同元素多了一个，给count++
       3. 循环结束后，返回count
*/

class Solution2 {
    public int removeDuplicates(int[] nums) {
        int count = 1;   // 标记从前往后遍历时，遇到的不同元素的个数
        for(int i = 1; i < nums.length; ++i){
            // 注意：count是从前往后遍历时遇到的不同元素的个数
            //       count-1就是i位置之前最后一个不同元素的位置
            // 因此需要将num[i]搬移到nums[count]处
            if(nums[count-1] != nums[i]){
                nums[count++] = nums[i];
            }
        }

        return count;
    }
}
public class Test3 {
    public int removeDuplicates(int[] nums) {
        int K=0;
        int Max=0;
        for (int i = 0; i < nums.length; i++) {
            if (Max < nums[i]){
                Max=nums[i];
            }
        }
        for (int i = 0; i < nums.length; i++) {
            int k1=nums[i];
            for (int j = i+1; j < nums.length; j++) {
                if(k1 == nums[j]  && k1!=(Max+1)*2){
                    nums[j]=(Max+1)*2;
                    K++;
                }
            }
        }
        Arrays.sort(nums);
        K=nums.length-K;

        return  K;
    }
    public void Print(int[] nums1,int K){
        for (int i = 0; i < K; i++) {
            System.out.print(nums1[i]+" ");
        }
    }

    public static void main(String[] args) {
        int[] nums=new int[]{0,0,1,1,1,2,2,3,3,4};
       /* Test3 T3=new Test3();//自己写的代码测试
        int K =T3.removeDuplicates(nums);
        T3.Print(nums,K);*/
        Solution2 S2=new Solution2();
        int K=S2.removeDuplicates(nums);
        Test3 T3=new Test3();
        T3.Print(nums,K);

    }
}
