package Work;
/**
 *  //杨辉三角写法
 */

import java.util.ArrayList;
import java.util.List;

public class Test1 {
    public List<List<Integer>> generate(int numRows) {

        //   ArrayList是顺序表;
        List<List<Integer>> ret=new ArrayList<>();
        //构建一个二维数组
        List<Integer> row=new ArrayList<>();//第一行
        row.add(1);
        ret.add(row);//将第一行放进去
        for (int i = 1; i < numRows; i++) {
            //杨辉三角的第二行开始
            List<Integer> pveroe=ret.get(i-1);//获取上一行
            List<Integer> roe=new ArrayList<>();
            roe.add(1);//第一个元素置为1

            for (int j = 1; j < i; j++) {
                //每行的第一个元素和最后一个元素为1
              int x= pveroe.get(j)+pveroe.get(j-1);
              roe.add(x);
            }
            roe.add(1);//最后一个元素置为1
            ret.add(roe);
        }
        return ret;

    }

    public void Print(List<List<Integer>> ret){
        for (int i=0;i<ret.size();i++){
            List<Integer> roe=ret.get(i);
            for (int j = 0; j < roe.size(); j++) {
                System.out.print(roe.get(j)+" ");
            }
            System.out.println();
        }

    }

    public static void main(String[] args) {
        int num=3;
        Test1 T=new Test1();
        List<List<Integer>> ret= T.generate(num);
        T.Print(ret);


    }
}
