﻿package demo.conpent;


import org.springframework.stereotype.Component;

@Component("aaa")
public class User {
    public int id;
    public String name;
    @Override
    public String toString() {
        return "user{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}
