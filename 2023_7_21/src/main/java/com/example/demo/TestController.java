package com.example.demo;

import com.example.demo.controller.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//@Controller
//@ResponseBody
@RestController
public class TestController {
//    @Value("${im}")
//    private String im;
//    @RequestMapping("/sy")
//    public  String say(){
//        return "hello World"+im;
//    }
    @Autowired
    private Student student;
    @RequestMapping("sh")
    private String say(){
        return student.toString();
    }
}
