package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;

public class LogController {

    private static Logger logger = LoggerFactory.getLogger(LogController.class);

    @RequestMapping("/log")
    public void log(){
        String msg = "这是测试打印日志的一条语句";
        // 使用logger打印对象

        logger.info(msg);
    }
}
