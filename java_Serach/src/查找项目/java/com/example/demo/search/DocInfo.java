package com.example.demo.search;
// 索引信息 - 表示一个文档对象
public class DocInfo {
    private int docId;// 文档的唯一身份标识
    private String title;// 该文档的标题- 就用文件名来表示
    private String url;//对应线上文档URL, 根据本地文件路径来构造出线上文档的URL
    private String content;// 去掉标签后的正文

    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "DocInfo{" +
                "docId=" + docId +
                ", title='" + title + '\'' +
                ", url='" + url + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
