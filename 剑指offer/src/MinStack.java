import java.util.*;
class MinStack {
    /*剑指 Offer 30. 包含min函数的栈*/
    /** initialize your data structure here. */
   Stack<Integer> stack1;
   Stack<Integer> stack2;
    public MinStack() {
        stack1=new Stack<>();
        stack2=new Stack<>();
    }

    public void push(int x) {
       stack1.push(x);
       if (stack2.isEmpty()){
           stack2.push(x);
       }else {
           if (x<=stack2.peek()){
               stack2.push(x);
           }
       }
    }
    
    public void pop() {
        if (!stack1.isEmpty()){
            int tmp= stack1.pop();
            if (tmp==stack2.peek()){
                stack2.pop();
            }
        }
    }
    
    public int top() {
        if (!stack1.isEmpty()){
            return stack1.peek();
        }
        return -1;

    }
    
    public int min() {
        if (!stack2.isEmpty()){
            return stack2.peek();
        }
        return -1;
    }
}