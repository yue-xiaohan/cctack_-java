import com.sun.org.apache.xerces.internal.impl.dv.xs.AnyURIDV;

import java.util.Arrays;

class Solution2 {
//    剑指offer，第二页得习题了
    /*剑指 Offer II 101. 分割等和子集*/
    public boolean canPartition(int[] nums) {
        Arrays.sort(nums);
        int left=0;
        int right=nums.length-1;
        int leftSum=nums[left],rightSum=nums[right];
        while (left < right){
            if (leftSum == rightSum){
                break;
            }else if (leftSum > rightSum){
                rightSum+=nums[--right];
            }else {
                leftSum+=nums[++left];
            }
        }
        if (++left==right){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        int [] arr={1,5,11,5};
        Solution2 s2=new Solution2();
        s2.canPartition(arr);
    }

}