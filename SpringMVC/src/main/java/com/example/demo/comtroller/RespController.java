package com.example.demo.comtroller;

import com.example.demo.model.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody

@RequestMapping("/resp")
public class RespController {

    @RequestMapping("/hi")
    public String sayHi(User user) {
        System.out.println("id"+user.getId());
        System.out.println("name"+user.getName());
        return "/index.html";
    }

}
