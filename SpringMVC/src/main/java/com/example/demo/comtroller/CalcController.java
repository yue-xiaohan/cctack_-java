package com.example.demo.comtroller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller // 让Spring框架启动时加载
//@ResponseBody // 返回的是非页面的数据
//@RequestMapping("/ca")
public class CalcController {
    @RequestMapping("/hi")
    public String process(){
        return "hello.html";
    }
    @RequestMapping("/calc")//路由注册
    public String calc(Integer num1 ,Integer num2) {

        if (num1 == null || num2 == null) return "参数错误";
        return "结果=" + (num1 + num2);
    }

}
