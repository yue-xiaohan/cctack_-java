class Animal{
    //
    public String name;
    public int age;
    public void  eat(){
        System.out.println("name="+name+"在吃饭");
    }
    public Animal() {

    }
   public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }


}
class Dog extends Animal{
    public void wang(){
        //
        System.out.println(super.name+"汪汪叫");
        //被继承但是不能访问，当修饰符是private
    }


    public Dog() {//用子类构造方法来初始化父类成员。
        super();

    }
    public Dog(int name) {//用子类构造方法来初始化父类成员。

        super("asd",10);
    }
}
class Cat extends Animal{
    public void miao(){
        System.out.println(name+"喵喵叫");
    }
}
public class chap12 {
    public static void main(String[] args) {
        Dog dog=new Dog();
        dog.name="大黄";
        dog.wang();
        dog.eat();
        System.out.println("==============");
        Cat cat=new Cat();
        cat.name="小白";
        cat.miao();
        cat.eat();
    }
}
