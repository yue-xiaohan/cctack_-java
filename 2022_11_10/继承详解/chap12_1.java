

class  Base{
    public  int a;
    public  int b;//
    public  int c=199;
    public void menthodBase(){
        System.out.println("Base");
    }

}
class  Derived extends Base{
    public int c=45;
    //子类有就拿子类的，子类没有就拿父类的
    //如果父类和子类有相同的变量，优先访问子类的
    public void menthodBase(){
        super.menthodBase();
        //super.data  在子类中访问父类的成员变量
        //super.方法   在子类中访问父类的方法
        System.out.println("Derived");
    }
    public void func(){
        System.out.println(a);
        System.out.println(b);
        System.out.println(super.c);
        System.out.println(c);
    }
}

public class chap12_1 {
    public static void main(String[] args) {
        Derived derived=new Derived();
        derived.menthodBase();//就近原则


        //System.out.println(derived.a);
       // derived.func();
        Base base=new Derived();
        System.out.println(base.c);
    }
}
