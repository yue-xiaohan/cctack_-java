package demo_M;
//抽象类和接口完
import java.util.Arrays;
import java.util.Comparator;


class Student implements Comparable<Student>{
    public String name;
    public int age;
    public int score;

    public Student(String name, int age, int score) {
        this.name = name;
        this.age = age;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", score=" + score +
                '}';
    }

    @Override
    public int compareTo(Student o) {
        return o.age - this.age;
    }
}
/*class Student {
    public String name;
    public int age;
    public int score;

    public Student(String name, int age, int score) {
        this.name = name;
        this.age = age;
        this.score = score;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", score=" + score +
                '}';
    }

}*/
class AgeComparator implements Comparator<Student> {

    @Override
    public int compare(Student o1, Student o2) {
        return o1.age - o2.age;
    }
}

class ScoreComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.score - o2.score;
    }
}

class NameComparator implements Comparator<Student> {
    @Override
    public int compare(Student o1, Student o2) {
        return o1.name.compareTo(o2.name);
    }
}

public class chap15_0 {
    public static void main(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhagnsan",10,19);
        students[1] = new Student("abc",8,78);
        students[2] = new Student("lisi",15,57);

        //比较器。
        AgeComparator ageComparator = new AgeComparator();

        ScoreComparator scoreComparator = new ScoreComparator();

        NameComparator nameComparator = new NameComparator();

        Arrays.sort(students);

        System.out.println(Arrays.toString(students));


    }
    public static void main1(String[] args) {
        Student[] students = new Student[3];
        students[0] = new Student("zhagnsan",10,19);
        students[1] = new Student("abc",8,78);
        students[2] = new Student("lisi",15,57);
        /*if(students[0].compareTo(students[1]) > 0) {
            System.out.println("students[0] > students[0]");
        }*/
        Arrays.sort(students);
        System.out.println(Arrays.toString(students));
    }
}
