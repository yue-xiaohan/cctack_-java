package demo_M2;



import java.util.Objects;

/**
 * 深克隆和浅克隆
 */
class Money implements Cloneable{
    public double money = 12.25;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();//
    }
}
class Student implements Cloneable{
    public String name;
    public Money m = new Money();

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        // 只是克隆了Student对象
        Student student = (Student)super.clone();
        // 克隆了 Student对象 里面的Money对象
        student.m = (Money) this.m.clone();
        return student;
        //return super.clone();
    }

    /*public boolean equals(Object obj) {
        if(obj == null) {
            return false;
        }
        //指向的是同一个对象
        if(this == obj) {
            return true;
        }
        if(! (obj instanceof Student)) {
            return false;
        }
        Student student = (Student)obj;
        if(this.name.equals(student.name)) {
            return true;
        }
        return false;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
class Person {
}
class Animal{}
class Dog{
    public void func1(){}
    public void func2(){}
    public void func3(){}
}

public class Test {

    public static void main(String[] args) {
        Student student1 = new Student();
        student1.name = "zhangsan";

        Student student2 = new Student();
        student2.name = "zhangsan";

        //我们希望这两个对象 放到同一个位置去
        System.out.println(student1.hashCode());
        System.out.println(student2.hashCode());

    }

    public static void main4(String[] args) {
        Student student1 = new Student();
        student1.name = "zhangsan";

        Dog dog = new Dog();
        boolean flg = student1.equals(dog);
        System.out.println(flg);

        //Student student2 = student1;
       /* Student student2 = new Student();
        student2.name = "zhangsan";*/

        /*System.out.println(student1 == student2);
        boolean flg = student1.equals(student2);
        System.out.println("flg: "+flg);*/
    }
    public static void func(Object obj) {

    }

    public static void main3(String[] args) {
        func(new Money());
        func(new Person());
        func(new Animal());
        func(new Dog());
        Dog dog = new Dog();
        dog.func1();
        dog.func2();
        dog.func3();
        System.out.println("=======");
        new Dog().func1();
        new Dog().func2();
        new Dog().func3();

    }

    public static void main2(String[] args) throws CloneNotSupportedException  {
        Student student1 = new Student();
        Student student2 = (Student)student1.clone();
        System.out.println(student1.m.money);
        System.out.println(student2.m.money);
        System.out.println("================");
        student2.m.money = 99;
        System.out.println(student1.m.money);
        System.out.println(student2.m.money);

        Object o  = new Student();


    }

    public static void main1(String[] args) throws CloneNotSupportedException  {
        Student student1 = new Student();
        student1.name = "bitjiuyeke";
        Student student2 = (Student)student1.clone();
        System.out.println(student1);
        System.out.println(student2);
    }
}

