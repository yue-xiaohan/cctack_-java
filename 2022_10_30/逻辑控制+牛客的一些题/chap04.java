import java.util.Random;
import java.util.Scanner;

public class chap04 {
    //逻辑控制+方法讲解
    public static void main22(String[] args) {

    }
    public static void main21(String[] args) {
        //打印X图形
        //解题思路：第一种用二维数组，第二种找规律。
        Scanner sc = new Scanner(System.in);
        while(sc.hasNextInt()) {
            int n = sc.nextInt();
            for(int i = 0 ; i < n ; i++) {
                for(int j = 0 ; j < n ; j++) {
                    if(i == j || i + j == n-1) {
                        System.out.print("*");
                    } else {
                        System.out.print(" ");
                    }
                }
                System.out.println();
            }
        }
    }
    public static void main20(String[] args) {
        //求2个整数的最大公约数
        Scanner sc=new Scanner(System.in);
        Boolean flag=true;

        while (flag)
        {
            System.out.println("请输入俩个整数");
            int a=sc.nextInt();
            int b=sc.nextInt();
            int c=a<b?a:b;
            int d=1;
            while(true)
            {
                if(a%c == 0 && b%c == 0 )
                {
                    System.out.println("最大公约数为"+c);
                    break;
                }
                c--;
            }
            System.out.println("请问是否继续，继续按1,退出按0");
            d= sc.nextInt();
            if(d!=0)
            {
                flag=true;
            }
            else
            {
                flag=false;
            }
        }
    }

    public static void main19(String[] args) {
        //求二进制1的个数第二种方法
        Scanner sc=new Scanner(System.in);
        while (sc.hasNextInt())
        {
            int a=sc.nextInt();
            int count=0;
            while (a != 0)
            {
                if((a&1)==1)
                {
                    count++;
                }
                a=a>>>1;
            }
            System.out.println("该正数二进制1的个数为"+count);
        }

    }
    public static void main18(String[] args) {
        //求一个整数，在内存当中存储时，二进制1的个数。
        Scanner sc=new Scanner(System.in);

        while (sc.hasNextInt())
        {
            int count=0;
            int a= sc.nextInt();
            while(a != 0)
            {
                count++;
                a=a & (a-1);
            }
            System.out.println("该正数二进制1的个数为"+count);
        }
    }
    public static void main17(String[] args) {
        //给定一个数字，判定一个数字是否是素数
        Scanner sc=new Scanner(System.in);
        while (sc.hasNextInt())
        {
            int a=sc.nextInt();
            if (a == 1)
            {
                System.out.println("1不是素数");
                continue;
            }

            Boolean flag=true;
            for(int j=2;j<=Math.sqrt(a);j++)
            {
                if(a%j==0)
                {
                    flag=false;
                    break;
                }
            }
            if (flag)
            {
                System.out.println(a+"是素数");
            }
            else
            {
                System.out.println(a+"不是素数");
            }
        }
    }
    public static void main16(String[] args) {
        //输出 1000 - 2000 之间所有的闰年
        int a=2000;
        int count=0;
        for(int i=1000 ; i <= a ; i++)
        {
            int year=i;
            if((year%4==0&&year%100!=0)||(year%400==0))
            {
                if(count%20 == 0)
                {
                    System.out.println();
                }
                count++;
                System.out.printf("%d ",year);
            }
        }
    }
    public static void main15(String[] args) {
        //编写程序数一下 1到 100 的所有整数中出现多少个数字9
        int a=100;
        int count=0;
        for(int i=1;i <= a; i++)
        {
            if(i%10 == 9 || i/10 == 9)
            {
                if(i%10 == 9 && i/10 == 9)
                {
                    count++;
                }
                count++;
                System.out.printf("%d  ",i);
            }
        }
        System.out.printf("\n%d",count);
    }
    public static void main14(String[] args) {
        //打印素数
        int a=100;
        for(int i=2 ;i <= a; i++ )
        {
            boolean flag=true;
            for(int j=2;j<=Math.sqrt(i);j++)
            {
                if(i%j==0)
                {
                    flag=false;
                    break;
                }
            }
            if(flag)
            {
                System.out.printf("%d  ",i);

            }
        }
    }
    public static void main13(String[] args) {
        Scanner scan=new Scanner(System.in);
        Random random=new Random();
        int randNum=random.nextInt(100)+50;//范围[50,100)
        System.out.println(randNum);
        double i=Math.random();// 生成大于0.0并小于 1.0 。

        System.out.println(i);
    }
    public static void main12(String[] args) {
        //生成随机数
        Scanner scan=new Scanner(System.in);
        Random random=new Random();
        int randNum=random.nextInt(100);//范围[0,100)
        System.out.println(randNum);
    }
    public static void main11(String[] args) {
        //循环读取个数
        Scanner scan=new Scanner(System.in);
        while(scan.hasNextInt())//ctrl+D结束
        {
            int n=scan.nextInt();
            System.out.println("n= "+n);
        }

    }
    public static void main10(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入姓名：");
        String name=scan.next();//遇到空格就结束了
        System.out.println(name);
        String name1=scan.next();//上边输入的回车会背下边读到
        System.out.println(name1);
    }
    public static void main9(String[] args) {
        //java输出函数
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入姓名：");
        String name=scan.nextLine();
        System.out.println(name);
        System.out.println("请输入年龄：");
        int n=scan.nextInt();
        System.out.println(n);
        System.out.println("请输入工资");
        float f=scan.nextFloat();
        System.out.println(f);
        scan.close();//关闭
    }
    public static void main8(String[] args) {
        //当循环无判断条件时候，默认死循环。
        int a=100;
        for(int i=1;i <= a;i++)
        {
            if(i%3==0&&i%5==0){
                System.out.println(i);
               continue;

            }
        }
    }
    public static void main7(String[] args) {
        //break跳出当前循环,剩余循环不会在执行了，continue跳出本次循环，剩余循环还会走。
        int a=10;
        int i=1;
        while(i<a){
            if(i==2)
            {
                System.out.println(i);
            }
            i++;
        }
    }
    public static void main6(String[] args) {
        //求n的阶乘
        int a=3;//阶乘数
        int sum=1;
        for(int i=1;i <= a; i++ )
        {
            sum*=i;
        }
        System.out.println(sum);
    }
    public static void main5(String[] args) {
        //循环 调试测试
        //F8 不进入函数，F7进去函数，Alt+shift F7可以跳到原码
        int a=1;
        while(a<=5){
            System.out.println(a);
            a++;
        }
    }
    public static void main4(String[] args) {
        //测试goto语句
        outer:
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (j == 5) {
                    break outer;
                }
                System.out.println(j);
            }
        }
    }
    public static void main3(String[] args) {
        //测试当前是否是闰年

        int year=2022;

        ffa:
        if((year%4==0&&year%100!=0)||(year%400==0))
        {
            System.out.println("Yes 闰年");
            break ffa;
        }
        else
        {
            System.out.println("NO 闰年");
        }

        System.out.println("测试goto语句");
    }


    //if 语句可以用来选择，if elseif else 三者只会选择一种。
    //else 只与最近的if语句结合
    public static void main2(String[] args) {
        //09点35分
        int a = 110;
        if (10 == a) {
            System.out.println("a==10");
        } else if (a != 10) {
            System.out.println(a);
        } else {
            System.out.println("a!=10");
        }


    }

    public static void main1(String[] args) {
        //作业见解
        short a = 128;
        byte b = (byte) a;
        System.out.println(a);
        System.out.println(b);//强制类型转换丢失
    }
}
