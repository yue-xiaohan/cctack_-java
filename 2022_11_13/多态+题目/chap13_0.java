/**
 * 多态：不同的对象去完成，结果是不同的状态
 */
class Myvalue{
 int a=10;//
 public void add1(){
     System.out.println(a);
 }
}
class  bbb extends Myvalue{

    int b=2;

    @Override
    public void add1() {
        super.add1();
        System.out.println(b);
    }
}

public class chap13_0 {
    public static void main(String[] args) {
        Myvalue aaa=new bbb();
        aaa.add1();

        aaa.a=289;
        aaa.add1();


    }




}
