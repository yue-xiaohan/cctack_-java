package demo1;

abstract class Shape {

    protected abstract void draw();//

   /* public void func() {

    }*/
}
class Rect extends Shape {
    @Override
    public void draw() {
        System.out.println("画矩形！");
    }
}
/*abstract class Rect extends Shape {

}
class A extends Rect {
    @Override
    public void draw() {
        System.out.println("画矩形！");
    }
}*/
class Cycle extends Shape {
    @Override
    public void draw() {
        System.out.println("画圆！");
    }
}
class Flower extends Shape {
    @Override
    public void draw() {
        System.out.println("❀！");
    }
}

public class chap13_5 {
    public static void drawMap(Shape shape) {
        shape.draw();
    }
    public static void main(String[] args) {
        //Shape shape = new Shape();
        drawMap(new Rect());
        drawMap(new Cycle());
        drawMap(new Flower());

        Shape shape = new Rect();
    }
}
