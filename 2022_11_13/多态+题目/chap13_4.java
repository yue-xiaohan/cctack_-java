class B {
    public B() {
        // do nothing
        func();// 以后不要这样写 ！！
    }
    public void func() {
        System.out.println("B.func()");//
    }
}
class D extends B {
    private int num = 1;
    public D () {
        super();
    }
    @Override
    public void func() {
        //System.out.println("fafdadssa!!!!!");
        System.out.println("D.func() " + num+" 因为父类此时还没有走完！");
    }
}
public class chap13_4 {
    public static void main(String[] args) {
        D d = new D();
    }
}
