class Base {
    private int x;
    private int y;

    public Base(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

}

class Sub extends Base {

    private int z;

    public Sub(int x, int y, int z) {
        //write your code here
        super(x,y);
        this.z = z;
    }

    public int getZ() {
        return z;
    }

    public int calculate() {
        return super.getX() * super.getY() * this.getZ();
    }

}

class MyValue {
    private int val;//

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }
}
public class chap13_1 {
    public static void swap(MyValue myV1,MyValue myV2) {
        int tmp = myV1.getVal();
        //myV1.val = myV2.val;
        myV1.setVal( myV2.getVal());
        //myV2.val = tmp;
        myV2.setVal(tmp);
    }
    public static void main1(String[] args) {
        MyValue myValue1 = new MyValue();
        myValue1.setVal(10);
        MyValue myValue2 = new MyValue();
        myValue2.setVal(20);

        swap(myValue1,myValue2);//传过去引用交换的是引用的地址

        System.out.println(myValue1.getVal());
        System.out.println(myValue2.getVal());
    }
    /*public static void swap(MyValue myV1,MyValue myV2) {
        int tmp = myV1.val;
        myV1.val = myV2.val;
        myV2.val = tmp;
    }
    public static void main(String[] args) {
        MyValue myValue1 = new MyValue();
        myValue1.val = 10;
        MyValue myValue2 = new MyValue();
        myValue2.val = 20;

        swap(myValue1,myValue2);

        System.out.println(myValue1.val);
        System.out.println(myValue2.val);
    }*/
}
