
class Shape {

    public void draw() {
        System.out.println("画图形！");
    }
}
class Rect extends Shape {
    @Override
    public void draw() {
        System.out.println("画矩形！");//
    }
}
class Cycle extends Shape {
    @Override
    public void draw() {
        System.out.println("画圆！");
    }
}
class Flower extends Shape {
    @Override
    public void draw() {
        System.out.println("❀！");
    }
}

public class chap13_3 {
    public static void drawMap(Shape shape) {
        shape.draw();
    }

    public static void drawMap2() {
        Rect rect = new Rect();
        Cycle cycle = new Cycle();
        Flower flower = new Flower();
        //cycle  rect  cycle  rect  flower
        String[] shapes = {"cycle", "rect", "cycle", "rect", "flower"};
        for (String s : shapes) {
            if(s.equals("cycle")) {
                cycle.draw();
            }else if(s.equals("rect")) {
                rect.draw();
            }else {
                flower.draw();
            }
        }
    }
    public static void drawMap() {
        Rect rect = new Rect();
        Cycle cycle = new Cycle();
        Flower flower = new Flower();

        Shape[] shapes = {cycle,rect,cycle,rect,flower};
        //int[] array = {1,2,3,4};
        for(Shape shape : shapes) {
            shape.draw();
        }
    }

    public static void drawMap3() {
        Shape rect = new Rect();
        Shape cycle = new Cycle();
        Shape flower = new Flower();

        Shape[] shapes = {cycle,rect,cycle,rect,flower};
        //int[] array = {1,2,3,4};
        for(Shape shape : shapes) {
            shape.draw();
        }
    }


    public static void main(String[] args) {
        drawMap();
        //drawMap2() ;
        /*Rect rect = new Rect();
        Cycle cycle = new Cycle();

        drawMap(rect);
        drawMap(cycle);
        drawMap(new Flower());*/
    }
}

