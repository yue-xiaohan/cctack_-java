class Animal {
    public String name;
    public int age;

    public void eat() {
        System.out.println(name + "正在吃饭！");
    }
}
class Dog extends Animal {

    public void wangwang() {
        System.out.println(name +" 正在汪汪叫！");
    }

    @Override//待表这个方法是重写的
    public void eat() {
        //方法重写
        System.out.println(name+ " 正在吃狗粮!");
    }
}
class  Bird extends  Animal{
    public String wing;//翅膀

    public void fly() {
        System.out.println(name + "正在飞！");
    }

    @Override
    public void eat() {
        System.out.println(name +" 正在吃鸟粮！");
    }
}
public class chap13_2 {

    public static void main7(String[] args) {
        Animal animal1 = new Dog();
        //向下转型
       /* Dog dog = (Dog)animal1;
        dog.name = "haha";
        dog.wangwang();*/

        System.out.println("=========");
        //可以理解为：animal1这个引用 是不是引用了 Bird对象
        if(animal1 instanceof Bird) {
            Bird bird = (Bird) animal1;
            bird.fly();
        }



    }

    public static void function(Animal animal) {
        animal.eat();//
    }

    public static void main6(String[] args) {
        Animal animal1 = new Dog();
        animal1.name =  "十三月";
        function(animal1);

        System.out.println("===========");
        Animal animal2 = new Bird();
        animal2.name = "圆圆";
        function(animal2);

    }

    public static int add(int a,int b) {
        return a+b;
    }

    public static int add(int a,int b,int c) {
        return a+b+c;
    }

    public static void main5(String[] args) {
        System.out.println(add(1, 2));
        System.out.println(add(1, 2,3));
    }

    public static void main(String[] args) {
        Animal animal1 = new Dog();
        animal1.name =  "十三月";
        animal1.eat();//动态绑定
        /**
         * 动态绑定：1：向上转型
         * 2：重写
         * 3：通过父类调用同名方法
         */
        //animal1.wangwang();
        System.out.println("===========");
        Animal animal2 = new Bird();
        animal2.name = "圆圆";
        animal2.eat();
    }
    public static void func(Animal animal) {
        //可以通过方法的传参进行向上转型

    }

    public static Animal func2() {

        return new Dog();
    }

    public static void main3(String[] args) {
        Dog dog = new Dog();
        func(dog);
    }
    public static void main2(String[] args) {
        Animal animal1 = new Dog();
        animal1.name =  "十三月";
        animal1.eat();
        //animal1.wangwang();
        //向上转型之后，通过父类的引用只能访问父类自己的成员，不能访问子类特有的成员。

        System.out.println("===========");
        Animal animal2 = new Bird();
        animal2.name = "圆圆";
        animal2.eat();

    }
    public static void main1(String[] args) {
        Dog dog = new Dog();
        dog.name = "十三月";
        dog.eat();
        dog.wangwang();

        Animal animal = dog;//向上转型

        Bird bird = new Bird();
        bird.name = "圆圆";
        Animal animal2 = bird;

    }
}


